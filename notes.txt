BEFORE PUBLIC RELEASE:
    [done]  - Select license (e.g. MIT, BSD, GNU)
- Determine hosting / version control
- Choose name for project, be aware of rules regarding use of "MIT" (http://web.mit.edu/policies/12/12.3.html)
- Choose logo, maybe feature cartpole or blocks as part of image

Tasks:
	[done]	(Bobby) - Improve installation process; automate setting of platform-dependent environment variables, either during installation or at runtime from Tools.py
-(Alborz, Bobby) - Doxygen 
(Elliot) Connect RL-Framework with ROS through the RCCar domain.
(Elliot) Improve iFDD Speed
- Optimization
- Feature Names
- bestAction in matrix form (how about breaking ties randomly)
- RL-Python directory is the key for running the code.
- Blending parameters between planning and learning. Make both of them uniform. Currently the output directory of the planner is hardcoded.
- Everything in dot format rather than * or outer. All items has to be ndarray rather than array.
- Config File For Experiments
(Elliot) - Fix the issue with legend not being shown during the run
- TBPI has problem with graphics due to deepcopy use.


Misc:
    [done] Doxygen page reads "Reinforced Learning", s/b "Reinforcement Learning"
- Doxygen representation page implimentation --> implementation
- Formatting change of license? Long lines with gaps between.
- Change "Bob Klein" to "Robert H Klein"
- 
