#include "Util.h"


double saturate(double val, double low, double hi){
	if (val < low){
		return low;
	}
	if (val > hi){
		return hi;
	}
	return val;
}

double dist(double dx, double dy){
	return sqrt(dx*dx+dy*dy);
}

double wrap(double ang){
	while(ang < -M_PI){
		ang += 2*M_PI;
	}
	while (ang > M_PI){
		ang -= 2*M_PI;
	}
	return ang;
}


bool checkMapContainsAllOf(map<string, double> strmap, vector<string> req){
	for (int i = 0; i < req.size(); i++){
		if (strmap.find(req[i]) == strmap.end()){
			return false;
		}
	}
	return true;
}
