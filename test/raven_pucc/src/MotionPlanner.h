/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 *
 */

#ifndef MOTIONPLANNER_H_
#define MOTIONPLANNER_H_

#include "Obstacle.h"
#include "Pose2D.h"
#include <string>
#include <vector>
#include <queue>
#include <boost/algorithm/string.hpp>

using namespace std;

class MotionPlanner {
public:
	MotionPlanner();
	virtual ~MotionPlanner();
	string name;
	double xmin, xmax, ymin, ymax;
	void setName(string newName);
	void setBoundaries(double xmin, double xmax, double ymin, double ymax);
	void updateWaypoints(Pose2D pose, queue<Pose2D>& wpts);

	virtual void advertiseGoal(ros::NodeHandle nhandle) = 0;
	virtual void subscribeCarrotList(ros::NodeHandle nhandle) = 0;
	virtual void plan(Pose2D curpose, vector<Obstacle*> obstacles, queue<Pose2D>& wpts, double& velref, double& dpsiref) = 0;
	virtual void plan(Pose2D curpose, vector<Obstacle*> obstacles, double veldes, double dpsides, double& velref, double& dpsiref) = 0;
	virtual bool reachedWaypoint(double dx, double dy) = 0;
};

#endif /* MOTIONPLANNER_H_ */
