#ifndef __UTIL_H

#include <cmath>
#include <vector>
#include <map>
#include <string>

using namespace std;

double saturate(double val, double low, double hi);
double wrap(double ang);
double dist(double dx, double dy);
bool checkMapContainsAllOf(map<string, double> strmap, vector<string> req);

#define __UTIL_H
#endif /* __UTIL_H */
