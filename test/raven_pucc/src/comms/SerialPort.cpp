#include "SerialPort.h"
// Serial Port
SerialPort::SerialPort()
{
	initialized = false;
}

int SerialPort::spInitialize(std::string hwDevice, int baudRate, bool blocking)
{

	struct termios options;

	// Architecture-dependent code: open and configure the serial port
	// Serial port settings:  38400, No parity, 8 data bits, 1 stop bit, no flow control
	if( blocking == true )
		serial = open(hwDevice.c_str(),O_RDWR);
	else
		serial = open(hwDevice.c_str(),O_RDWR | O_NONBLOCK);

	if(serial == -1 ) {
		printf("\tSerial Error :: Couldn't open serial port at %s\n", hwDevice.c_str());
		return -1;
	}

	tcgetattr(serial, &options);
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

	if( baudRate == 38400 ) { // ghetto hack
		cfsetispeed(&options, B38400);
		cfsetospeed(&options, B38400);
	} else if( baudRate == 115200 ) {
		cfsetispeed(&options, B115200);
		cfsetospeed(&options, B115200);
	} else if( baudRate == 19200 ) {
		cfsetispeed(&options, B19200);
		cfsetospeed(&options, B19200);
	} else if( baudRate == 57600 ) {
		cfsetispeed(&options, B57600);
		cfsetospeed(&options, B57600);
	} else {
		cfsetispeed(&options, B9600);
		cfsetospeed(&options, B9600);
	}

	options.c_cflag &= ~CSIZE; /* Mask the character size bits */
	options.c_cflag |= CS8;    /* Select 8 data bits */
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;  // 1 stop bit
	options.c_cflag &= ~CRTSCTS; // No RTC/CTS (hardware) flow control
	options.c_cflag &= ~PARODD;
	options.c_cflag &= ~CRTSCTS;
	options.c_lflag &= ~ECHOCTL;
	options.c_lflag &= ~ECHOKE;
	options.c_lflag &= ~ECHOK;
	options.c_iflag |= IGNBRK;
	options.c_cc[VTIME] = 5;

	if(tcsetattr(serial, TCSANOW, &options) != 0) {
		printf("\tSerial Error :: Couldn't configure serial port\n");
		return -1;
	}
	// End architecture-dependent code

	printf("\tSerial Port %s Initialized\n",hwDevice.c_str());
	initialized = true;
	return 1;
}


int SerialPort::spSend(char* pkt)
{
	return write(serial,pkt,strlen(pkt));
}

int SerialPort::spSend(char* pkt, int sz)
{
	return write(serial,pkt,sz);
}

int SerialPort::spSend(uint8_t* pkt, uint8_t sz)
{
	return write(serial,pkt,sz);
}

int SerialPort::spSend(unsigned char* pkt, int sz)
{
	return write(serial,pkt,sz);
}


unsigned char* SerialPort::spReceive()
{
	int n = read(serial, &buffer, SER_BUF_SZ);
	return buffer;
}

unsigned char* SerialPort::spReceive(int* nbytes)
{
	int n = read(serial, &buffer, *(nbytes));
	*(nbytes) = n;
	return buffer;
}

int SerialPort::spReceive(unsigned char* buf, int nbytes)
{
	return read(serial, buf, nbytes);
}

char SerialPort::spReceiveSingle()
{
	char ch;
	int n = read(serial, &ch, 1);
	return ch;
}

int SerialPort::spSetBaud(int rate)
{
	struct termios options;
	tcgetattr(serial, &options);

	if( rate == 38400 ) { // ghetto hack
		cfsetispeed(&options, B38400);
		cfsetospeed(&options, B38400);
	} else if( rate == 115200 ) {
		cfsetispeed(&options, B115200);
		cfsetospeed(&options, B115200);
	} else if( rate == 19200 ) {
		cfsetispeed(&options, B19200);
		cfsetospeed(&options, B19200);
	} else if( rate == 57600 ) {
		cfsetispeed(&options, B57600);
		cfsetospeed(&options, B57600);
	} else {
		cfsetispeed(&options, B9600);
		cfsetospeed(&options, B9600);
	}

	if( tcsetattr(serial, TCSANOW, &options) != 0 ) {
		printf("\tSerial Error :: Couldn't reset baud rate\n");
		return(-1);
	}
	return(0);
}

void SerialPort::spFlush()
{
	tcflush(serial, TCIFLUSH);
}

void SerialPort::spClose()
{
	close(serial);
	initialized = false;
}




