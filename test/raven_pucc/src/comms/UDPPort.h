#ifndef __UDPPORT_H
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <signal.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>
#include <vector>
#include <iostream>
#include <string>
#include <string.h>
#include <math.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>
struct Message
{
    int    src;
    int    dest;
    int    cmdID;
    double data[12];

    // Constructors:
    Message()
    {
        src   = 0;
        dest  = 0;
        cmdID = 0;
        for( int ii=0; ii<12; ii++)
        {
            data[ii] = 0.0;
        }
    }
};

class UDPPort
{

        bool broadcast;
        int  portTx;
        int  portRx;
        int  socketTx;
        int  socketRx;
        struct sockaddr_in addressTx;
        struct sockaddr_in addressRx;

        char buffer[2048];

public:

        int   udpInit(bool bcast, std::string ipaddr, int port);
        int   udpInit(bool bcast, std::string ipaddr, int listen_port, int bcast_port);
        int   udpSend(char* pkt);
        int   udpSend(Message* pkt);
        char* udpReceive();
        void  udpClose();

};
#define __UDPPORT_H
#endif /* __UDPPORT_H */
