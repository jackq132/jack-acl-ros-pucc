#ifndef __BTPORT_H
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <signal.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>
#include <vector>
#include <iostream>
#include <string>
#include <string.h>
#include <math.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>
class BTPort
{

		const char * mac;
        char buffer[2048];
        int s;

public:

        int   BTInit(std::string macaddr);
        int   BTSend(char* pkt, int len);
        uint8_t  BTReceiveByte();
        void  BTClose();

};


#define __BTPORT_H
#endif /* __BTPORT_H */
