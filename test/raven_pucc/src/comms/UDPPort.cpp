#include "UDPPort.h"
// UDP Port
//
int UDPPort::udpInit(bool bCast, std::string addr, int lport)
{
	return( UDPPort::udpInit(bCast, addr, lport, lport) );	
}

int UDPPort::udpInit(bool bCast, std::string addr, int lport, int bport)
{
	broadcast = bCast;
	portTx    = bport;
	portRx    = lport;

	// Receive port
	socketRx = socket(PF_INET, SOCK_DGRAM, 0);
	bzero(&addressRx, sizeof(addressRx));

	addressRx.sin_family      = AF_INET;
	addressRx.sin_port        = htons(portRx);
	addressRx.sin_addr.s_addr = INADDR_ANY;

	if( bind(socketRx, (struct sockaddr*)&addressRx, sizeof(addressRx)) != 0 ) {
		std::cout << "Couldn't bind to Rx port " << portRx << std::endl;
	}

	std::cout << "UDP listen on port " << portRx << " complete" << std::endl;

	// Broadcast port
	if(broadcast) {
		socketTx = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
		bzero(&addressTx, sizeof(addressTx));

		if(socketTx < 0)
			std::cout << "Couldn't create socket" << std::endl;

		const int on = 1;
		if(setsockopt(socketTx, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on)) < 0)
			std::cout << "Couldn't set SO_BROADCAST" << std::endl;

		addressTx.sin_family = AF_INET;
		addressTx.sin_port   = htons(portTx);

		inet_aton(addr.c_str(), &addressTx.sin_addr);
		std::cout << "UDP broadcast on port " << portTx << " complete" << std::endl;
	}


	return(1);
}


int UDPPort::udpSend(char* pkt)
{

	sendto(socketTx, pkt, strlen(pkt), 0, (struct sockaddr *)&addressTx, sizeof(addressTx));

	return strlen(pkt);

}
int UDPPort::udpSend(Message* msg)
{
	char pkt[1028];
	bzero(pkt, sizeof(pkt));

	sprintf(pkt,"%d %d %d %f %f %f %f %f %f %f %f;",msg->src,msg->dest,msg->cmdID,
			msg->data[0],msg->data[1],msg->data[2],msg->data[3],msg->data[4],msg->data[5],msg->data[6],msg->data[7]);

	return udpSend(pkt);
}

char* UDPPort::udpReceive()
{

	bzero(buffer, sizeof(buffer));

	int addr_len = sizeof(addressRx);

	recvfrom(socketRx, buffer, sizeof(buffer), 0, (struct sockaddr*)&addressRx, (socklen_t*)&addr_len);

	return buffer;
}


void UDPPort::udpClose()
{

	close(socketRx);

	if(broadcast)
		close(socketTx);

}
