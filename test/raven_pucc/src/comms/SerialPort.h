#ifndef __SERIALPORT_H
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <signal.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>
#include <vector>
#include <iostream>
#include <string>
#include <string.h>
#include <math.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>

#define SER_BUF_SZ 1024

class SerialPort
{

        int  serial;
        unsigned char buffer[SER_BUF_SZ];

public:

        SerialPort();

        bool  initialized;
        int   spInitialize(std::string usbport,int baudrate, bool block);
        int   spSend(char* pkt);
        int   spSend(char* pkt, int sz);
        int   spSend(uint8_t* pkt, uint8_t sz);
        int   spSend(unsigned char* pkt, int sz);
        unsigned char* spReceive();
        unsigned char* spReceive(int* nbytes);
        int   spReceive(unsigned char* buf, int nbytes);
        char  spReceiveSingle();
        int   spSetBaud(int rate);
        void  spFlush();        
        void  spClose();

};


#define __SERIALPORT_H
#endif /* __SERIALPORT_H */
