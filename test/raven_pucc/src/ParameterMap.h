#ifndef __PARAMETER_MAP_H
#define __PARAMETER_MAP_H
#include <map>
#include <string>
#include <ros/ros.h>

using namespace std;

class ParameterMap{
	public:
		void put(string s, double v);
		double get(string s);
	private:
		map<string, double> params;
};
#endif
