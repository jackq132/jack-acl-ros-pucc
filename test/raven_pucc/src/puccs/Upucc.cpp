/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 *
 */

#include "Upucc.h"


Upucc::Upucc() : Pucc()
{
	battVoltage = -1;
}

Upucc::~Upucc()
{
	stop();
	bt->BTClose();
}

/* If necessary, initialize the usb and serial port, return false for failure,
 *  return true for success.
 */
bool Upucc::setupHardware(int argc, char *argv[], int argcnt, std::string puccname)
{
	if(! this->isSimulated()) { // hardware
		ROS_INFO("Getting bluetooth mac address for %s...", puccname.c_str());
		std::string btparam = "/BTpairings/";
		btparam.append(puccname);
		std::string macaddr;
		bool res = ros::param::get(btparam, macaddr);
		if (!res){
			ROS_FATAL("ERROR: Could not find %s on the param server!\n Did you make sure to load upucc/config/BTpairings.yml?", btparam.c_str());
			exit(0);
		}
		ROS_INFO("Opening bluetooth for %s...", puccname.c_str());
		openBT(macaddr);
	} // else take no action
	return true;
}

void Upucc::openBT(std::string& macaddr)
{
	bt = new BTPort;
	bt->BTInit(macaddr);

	// Start listen thread
//	pthread_t thread;
//	pthread_create(&thread, NULL, BTlisten, (void *)this);
	return;

}

void Upucc::sendDriveCommands()
{
	// keep args within Create limits
	control.vR = saturate(control.vR,-500,500);
	control.vL = saturate(control.vL,-500,500);

	//printf("L: %f R: %f \n", control.vL, control.vR);

	uint8_t l,r;
	l = (uint8_t)(64.0+62.0*control.vL/500.0);
	r = (uint8_t)(192.0+62.0*control.vR/500.0);

	bt->BTSend((char *)&l,1);
	bt->BTSend((char *)&r,1);

}

void Upucc::simulateDynamics(double dt){
	// keep args within Create limits
	control.vR = saturate(control.vR,-500,500);
	control.vL = saturate(control.vL,-500,500);

	//simulate for dt
	double vR = control.vR/1000.0;
	double vL = control.vL/1000.0;
	vel = (vL+vR)/2.0;
	dpsi = (vR-vL)/(UPUCC_WHEELBASE);
	vx = vel*cos(psi);
	vy = vel*sin(psi);
	x = x + vx*dt;
	y = y + vy*dt;
	psi = psi + dpsi*dt;
}

/* TODO At the moment, does not appear that upuccs require scaling, happens
 * during calculation of controls based on wheelbase.  Check this in hardware.
 */
void Upucc::scaleDriveCommands()
{

}

void * BTlisten(void * arg)
{
	Upucc * gpucc = (Upucc *) arg;

	// The uPucc sends back one byte every second. It is the raw 8-bit ADC voltage
	// which corresponds to 1/2 the battery voltage.

	/*
	while(1)
	{
		gpucc->battVoltage = ((double)gpucc->bt->BTReceiveByte())/255.0*5.0*2.0;
		if(gpucc->battVoltage < 7.2)
		{
			printf("WARNING: Batt at %0.2f \n",gpucc->battVoltage);
			exit(0);
		}
		//printf("Batt: %0.2f \n",gpucc->battVoltage);
	}
	*/
}

/*
void toggleLEDCallback(const ros::TimerEvent& timerEvent)
{
	unsigned char newPlayLEDColor = 1; // TODO Placeholder, can modify colors later
	// The casts below are to remind you that the data types are a bit wonky
	if(robot.playLEDIntensity > 0) robot.playLEDIntensity = 0;
	else robot.playLEDIntensity = (unsigned char)(255);
	ROS_INFO("Set LED to be %d", robot.playLEDIntensity);
	// Only actually physically set the LED if not simulated
	if(!robot.isSimulated) robot.setLED(LED_PLAY, newPlayLEDColor, robot.playLEDIntensity);
	// double below to emphasize that the feature is stored as a double, though value itself cannot have decimal
	robot.features[LED_INTENSITY_FEAT] = (double)(robot.playLEDIntensity);
	robot.features[LED_COLOR_FEAT] = newPlayLEDColor;
}



void updateLEDDuration(ros::NodeHandle &nh)
{
	if(robot.features.size() > 0) {// Make sure the robot has been assigned features
		double LEDDur = robot.features[LED_DURATION_FEAT];
		if(robot.togglePeriod != LEDDur) {
			ROS_INFO("Setting new toggle period to be %f", LEDDur);
			robot.togglePeriod = LEDDur;
			ros::Duration toggleDur = ros::Duration(LEDDur);
			robot.ledToggleTimer = nh.createTimer(toggleDur, toggleLEDCallback);
		}
	}
}*/
