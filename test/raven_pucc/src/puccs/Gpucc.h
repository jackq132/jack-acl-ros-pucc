/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 *
 */

#ifndef GPUCC_H_
#define GPUCC_H_

#include "ros/ros.h"
#include "../Pucc.h"
#include "../comms/createoi.hpp" // For things like LED_PLAY
#include "../comms/SerialPort.h"


// no batt voltage
// no bt
// additional camera
// sensor list

class Gpucc : public Pucc {

protected:
    SensorList*           sensor;	// Sensor list, via createoi.hpp
    SerialPort*           serial;  	// serial port

    unsigned char playLEDIntensity;	// Current intensity of the playLED, 255 max, 0 off.
    unsigned char playLEDColor;		// 0-255 color setting for LED
    double togglePeriod;			// The period between flashes of the light
    ros::Timer ledToggleTimer;		// Manages the time between on/off toggling of LED

public:
    Gpucc(string name, bool isSim, bool useJoy);
    virtual ~Gpucc();

    /* Used for initialization and shutdown of Open Interface (OI) to gpucc */
        void  start_OI();
        void  stop_OI();
		bool setupHardware(std::string usbPortLoc);

	/* *********** Drive commands *************************** */
        void sendDriveCommands();

	/* *********** Accessor and assignment methods *********** */
		int* getAllSensors();
		int  readSensor(oi_sensor packet);
		void readSensors();
		int  readRawSensor(oi_sensor packet, unsigned char* buffer, int size);
		std::string getPuccType();

    /* ******************** Mutator methods ******************** */
        int  setLED(oi_led lflags, unsigned char clr, unsigned char intensity);
        int  setBaud(int rate);
        int  setDigitalOuts(oi_output oflags);
        int  setPWMLowSideDrivers(unsigned char pwm0, unsigned char pwm1, unsigned char pwm2);
        int  setLowSideDrivers(oi_output oflags);
        int  sendIRbyte(unsigned char irbyte);
        int  writeSong(unsigned char number, unsigned char length, unsigned char* song);
        int  playSong(unsigned char number);


    /* ****************** Computation methods ******************* */
        double  getWheelBase();
        void simulateDynamics(double dt);
        void scaleDriveCommands();

		/* CLIENT CODE */
        void puccMain(ros::NodeHandle &nhandle);
		void assignLEDs(ros::NodeHandle nhandle);
		void updateLEDDuration(ros::NodeHandle &nhandle);
		void toggleLEDCallback(const ros::TimerEvent& timerEvent);
};

#endif
