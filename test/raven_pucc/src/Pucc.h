/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 * 
 *
 */


#ifndef PUCC_H_
#define PUCC_H_

#include "ros/ros.h" // for callbacks, etc
#include "MotionPlanner.h"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Point.h"
#include "sensor_msgs/Joy.h"
#include "raven_pucc/waypoint.h"
#include "raven_pucc/vrotcmd.h"
#include "raven_pucc/idleStatus.h"
#include "raven_pucc/trajId.h"
#include "raven_pucc/goal.h"
#include "raven_pucc/carrots.h"
#include <string>
#include <vector>
#include <queue>
#include "Obstacle.h"
#include "Pose2D.h"
#include "Util.h"
#include <map>
#include "ParameterMap.h"
#include "comms/createoi.hpp" // For things like LED_PLAY

using namespace std;


enum CommandType {WAYPOINT_FOLLOW, V_PSI_DOT_DES};
class Pucc {
public:
	// functions
	Pucc(string name, bool isSim, bool useJoy);
	virtual ~Pucc();
	string getName();

	void setPose(Pose2D pose);
	void setVel(Pose2D pose);
	void setPoseVel(Pose2D pose);
	void getPose(Pose2D &curState);
	void getGoal(Pose2D &curGoal);

	bool isSimulated();
	bool useJoystick();
	void updateDriveCommands(bool shouldUpdateMp);

	ParameterMap getParams();
	

	void setVPsidotDes(double v, double psidot);
	void addWaypoint(bool clear, Pose2D wpt);
	void stop();

	void setMotionPlanner(MotionPlanner *newMP);

	void updateObstacleList(ros::NodeHandle nhandle);


	void subscribePoseVel(ros::NodeHandle nhandle); 
	void advertisePoseVel(ros::NodeHandle nhandle);
	void advertiseIdleStatus(ros::NodeHandle nhandle);
	void advertiseCurTrajId(ros::NodeHandle nhandle);
	void subscribeTrajId(ros::NodeHandle nhandle);
	void subscribeWayptVPsidot(ros::NodeHandle nhandle);
	void subscribeJoy(ros::NodeHandle nhandle);
	void publishSpoofedPoseVel();
	void setLedBar(int ledColor);
	void ledBarPwm();

	virtual void sendDriveCommands() = 0;
	virtual int setDigitalOuts(oi_output oflags) = 0;
	virtual void simulateDynamics(double dt) = 0;
	virtual bool setupHardware(string usbPortLoc) = 0;
	virtual void puccMain(ros::NodeHandle &nhandle)=0;


protected:

	ParameterMap params;

	MotionPlanner *motionPlanner;
	string name;		// Pucc name (uP01, uP02, ...)
	bool firstRun;
	bool isSim;				// Is this pucc simulated or real?
	bool useJoy;			// Is the pucc controlled via joystick, or waypoints/vpsirot?
	double x, y, psi;       	// x,y location of the pucc in RAVEN (m) and its heading, psi (rad)
	double vx, vy, dpsi;   		// inertial velocities & turn rate (m/s, rad/s)
	double vel;    				// velocity (+forward, -backwards) (m/s)
	double veldes, dpsides; 	// desired v and psidot (only used in v/psidot command mode) -- different from velref/dpsiref due to possible obstacles
	double velref, dpsiref; 	// reference v/psidot, sent to controller
	int trajIdIncoming, ledColorIncoming;
	double timePrevLed;
	int dutyCycleCounter;

	CommandType cmdtype;
	queue<Pose2D> wpts;       // queue of goal waypoints, index 0 (front) has current target.
	vector<Obstacle*> obstacles;

	ros::Subscriber posesub, velsub, wptsub, vpsidotsub, joysub, trajIdSub; // used for actual hardware
    	ros::Publisher poseSpooferPub, velSpooferPub; // used during simulation
 	ros::Publisher idleStatusPub, curTrajIdPub;


	void poseCallback(const geometry_msgs::PoseStamped &msg);
	void velCallback(const geometry_msgs::TwistStamped &msg);
	void trajIdCallback(const raven_pucc::trajId &msg);
	void wayptCallback(const raven_pucc::waypoint::ConstPtr& msg);
	void vpsidotCallback(const raven_pucc::vrotcmd::ConstPtr& msg);
	void joyCallback(const sensor_msgs::Joy::ConstPtr& msg);

};

#endif /* PUCC_H_ */
