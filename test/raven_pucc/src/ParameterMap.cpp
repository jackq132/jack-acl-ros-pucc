#include "ParameterMap.h"

void ParameterMap::put(string s, double v){
	params[s] = v;
	return;
}

double ParameterMap::get(string s){
	if (params.find(s) == params.end()){
		ROS_FATAL("Parameter map does not contain %s", s.c_str());
		return 0;	
	}	
	return params[s];
}
