/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 *
 */

#include "MotionPlanner.h"


MotionPlanner::MotionPlanner(){
}

MotionPlanner::~MotionPlanner(){
}

void MotionPlanner::setName(string newName){
	this->name = newName;
	return;
}


void MotionPlanner::setBoundaries(double xmin, double xmax, double ymin, double ymax){
	this->xmin = xmin;
	this->xmax = xmax;
	this->ymin = ymin;
	this->ymax = ymax;
	return;
}

void MotionPlanner::updateWaypoints(Pose2D pose, queue<Pose2D>& wpts){
	if (!wpts.empty()){	
		//ROS_INFO("checking if reached waypoint (x,y): (%f, %f)", wpts.front().x, wpts.front().y);
		//ROS_INFO("checking if reach...curpose  (x,y): (%f, %f)", pose.x, pose.y);
		if (reachedWaypoint(wpts.front().x - pose.x, wpts.front().y - pose.y)){
			wpts.pop();
		}
	}
	return;
}
