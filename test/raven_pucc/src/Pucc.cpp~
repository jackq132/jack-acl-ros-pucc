/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 *
 */

#include "Pucc.h"

Pucc::Pucc(string name, bool isSim, bool useJoy) {
	this->name = name;
	this->isSim = isSim;
	this->useJoy = useJoy;
	x = y = psi = 0;
	vx = vy = dpsi = 0;
	vel = dpsides = veldes = 0;
	cmdtype = WAYPOINT_FOLLOW;
	motionPlanner = NULL;
	trajIdIncoming = -1;
	ledColorIncoming = 0;
	firstRun = true;
}

Pucc::~Pucc() {
	if (motionPlanner != NULL){
		 delete motionPlanner;
		 motionPlanner = NULL;
	}
	for (int i = 0; i < obstacles.size(); i++){
		if (obstacles[i] != NULL){
			delete obstacles[i];
		}
		obstacles[i] = NULL;
	}
}

string Pucc::getName()
{
	return name;
}

void Pucc::ledBarPwm(){
	double timePassed = ros::Time::now().toSec() - timePrevLed;

	int freqLedPwm = 25; //maximum frequency of LED turning off/on.
	int cycleResolution = 2; //resolution of a single LED PWM cycle.
	//double completeCycleTime = cycleResolution/freqLedPwm; //amount of time for a single LED PWM cycle
	double dutyCyclePercent = 0.5; //should be [0,1]
	int dutyCycleMaxCount = dutyCyclePercent*cycleResolution;

	//ROS_INFO("dutyCycleMaxCount %d", dutyCycleMaxCount);

	if (timePassed >= 1.0f/freqLedPwm) {
		timePrevLed = ros::Time::now().toSec();
		ROS_INFO("Actual Sampling Freq: [%f]", 1.0f/timePassed);
		//ROS_INFO("dutyCycleCounter %d", dutyCycleCounter);

		if (dutyCycleCounter > cycleResolution) {
			dutyCycleCounter = 0;
		}

		if (dutyCycleCounter<dutyCycleMaxCount) {
			setLedBar(1);
		} else {
			setLedBar(0);
		}

		dutyCycleCounter++;
	}
}

void Pucc::setLedBar(int ledColor){
	if (ledColor == 0) {
		setDigitalOuts(OUTPUT_NONE);
	} else if (ledColor == 1) {
		setDigitalOuts(OUTPUT_0);
	} else if (ledColor == 2) {
		setDigitalOuts(OUTPUT_1);
	} else if (ledColor == 3) {
		setDigitalOuts(OUTPUT_2);
	}
}

void Pucc::getGoal(Pose2D &curGoal){
	raven_pucc::idleStatus msg;
	raven_pucc::trajId msgTrajId;
	msgTrajId.id = trajIdIncoming;
	msgTrajId.ledColor = ledColorIncoming;
	setLedBar(ledColorIncoming);

	if (!wpts.empty()){
		msg.idle = false;
		msg.name = name;
		idleStatusPub.publish(msg);
		curTrajIdPub.publish(msgTrajId);
		Pose2D& goal = wpts.front();
		curGoal.x = goal.x;
		curGoal.y = goal.y;
		curGoal.psi = goal.psi;
		curGoal.dx = 0; 
		curGoal.dy = 0;
		curGoal.dpsi = 0;
		curGoal.vel = goal.vel;
	} else {
		ROS_INFO("Final goal node reached!");
		msg.idle = true;
		msg.name = name;
		trajIdIncoming = -1;
		ledColorIncoming = 0;
		setLedBar(ledColorIncoming);
		idleStatusPub.publish(msg);
		getPose(curGoal);
		curGoal.dx = curGoal.dy = curGoal.dpsi = 0;
	}
	return;
}

void Pucc::getPose(Pose2D &pose)
{
	pose.x = x;
	pose.y = y;
	pose.psi = psi;
	pose.dx = vx;
	pose.dy = vy;
	pose.dpsi = dpsi;
	pose.vel = vel;
}

bool Pucc::isSimulated()
{
	return isSim;
}


bool Pucc::useJoystick(){
	return useJoy;
}

void Pucc::setPose(Pose2D pose){
	x    = pose.x;
	y    = pose.y;
	psi  = pose.psi;
}


void Pucc::setVel(Pose2D pose){
	vel  = pose.vel;
	vx   = pose.dx;
	vy   = pose.dy;
	dpsi = pose.dpsi;
}

void Pucc::setPoseVel(Pose2D pose){
	x    = pose.x;
	y    = pose.y;
	psi  = pose.psi;
	vel  = pose.vel;
	vx   = pose.dx;
	vy   = pose.dy;
	dpsi = pose.dpsi;
}


/* Sets the reference velocity to v and reference angular velocity to psidot.
 * Automatically changes cmdtype from whatever its old value was (eg waypoints)
 * to velocity / angular velocity following.
 */
void Pucc::setVPsidotDes(double v, double psidot){
	//ROS_INFO("Setting v = %f dpsi = %f", v, psidot);
	wpts = queue<Pose2D>();

	cmdtype = V_PSI_DOT_DES; //adding a reference v/dpsi automatically causes v/psidot control
	veldes = v;
	dpsides = psidot;
}


/* Adds a new waypoint; automatically changes cmdtype from from whatever its old
 * value was (eg V_PSI_DOT) to waypoint following.
 * If wptAddingMode parameter == WPTS_CLEAR, remove all previous goals and add this one.
 * If wptAddingMode parameter == WPTS_APPEND, add this waypoint to the tail of our waypoint queue.
 */
void Pucc::addWaypoint(bool clear, Pose2D wpt) {
	cmdtype = WAYPOINT_FOLLOW; //adding a waypoint automatically sets control to waypoint

	if(!clear) { // add this waypiont to the end of the wpts queue
		ROS_INFO("Appending waypoint: x = %f y = %f psi = %f speed = %f", wpt.x, wpt.y, wpt.psi, wpt.vel);
		raven_pucc::idleStatus msg;
		msg.idle = false;
		msg.name = name;
		idleStatusPub.publish(msg);
		wpts.push(wpt);
	} else{ // clear all previous waypoints, add this one
		ROS_INFO("Clearing and adding waypoint: x = %f y = %f psi = %f speed = %f", wpt.x, wpt.y, wpt.psi, wpt.vel);
		wpts = queue<Pose2D>();
		wpts.push(wpt);
	}
}


ParameterMap Pucc::getParams(){
	return this->params;
}

void Pucc::updateDriveCommands(bool shouldUpdateMp){
	Pose2D curPose;
	this->getPose(curPose);
	if (cmdtype == WAYPOINT_FOLLOW){ //control type: waypoints
		int sz = wpts.size();
		//do not update MP for some planners (e.g. ORCA), since they take care of wpt.popping internally
		if (shouldUpdateMp){
	    		motionPlanner->updateWaypoints(curPose, wpts);
		
		} else if (firstRun) {
			wpts.pop();
		}

		if (wpts.size() < sz){
			ROS_INFO("Completed a waypoint.");
		}
		motionPlanner->plan(curPose, obstacles, wpts, velref, dpsiref); 
		//ROS_INFO("Sending v = %.3f dpsi = %.3f", velref, dpsiref);
	} else if (cmdtype == V_PSI_DOT_DES){
		motionPlanner->plan(curPose, obstacles, veldes, dpsides, velref, dpsiref); 
	}
}

void Pucc::stop()
{
	velref = dpsiref = 0;
	veldes = dpsides = 0;
	wpts = queue<Pose2D>();
	if (isSim){
		vx = vy = dpsi = vel = 0;
	} else {
		sendDriveCommands();
	}
}

void Pucc::setMotionPlanner(MotionPlanner *newMP)
{
	motionPlanner = newMP;
	motionPlanner->setName(name);
}


/* Receive position updates, either from vicon for hardware puccs or from spoofed
 * data generated by the puccs themselves
 */
void Pucc::poseCallback(const geometry_msgs::PoseStamped &msg){
	Pose2D pose;
	pose.x = msg.pose.position.x;
	pose.y = msg.pose.position.y;
	pose.psi = wrap(2*atan2(msg.pose.orientation.z, msg.pose.orientation.w));
//	ROS_INFO("Detected x in poseCallback: %f", msg.pose.position.x);
	this->setPose(pose);
}

void Pucc::trajIdCallback(const raven_pucc::trajId &msg){
	trajIdIncoming = msg.id;
	ledColorIncoming = msg.ledColor;
	ROS_INFO("Trajectory ID incoming: %d", trajIdIncoming);
}

void Pucc::velCallback(const geometry_msgs::TwistStamped &msg){
	double speed = dist(msg.twist.linear.x, msg.twist.linear.y);
	double velHdg = atan2(msg.twist.linear.y, msg.twist.linear.x);
	double diff = wrap(velHdg-psi);

	if(fabs(diff) > M_PI/2.0) speed *= -1.0;

	Pose2D pvel;
	pvel.vel = speed;
	pvel.dx = msg.twist.linear.x;
	pvel.dy = msg.twist.linear.y;
	pvel.dpsi = msg.twist.angular.z;
	setVel(pvel);
}

//receive waypoint updates
void Pucc::wayptCallback(const raven_pucc::waypoint::ConstPtr& msg){
	ROS_INFO("New waypoint received");
	Pose2D wpt;
	wpt.x = msg->x;
	wpt.y = msg->y;
	wpt.psi = msg->psi;
	wpt.vel = msg->speed;
	addWaypoint(msg->clear, wpt); 
}

void Pucc::vpsidotCallback(const raven_pucc::vrotcmd::ConstPtr& msg){
	setVPsidotDes(msg->v, msg->psidot);
}

void Pucc::joyCallback(const sensor_msgs::Joy::ConstPtr& msg) {
	////## Joystick Key and Axis Mappings
	//This is for the xbox controller, as far as I can tell
	//I ripped it right out of Mark's quad_control package
	const int A=0;
	const int B=1;
	const int X=2;
	const int Y=3;
	const int LB=4;
	const int RB=5;
	const int BACK=6;
	const int START=7;
	const int ON=8;
	const int LEFT_AXIS=9;
	const int RIGHT_AXIS=10;
	const int LEFT=11;
	const int RIGHT=12;
	const int UP=13;
	const int DOWN=14;
	const int LEFT_X=0;
	const int LEFT_Y=1;
	const int LT=2;
	const int RIGHT_X=3;
	const int RIGHT_Y=4;
	const int RT=5;

	// Control status
	// b -> kill motors
	// left joystick = throttle
	// right joystick = turn
	
	double speedlim = params.get("SPEED_LIMIT");
	double vL = msg.get()->axes[LEFT_Y]*params.get("SPEED_LIMIT"); 
	double vR = msg.get()->axes[RIGHT_Y]*params.get("SPEED_LIMIT"); 
	velref = (vL+vR)/2.0;
	dpsiref = (vR-vL)/params.get("WHEEL_BASE");
	//ROS_INFO("Got joystick msg: Fwd: %f Turn %f", msg.get()->axes[LEFT_Y], msg.get()->axes[RIGHT_X]);

	if (msg.get()->buttons[B]){
		this->stop();
	}
}

void Pucc::updateObstacleList(ros::NodeHandle nhandle){
	ros::master::V_TopicInfo allTopics;
	ros::master::getTopics(allTopics);
	//go over allTopics, and add any that we don't have
	vector<string> objNames;
	for (int i = 0; i < allTopics.size(); i++){
        string topicName = allTopics[i].name; // e.g. /uP04/pose
		//only deal with it if it doesn't have the same name as this robot and it's a vicon object
		if (topicName.find(name) == string::npos && topicName.find("/pose") != string::npos){
			size_t secondSlashIdx = topicName.find("/", 1);
			objNames.push_back(topicName.substr(1, secondSlashIdx-1));
			//search through the list to see if it's already there
			bool alreadySubbed = false;
			for (int j = 0; j < this->obstacles.size(); j++){
				if (strncmp(objNames.back().c_str(), this->obstacles[j]->name.c_str(), strlen(objNames.back().c_str())) == 0){
					alreadySubbed = true;
					break;
				}
			}
			if (!alreadySubbed){
				//constructor call here subscribes to the object's RAVEN stream
				this->obstacles.push_back(new Obstacle(objNames.back(), nhandle));
			}
		}
	}
	//go over everything we have, and remove any that are not in allTopics
	//the removal unsubscribes from the object's stream
	vector<Obstacle*>::iterator it = obstacles.begin();
	while (it != obstacles.end()){
		if (find(objNames.begin(), objNames.end(), (*it)->name) == objNames.end()){
			delete *it;
			it = obstacles.erase(it);
		} else {
			++it;
		}
	}
}

void Pucc::subscribePoseVel(ros::NodeHandle nhandle){
	posesub = nhandle.subscribe(name+"/pose", 1, &Pucc::poseCallback, this);
	ROS_INFO("Subscribed %s/pose to poseCallback", name.c_str());
	velsub = nhandle.subscribe(name+"/vel", 1, &Pucc::velCallback, this); 
}
void Pucc::advertisePoseVel(ros::NodeHandle nhandle){
	poseSpooferPub = nhandle.advertise<geometry_msgs::PoseStamped>(name+"/pose", 1);
	velSpooferPub = nhandle.advertise<geometry_msgs::TwistStamped>(name+"/vel", 1);
}

void Pucc::advertiseCurTrajId(ros::NodeHandle nhandle){
	curTrajIdPub = nhandle.advertise<raven_pucc::trajId>(name + "/curTrajId", 1);
}

void Pucc::advertiseIdleStatus(ros::NodeHandle nhandle){
	idleStatusPub = nhandle.advertise<raven_pucc::idleStatus>(name + "/idleStatus", 1);
}
void Pucc::subscribeWayptVPsidot(ros::NodeHandle nhandle){
	vpsidotsub = nhandle.subscribe(name+"/vpsidot", 1, &Pucc::vpsidotCallback, this);
	wptsub = nhandle.subscribe(name+"/waypts", 1, &Pucc::wayptCallback, this); 
}
void Pucc::subscribeJoy(ros::NodeHandle nhandle){
	string nm = "/joy";
	joysub = nhandle.subscribe(nm, 1, &Pucc::joyCallback, this);
}

void Pucc::subscribeTrajId(ros::NodeHandle nhandle){
	ROS_INFO("Subscribing to %s/trajId", name.c_str());
	trajIdSub = nhandle.subscribe(name+"/trajId", 1, &Pucc::trajIdCallback, this);
}

void Pucc::publishSpoofedPoseVel(){
	//publish the spoofed pose for this simulated bot.
	geometry_msgs::PoseStamped posemsgSpoof;
	posemsgSpoof.header.stamp = ros::Time::now();
	posemsgSpoof.header.frame_id = name;
	posemsgSpoof.pose.position.x = x;
	posemsgSpoof.pose.position.y = y;
	posemsgSpoof.pose.position.z = 0;
	posemsgSpoof.pose.orientation.x = 0;
	posemsgSpoof.pose.orientation.y = 0;
	posemsgSpoof.pose.orientation.z = sin(psi/2.0); // Conversions to quaternion representation
	posemsgSpoof.pose.orientation.w = cos(psi/2.0); // see http://wiki.alioth.net/index.php/Quaternion

	poseSpooferPub.publish(posemsgSpoof);

	geometry_msgs::TwistStamped velmsgSpoof;
	velmsgSpoof.header.stamp = ros::Time::now();
	velmsgSpoof.header.frame_id = name;
	velmsgSpoof.twist.linear.x = vx;
	velmsgSpoof.twist.linear.y = vy;
	velmsgSpoof.twist.linear.z = 0;
	velmsgSpoof.twist.angular.x = 0;
	velmsgSpoof.twist.angular.y = 0;
	velmsgSpoof.twist.angular.z = sin(dpsi/2.0); // Conversions to quaternion representation

	velSpooferPub.publish(velmsgSpoof);
}


