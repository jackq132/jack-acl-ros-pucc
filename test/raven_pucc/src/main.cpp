#include <sys/time.h>
#include <semaphore.h>
#include <signal.h>
#include <iostream>
#include <vector>
#include <string>
#include "ros/ros.h"
#include "raven_pucc/waypoint.h"
#include "raven_pucc/vrotcmd.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include <cmath> // for trig operations etc
#include <boost/thread.hpp>  
#include <boost/date_time.hpp>  


#include "MotionPlanner.h"
#include "motionplanners/PotFieldPlanner.h"
#include "motionplanners/orcaPlanner.h"
#include "Pucc.h"
#include "puccs/Gpucc.h"
//#include "puccs/Upucc.h"


using namespace std;

//raven boundaries
const double RAVEN_XMIN = -2.2;
const double RAVEN_XMAX =  2.2;
const double RAVEN_YMIN = -7.6;
const double RAVEN_YMAX =  3.0;

Pucc *robot;

enum RobotType{
	GPUCC,
	UPUCC
};
enum MotionPlannerType{
	POTENTIAL,
	ORCA
};

void processArgv(int argc, char** argv, string& puccname, string& portLoc, RobotType& rtype, MotionPlannerType& mptype, bool& useJoy, bool& isSim);

int main( int argc, char **argv ) {
	//flags:
	//--sim false/true  simulation/hardware (default true)
	//--joy false/true use joystick (default false)
	//--name abcds  specify the name  (required)
	//--type gpucc/upucc specify the type (default gpucc)
	//--motion potential/orca  specify the motion (default potential)
	//--port /dev/ttyUSB1 (default none)

	std::string puccname = "";
	std::string portLoc = "";
	RobotType rtype = GPUCC;
	MotionPlannerType mptype;	
	mptype = POTENTIAL;
	//mptype = ORCA;
	bool useJoy = false;
	bool isSim = true;
	processArgv(argc, argv, puccname, portLoc, rtype, mptype, useJoy, isSim);

	//initialize ROS and subscribe/advertise topics
	std::string nodename = "GP_controller_";
	nodename.append(puccname);
//	nodename.append("_controller");
	ros::init(argc, argv, nodename);
	ros::NodeHandle nhandle;


	//Error checking on command line call
	if (puccname.empty()){//make sure the name is set
		ROS_FATAL("Robot name required. Use --name [robot name] when calling pucc controller.");
		return EXIT_FAILURE;
	}
	if (!isSim && portLoc.empty()){//if it's in hardware mode, make sure the port is set
		ROS_FATAL("Port required for hardware robot. Use --port [dev loc] when calling pucc controller.");
		return EXIT_FAILURE;
	}
	if (!isSim){//if it's in hardware mode, make sure a RAVEN topic is active
		ros::master::V_TopicInfo allTopics;
		ros::master::getTopics(allTopics);
		bool foundThisTopic = false;
		for (int i = 0; i < allTopics.size(); i++){
    	    std::string topicName = allTopics[i].name; // e.g. /uP04/pose
			if (topicName.find(puccname) != string::npos && topicName.find("/pose") != string::npos){
				foundThisTopic = true;
				break;
			}
		}
		if (!foundThisTopic){
			ROS_FATAL("Robot name not found in RAVEN topics. Make sure --name [robot name] is set to a currently active RAVEN topic.");
			return EXIT_FAILURE;
		}
	}

	
	
	//create the robot
	switch(rtype){
		case GPUCC:
			robot = new Gpucc(puccname, isSim, useJoy);
			break;
		case UPUCC:
			ROS_FATAL("ERROR: UPUCC NOT IMPLEMENTED YET.");
			//robot = new Upucc(puccname, isSim, useJoy);
			break;
		default:
			ROS_FATAL("ERROR: Shouldn't ever get here - rtype cases should be exhaustive.");
			return EXIT_FAILURE;
			break;
	}

	//if the robot is hardware mode, start subscribing to raven and set up the hardware communication link
	if (!robot->isSimulated()) {
		robot->subscribePoseVel(nhandle); 
		robot->subscribeTrajId(nhandle);
		robot->advertiseIdleStatus(nhandle);
		robot->advertiseCurTrajId(nhandle);
		if(!robot->setupHardware(portLoc)){
			ROS_FATAL("Failed to initialize robot communication.");
			return EXIT_FAILURE;
		}
	} else { // if the robot is simulation, create fake raven publisher, and give the robot an initial position
		robot->advertisePoseVel(nhandle);
	    //give it an initial position
		Pose2D randomStoppedPose; 
		randomStoppedPose.vel = randomStoppedPose.dx = randomStoppedPose.dy = randomStoppedPose.dpsi = 0;
		boost::mt19937 rng;
		rng.seed(static_cast<unsigned int>(time(0)));
		boost::uniform_01<> u01;
		randomStoppedPose.x = RAVEN_XMIN + (RAVEN_XMAX-RAVEN_XMIN)*u01(rng);
		randomStoppedPose.y = RAVEN_YMIN + (RAVEN_YMAX-RAVEN_YMIN)*u01(rng);
		randomStoppedPose.psi = -M_PI + 2*M_PI*u01(rng);
		robot->setPoseVel(randomStoppedPose);
    }

	//if the robot is not listening to joystick commands, create a motion planner and subscribe to waypoint/velrot commands, and give initial waypoint at current loc
	if (!robot->useJoystick()){
		MotionPlanner* mp;
		switch(mptype){
			case POTENTIAL:
				ROS_INFO("Using PotentialField planner");
				mp = new PotFieldPlanner(nhandle, robot->getParams());
				break;
			case ORCA:
				ROS_INFO("Using ORCA planner");
				mp = new orcaPlanner(nhandle, robot->getParams());
				mp->setName(puccname);
				mp->advertiseGoal(nhandle);
				mp->subscribeCarrotList(nhandle);
				break;
			default:
				ROS_FATAL("ERROR: Shouldn't ever get here - mptype cases should be exhaustive.");
				return EXIT_FAILURE;
				break;
		}
		
		mp->setBoundaries(RAVEN_XMIN, RAVEN_XMAX, RAVEN_YMIN, RAVEN_YMAX);
		robot->setMotionPlanner(mp);
		robot->subscribeWayptVPsidot(nhandle);
		//## Give an initial goal at current location
		ros::spinOnce(); // Get first waypoint (current robot position)
		Pose2D curPos;
		robot->getPose(curPos);
		curPos.dx = curPos.dy = curPos.dpsi = curPos.vel = 0;
		robot->addWaypoint(true, curPos); // Magic Number 0.0 is the desired velocity at goal
	} else {//if the robot is listening to joystick, just subscribe to the joystick messages
		robot->subscribeJoy(nhandle);
	}

	//## Welcome msg
	ROS_INFO("Starting %s pucc_controller for %s...", (robot->isSimulated() ? "simulation" : "hardware") , robot->getName().c_str() );

	//**************************************************
	//## MAIN LOOP
	//**************************************************
	ROS_INFO("Starting main control loop for %s...", puccname.c_str());
	ros::Time curTime, prevTime;
	prevTime = ros::Time::now();
	const unsigned int LOOP_RATE = 25, TOPIC_UPDATE_RATE = 1, INFO_OUTPUT_RATE = 5;
	const unsigned int TOPIC_UPDATE_PERIOD = LOOP_RATE / TOPIC_UPDATE_RATE;
	const unsigned int INFO_OUTPUT_PERIOD = LOOP_RATE / INFO_OUTPUT_RATE;
	ros::Rate loop_timer(LOOP_RATE);
	unsigned int loopCount = 0;
	
	int colorId;

	while( ros::ok() ) {
		colorId = rand();		
		if (loopCount % INFO_OUTPUT_PERIOD == 0){
			Pose2D pp, ppg;
			robot->getPose(pp);
			robot->getGoal(ppg);
			ROS_INFO((robot->getName() + " -- x: %.3f y: %.3f psi: %.3f dx: %.3f dy: %.3f dpsi: %.3f").c_str(), pp.x, pp.y, pp.psi, pp.dx, pp.dy, pp.dpsi);
			ROS_INFO((robot->getName() + " -- Goal: x: %.3f y: %.3f psi: %.3f vel: %.3f").c_str(), ppg.x, ppg.y, ppg.psi, ppg.vel);
		}
		// Update obstacle list infrequently
		if (loopCount % TOPIC_UPDATE_PERIOD == 0) {
			robot->updateObstacleList(nhandle);
		}

		ros::spinOnce(); //do callbacks
		//run the motion planner
		if (!robot->useJoystick()){
			if (mptype == ORCA){
				robot->updateDriveCommands(false);
 			} else {
				robot->updateDriveCommands(true);				
			}
		}
		//if in hardware, send commands
		//if in simulation, simulate the dynamics
		if (!robot->isSimulated()){
			robot->sendDriveCommands();
			//boost::thread thread1( boost::bind( &Pucc::ledBarPwm, &robot ) );
    			//boost::thread workerThread(robot->ledBarPwm()); 
			
		} else {
			//simulate
			curTime = ros::Time::now();
			double dt = curTime.sec + curTime.nsec/1.0e9 - prevTime.sec - prevTime.nsec/1.0e9;
			prevTime = curTime;
			robot->simulateDynamics(dt);
			robot->publishSpoofedPoseVel();
		}

		//do anything else specific to this type of robot
		robot->puccMain(nhandle);

		//loop maintenance
		++loopCount;
		loop_timer.sleep();

		

	} // end while(rosok)

	//## Close up shop
	ROS_INFO("Shutting down pucc_controller for %s", puccname.c_str());
	delete(robot);
	//## Return
	return EXIT_SUCCESS;
}



void processArgv(int argc, char** argv, string& puccname, string& portLoc, RobotType& rtype, MotionPlannerType& mptype, bool& useJoy, bool& isSim){
	int argvit = 0;
	while (argvit < argc-1){ //-1 since each command line option is of the format --xxx yyy
		if (strncmp(argv[argvit], "--name", 5) == 0){
			puccname = argv[argvit+1];
			argvit += 2;
		} else if (strncmp(argv[argvit], "--sim", 5) == 0){
			isSim = strncmp(argv[argvit+1], "false", 5)==0 ? false : true;
			argvit += 2;
		} else if (strncmp(argv[argvit], "--joy", 5) == 0){
			useJoy = strncmp(argv[argvit+1], "true", 4)==0 ? true : false;
			argvit += 2;
		} else if (strncmp(argv[argvit], "--motion", 8) == 0){
			if (strncmp(argv[argvit+1], "potential", 9) == 0){
				mptype = POTENTIAL;
			}else if (strncmp(argv[argvit+1], "orca", 5) == 0){
				mptype = ORCA;
			}
			argvit += 2;
		} else if (strncmp(argv[argvit], "--type", 6) == 0){
			if (strncmp(argv[argvit+1], "upucc", 5) == 0){
				rtype = UPUCC;
			} else if (strncmp(argv[argvit+1], "gpucc", 5) == 0){
				rtype = GPUCC;
			}
			argvit += 2;
		} else if (strncmp(argv[argvit], "--port", 6) == 0){
			portLoc = argv[argvit+1];
			argvit += 2;
		} else {
			//if it isn't any of the normal switches, just increment
			argvit++;
		}
	}
	return;
}
