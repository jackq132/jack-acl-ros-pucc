/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 *
 */

#include "PotFieldPlanner.h"

//Integrator::Integrator()
//{
//        value = 0;
//        absLimit = 5.0;
//}
//
//Integrator::Integrator(double limit)
//{
//        value = 0;
//        absLimit = limit;
//}
//
//void Integrator::increment (double inc, double dt)
//{
//        if(fabs(value) < absLimit)
//                value+=inc*dt;
//}
//
//void Integrator::reset()
//{
//        value = 0;
//}


PotFieldPlanner::PotFieldPlanner(ros::NodeHandle &nhandle, ParameterMap params) {
	//required parameters
	SPEED_LIMIT 			= params.get("SPEED_LIMIT");
	TURNRATE_LIMIT 			= SPEED_LIMIT/(params.get("WHEEL_BASE")/2.0);
	WP_TOL 				= params.get("WP_TOL");
	KR 				= params.get("KR");
	KTH 				= params.get("KTH");
	TLOOK 				= params.get("TLOOK");
	WRISK 				= params.get("WRISK");
	RADIUS 				= params.get("RADIUS");
}

PotFieldPlanner::~PotFieldPlanner() {
}

void PotFieldPlanner::controller(Carrot carrot, Pose2D pose, double& velref, double& dpsiref){
    	double pos_err = dist(carrot.x-pose.x,carrot.y-pose.y); // distance to goal
    	double th_ref = atan2(carrot.y - pose.y,carrot.x - pose.x);
	double th_err = wrap(th_ref - pose.psi);

	// Low-pass velocity filter
	//velFilt = velFilt + (1.0/20.0 * (pos.vel-velFilt));
	//double vel_err = carrot.vel - velFilt;
	// Simple proportional heading controller (inner loop -- faster gain)
	dpsiref = saturate(KTH*th_err, -TURNRATE_LIMIT, TURNRATE_LIMIT);
	// Simple proportional range controller (outer loop -- slower gain)
	double lowerbd = min(2.0f*fabs(carrot.vel), fabs(SPEED_LIMIT));
	velref = saturate(KR*pos_err,-lowerbd, lowerbd);
	return;
}

void PotFieldPlanner::advertiseGoal(ros::NodeHandle nhandle){}

void PotFieldPlanner::subscribeCarrotList(ros::NodeHandle nhandle){};

void PotFieldPlanner::plan(Pose2D curpose, vector<Obstacle*> obstacles, queue<Pose2D>& wpts, double& velref, double& dpsiref){
	if (!wpts.empty()){
		ROS_INFO("Planning for carrot");
		
		//if a CLOSE obstacle is standing on the next waypoint,
		// and is not moving fast enough to leave the waypoint in time, 
		// then project the waypoint outward from the obstacle
		wpts.front() = moveWptIfBlocked(curpose, wpts.front(), obstacles);
		//ROS_INFO("wpts.front: (%f, %f)", wpts.front().x, wpts.front().y);

		Carrot carrot = carrotGen(curpose, wpts.front(), obstacles);
		ROS_INFO("Carrot (x,y,vel): (%f, %f, %f)", carrot.x, carrot.y, carrot.vel);
		controller(carrot, curpose, velref, dpsiref);
		//vel/dpsi are already saturated here by controller
	} else {
		velref = 0;
		dpsiref = 0;
	}
	return;
}

Pose2D PotFieldPlanner::moveWptIfBlocked(Pose2D pose, Pose2D goal, vector<Obstacle*> obstacles){
	double obstDistBuffer  = 0.01 + 4.0*RADIUS; // buffer in which an obstacle is checked for covering the goal
	double obstGoalCoverBuffer = 2.5*RADIUS; // red flag is thrown when a goal is this close to a nearby obstacle
		
	for(vector<Obstacle*>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
		Obstacle* obst = *it;
		double distPoseToObst = dist(obst->y-pose.y, obst->x-pose.x);
		double distGoalToObst = dist(obst->y-goal.y, obst->x-goal.x);
		
		//only check obstacle if it's close. No point checking whether a far obstacle covers the waypoint, since chances are it will move away in time.
		if ((distPoseToObst < obstDistBuffer) && (distGoalToObst < obstGoalCoverBuffer)){
			//check if obstacle can move away in time
			double nextx = obst->x + TLOOK*obst->dx;
			double nexty = obst->y + TLOOK*obst->dy;
			double distGoalToFutureObst = dist(nextx - goal.x, nexty - goal.y);
			if (distGoalToFutureObst < obstGoalCoverBuffer){
				ROS_INFO("Goal is blocked by %s (x,y): (%f, %f)", obst->name.c_str(), obst->x, obst->y);
				Pose2D obstPos;
				obstPos.x = obst->x;
				obstPos.y = obst->y;
				return moveObstructedWpt(pose, goal, obstPos, obstGoalCoverBuffer);                        	
			}
		}
	}
	//don't change the goal if it's clear
	return goal;
}

Pose2D PotFieldPlanner::moveObstructedWpt(Pose2D pose, Pose2D goal, Pose2D obst, double bufferDist){
	Pose2D v1 = obst;
	Pose2D v2 = pose;
	Pose2D v3 = goal;

	ROS_INFO("obst: (%f, %f)", obst.x, obst.y);
	ROS_INFO("pose: (%f, %f)", pose.x, pose.y);
	ROS_INFO("goal: (%f, %f)", goal.x, goal.y);

	Pose2D v12;
	v12.x = v2.x - v1.x;
	v12.y = v2.y - v1.y;
	double v12Mag = sqrt(pow(v12.x,2) + pow(v12.y,2));

	Pose2D v13; 
	v13.x = v3.x - v1.x;
	v13.y = v3.y - v1.y;

	Pose2D v14;
	v14.x = v12.x* (v13.x*v12.x + v13.y*v12.y)/pow(v12Mag,2);
	v14.y = v12.y* (v13.x*v12.x + v13.y*v12.y)/pow(v12Mag,2);
	double v14Mag = sqrt(pow(v14.x,2) + pow(v14.y,2));

	Pose2D v43;
	v43.x = v3.x - v1.x - v14.x;
	v43.y = v3.y - v1.y - v14.y;
	double v43Mag = sqrt(pow(v43.x,2) + pow(v43.y,2));

	double h = cos(v14Mag*M_PI/(2.0f*(1.2f*bufferDist)))*(1.2f*bufferDist);

	Pose2D v4f;
	v4f.x = h/v43Mag*v43.x;
	v4f.y = h/v43Mag*v43.y;

	Pose2D vf = goal; //there are some goal params such as vel that we'd like to retain
	
	//we randomly choose the direction to project, this gives us an opportunity to escape locked positions
	double r = ((double) rand() / (RAND_MAX));

	if (r<0.5){
		vf.x = v1.x + v14.x + v4f.x;
		vf.y = v1.y + v14.y + v4f.y;
	} else{
		vf.x = v1.x + v14.x - v4f.x;
		vf.y = v1.y + v14.y - v4f.y;
	}

	ROS_INFO ("Moved waypoint to vf (%f, %f)", vf.x, vf.y);

	return vf;
}

void PotFieldPlanner::plan(Pose2D curpose, vector<Obstacle*> obstacles, double veldes, double dpsides, double& velref, double& dpsiref){
	velref = saturate(veldes, -SPEED_LIMIT, SPEED_LIMIT);
	dpsiref = saturate(dpsides, -TURNRATE_LIMIT, TURNRATE_LIMIT);
	return;
}

bool PotFieldPlanner::reachedWaypoint(double dx, double dy){
	return dist(dx,dy) < WP_TOL;
}

/* Note that objects farther away than 'drone_buffer' don't even get penalized */

Carrot PotFieldPlanner::carrotGen(Pose2D pose, Pose2D goal, std::vector<Obstacle*> obstacles){
	// Planner constants
	double drone_buffer	= 0.1 + 1.2*RADIUS;	// Add drone radius - only penalized inside this buffer
	double wall_buffer 	= 0.03 + RADIUS;	// Add static object radius (wall and around vehicles)
	double t_ahead  	= TLOOK;    		// seconds
	double r_ahead      	= t_ahead*SPEED_LIMIT;	// m, 0.2 = max_vel
	double w_risk		= WRISK;		// Risk knob (larger == more risk averse)
	double penalty   	= 20.0;			// Penalty at wall boundary region
	double turnPenalty 	= 0.01; 			//penalty for turning
	int    N_PTS  		= 64;			////BOB Appears to be # of angular points (64 lines directed out)
	int    R_PTS		= 64;			////BOB Appears to be # of radial point (64 circles radius r_ahead out)
							////Bob can't be both, greedy Bob.

	double best_potential    = numeric_limits<double>::infinity();	// Best cost
	double best_th = 0.0;			// Best theta
	double best_r  = 0.0;			// Best range

	// Carrot starts as unobsructed
	Carrot carrot;

	double dx_err = goal.x-pose.x;
	double dy_err = goal.y-pose.y;

	
	if ( isBlocked(pose, obstacles) || dist(dx_err, dy_err) < WP_TOL){
		ROS_INFO("isBlocked");
		ROS_INFO("Pose.psi: %f", pose.psi);
		carrot.dpsi = 0;
		if ( dist(dx_err, dy_err) < WP_TOL){
			carrot.x = pose.x;
			carrot.y = pose.y;
			carrot.vel = carrot.dx = carrot.dy = 0.0;
			carrot.psi = goal.psi;
		} else {
			int backwardMult;
			if (pose.psi>-M_PI/2 && pose.psi<M_PI/2){
				backwardMult = -1;
			} else {
				backwardMult = 1;
			}
			carrot.x = pose.x+backwardMult*0.2*drone_buffer;
			carrot.y = pose.y+backwardMult*0.2*drone_buffer;
			carrot.vel = carrot.dx = carrot.dy = 0.1;
			carrot.psi = -pose.psi;
		}
		return carrot;
	}

	//Iterate around radially with resolution 2pi / N_PTS
	for(int i=0;i<N_PTS;i++) {
		for(int j=0;j<R_PTS;j++) { // Iterate outwards
			// Point around you

			double x_i = pose.x + r_ahead*((double)j/((R_PTS-1)*1.0))*cos(2*M_PI*i/(N_PTS*1.0));
			double y_i = pose.y + r_ahead*((double)j/((R_PTS-1)*1.0))*sin(2*M_PI*i/(N_PTS*1.0));

			// Waypoint potential
			// Initial potential field linearly increases around goal, giant bowl shape around goal.
			// Potential at each point i,j is initialized as such.
			double potential = dist(goal.x-x_i,goal.y-y_i);

			// Are we close to any walls? If so, penalize...
			// If we start inside the box - we'll never get out!
			// If we start outside the box - we'll never get in!
			double d = fabs(x_i-xmin);
			if(d < wall_buffer) {
				potential += penalty;
			}
			d = fabs(x_i-xmax);
			if(d < wall_buffer) {
				potential += penalty;
			}
			d = fabs(y_i-ymin);
			if(d < wall_buffer) {
				potential += penalty;
			}
			d = fabs(y_i-ymax);
			if(d < wall_buffer) {
				potential += penalty;
			}

			// For each obstacle (static/dynamic unimportant, the below is general to any obstacle velocity)
			for(vector<Obstacle*>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
				Obstacle* obst = *it;
				//ROS_INFO("Obstacle %s: x = %.3f, y = %.3f, r = %.3f", obst->name.c_str(), obst->x, obst->y, obst->radius);
				// Pull out obstacle info...
				double d = dist( obst->x-x_i, obst->y-y_i);

				// If we're in the obstacle's halo, penalize...
				if ( (d < (obst->radius + drone_buffer)) && (obst->z < .4) ) { //if obstacle is near ground
					potential += w_risk*penalty/2 * pow(drone_buffer + obst->radius - d,2);
				}

				// Moving?
				double nextx = obst->x + t_ahead*obst->dx;
				double nexty = obst->y + t_ahead*obst->dy;
				d = dist(nextx- x_i, nexty - y_i);

				// If we're in the obstacle's halo and its altitude is low, penalize...
				if( (d < obst->radius + drone_buffer) && (obst->z < .4) ) {
					potential += w_risk*penalty/2 * pow(drone_buffer + obst->radius - d,2);
				}
			}

			double theta = 2.0*M_PI*(double)i/((double)N_PTS);
			potential += abs(theta - pose.psi) * turnPenalty;
//			ROS_INFO("turn potential %f, added %f",potential [i][j], abs(theta - pos.psi) * turnPenalty);

			if(potential < best_potential) {
				//ROS_INFO("Better Potential = %.3f at %d, %d\n",potential[i][j], i, j);
				best_potential    = potential;
				best_th = theta;
				best_r  = r_ahead*((double)j/((R_PTS-1)*1.0));
			}
		}
	}

	// Find the point with minimum potential
	carrot.x = pose.x + best_r*cos(best_th);
	carrot.y = pose.y + best_r*sin(best_th);
	carrot.psi = goal.psi;
	carrot.vel = goal.vel * (best_r / r_ahead);//saturate((SLOW_SPEED/estSpeed*2.0),0.0,1.0);
	//ROS_INFO("Carrot x = %.3f, Carrot y = %.3f, Carrot psi = %.3f, Carrot vel = %.3f, best_th = %.3f, best_r = %.3f, best_potential = %.3f", carrot.x , carrot.y, carrot.psi, carrot.vel, best_th,best_r, best_potential);
	//ROS_INFO("Goal x = %.3f, Goal y = %.3f, Goal psi = %.3f, Goal vel = %.3f", goal.x , goal.y, goal.psi , goal.vel);
	return carrot;
}

bool PotFieldPlanner::isBlocked(Pose2D pose, vector<Obstacle*> obstacles){
	/********* Check if a robot needs to stop *********/
	double stop_buffer  = 0.01 + 2.0*RADIUS; // buffer in which GPUCC stops
	int N_PTS = 64; //appears to be # of angular points (64 lines directed out)
							
	for(vector<Obstacle*>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {//check all obstacles
		Obstacle* obst = *it;
		for(int i=0;i<N_PTS;i++) { // Iterate over angles front half
			double hdgToObst = wrap(atan2(obst->y - pose.y, obst->x - pose.x) - pose.psi);
			double distToObst = dist(obst->y-pose.y, obst->x-pose.x);
			bool obstIsBehind = hdgToObst > M_PI/2.0 || hdgToObst < -M_PI/2.0;
			bool obstIsAbove = obst->z > 0.4;
			//ROS_INFO("Robot %d is behind %d: %s",myID,currentObst->id, isBehind ? "true" : "false");
			if (!obstIsBehind && !obstIsAbove && (distToObst < (obst->radius + 0.05 + stop_buffer * cos(hdgToObst)))) { // need large distance ahead, small on sides
					return true;
			}
		}
	}
	return false;
}
