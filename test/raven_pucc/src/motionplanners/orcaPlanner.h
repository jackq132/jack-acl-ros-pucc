/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 *
 */

#ifndef orcaPlanner_H_
#define orcaPlanner_H_

#include "ros/ros.h" // ONLY NECESSARY if carrots are published, which at moment, they are not.
#include "../Pucc.h"
#include "../MotionPlanner.h"
#include "../ParameterMap.h"
#include "../Util.h"
#include <deque>


typedef Pose2D Carrot;

//class Integrator{
//	public:
//		double value;
//		Integrator();
//		Integrator(double limit);
//		void reset();
//		void increment(double y, double dx);
//	private:
//		double absLimit;
//};

class orcaPlanner : public MotionPlanner {
public:

	double SPEED_LIMIT;
	double TURNRATE_LIMIT;
	double WP_TOL;
	double KR;
	double KTH;
	double TLOOK;
	double WRISK;
	double RADIUS;
	bool isOrcaProcessingGoal;
	bool isNewCarrot;
	double tLeft;
	ros::Time tStart;
	deque<Carrot> carrotList;

	orcaPlanner(ros::NodeHandle &nhandle, ParameterMap params);
	virtual ~orcaPlanner();
	
	ros::Subscriber carrotListSub, goalSub;
    	ros::Publisher goalPub;

	void carrotListCallback(const raven_pucc::carrots &msg);
	void goalCallback(const raven_pucc::goal &msg);

	
	//child methods for abstract class methods
	virtual void subscribeCarrotList(ros::NodeHandle nhandle);

	virtual void advertiseGoal(ros::NodeHandle nhandle);
	virtual void plan(Pose2D curpose, vector<Obstacle*> obstacles, queue<Pose2D>& wpts, double& velref, double& dpsiref);
	virtual void plan(Pose2D curpose, vector<Obstacle*> obstacles, double veldes, double dpsides, double& velref, double& dpsiref);
	virtual bool reachedWaypoint(double dx, double dy);
	virtual bool reachedCarrot(double dx, double dy);

	Carrot carrotGen(Pose2D pose, Pose2D goal, std::vector<Obstacle*> obstacles);
	void controller(Carrot carrot, Pose2D pos, double& velref, double& dpsiref);
	void controllerOrca(Carrot carrot, Pose2D pos, double& velref, double& dpsiref);
	bool isBlocked(Pose2D pose, vector<Obstacle*> obstacles);

};

#endif /* orcaPlanner_H_ */
