/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 *
 */

#ifndef POTFIELDPLANNER_H_
#define POTFIELDPLANNER_H_

#include "ros/ros.h" // ONLY NECESSARY if carrots are published, which at moment, they are not.
#include "../Pucc.h"
#include "../MotionPlanner.h"
#include "../ParameterMap.h"
#include "../Util.h"

typedef Pose2D Carrot;

//class Integrator{
//	public:
//		double value;
//		Integrator();
//		Integrator(double limit);
//		void reset();
//		void increment(double y, double dx);
//	private:
//		double absLimit;
//};

class PotFieldPlanner : public MotionPlanner {
public:

	double SPEED_LIMIT;
	double TURNRATE_LIMIT;
	double WP_TOL;
	double KR;
	double KTH;
	double TLOOK;
	double WRISK;
	double RADIUS;

	PotFieldPlanner(ros::NodeHandle &nhandle, ParameterMap params);
	virtual ~PotFieldPlanner();
	
	//child methods for abstract class methods
	virtual void subscribeCarrotList(ros::NodeHandle nhandle);
	virtual void advertiseGoal(ros::NodeHandle nhandle);
	virtual void plan(Pose2D curpose, vector<Obstacle*> obstacles, queue<Pose2D>& wpts, double& velref, double& dpsiref);
	virtual void plan(Pose2D curpose, vector<Obstacle*> obstacles, double veldes, double dpsides, double& velref, double& dpsiref);
	virtual bool reachedWaypoint(double dx, double dy);

	Carrot carrotGen(Pose2D pose, Pose2D goal, std::vector<Obstacle*> obstacles);
	void controller(Carrot carrot, Pose2D pos, double& velref, double& dpsiref);
	bool isBlocked(Pose2D pose, vector<Obstacle*> obstacles);
	Pose2D moveWptIfBlocked(Pose2D pose, Pose2D goal, vector<Obstacle*> obstacles);
	Pose2D moveObstructedWpt(Pose2D pose, Pose2D goal, Pose2D obst, double bufferDist);

};

#endif /* POTFIELDPLANNER_H_ */
