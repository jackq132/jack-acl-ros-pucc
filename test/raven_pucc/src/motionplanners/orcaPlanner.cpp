/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 *
 */

#include "orcaPlanner.h"

//Integrator::Integrator()
//{
//        value = 0;
//        absLimit = 5.0;
//}
//
//Integrator::Integrator(double limit)
//{
//        value = 0;
//        absLimit = limit;
//}
//
//void Integrator::increment (double inc, double dt)
//{
//        if(fabs(value) < absLimit)
//                value+=inc*dt;
//}
//
//void Integrator::reset()
//{
//        value = 0;
//}


orcaPlanner::orcaPlanner(ros::NodeHandle &nhandle, ParameterMap params) {
	//required parameters
	SPEED_LIMIT 			= params.get("SPEED_LIMIT");
	TURNRATE_LIMIT 			= SPEED_LIMIT/(params.get("WHEEL_BASE")/2.0);
	WP_TOL 				= params.get("WP_TOL");
	KR 				= params.get("KR");
	KTH 				= params.get("KTH");
	TLOOK 				= params.get("TLOOK");
	WRISK 				= params.get("WRISK");
	RADIUS 				= params.get("RADIUS");
	isOrcaProcessingGoal = false;
	isNewCarrot = true;
}

orcaPlanner::~orcaPlanner() {
}

void orcaPlanner::controller(Carrot carrot, Pose2D pose, double& velref, double& dpsiref){
    	double pos_err = dist(carrot.x-pose.x,carrot.y-pose.y); // distance to goal
    	double th_ref = atan2(carrot.y - pose.y,carrot.x - pose.x);
	double th_err = wrap(th_ref - pose.psi);

	// Low-pass velocity filter
	//velFilt = velFilt + (1.0/20.0 * (pos.vel-velFilt));
	//double vel_err = carrot.vel - velFilt;
	// Simple proportional heading controller (inner loop -- faster gain)
	dpsiref = saturate(KTH*th_err, -TURNRATE_LIMIT, TURNRATE_LIMIT);
	// Simple proportional range controller (outer loop -- slower gain)
	double lowerbd = min(fabs(carrot.vel), fabs(SPEED_LIMIT));
	velref = (double) saturate(KR*pos_err,-lowerbd, lowerbd);
	return;
}

void orcaPlanner::controllerOrca(Carrot carrot, Pose2D pose, double& velref, double& dpsiref){
    	double pos_err = dist(carrot.x-pose.x,carrot.y-pose.y);
    	double th_ref = atan2(carrot.y - pose.y,carrot.x - pose.x);
	double th_err = wrap(th_ref - pose.psi);
	
	// Simple proportional heading controller (inner loop -- faster gain)
	dpsiref = saturate(KTH*th_err, -TURNRATE_LIMIT, TURNRATE_LIMIT);
	// Simple proportional range controller (outer loop -- slower gain)
	double lowerbd = min(fabs(carrot.vel), fabs(SPEED_LIMIT));
	velref = lowerbd; //(double) saturate(KR*pos_err,-lowerbd, lowerbd);
	return;
}

void orcaPlanner::advertiseGoal(ros::NodeHandle nhandle){
	goalPub = nhandle.advertise<raven_pucc::goal>("/orca/goal", 1);
}

void orcaPlanner::goalCallback(const raven_pucc::goal &msg){
	//if another agent publishes a goal, we immediately stop.
	//otherwise, we'd move forward to pose(t+dt), while orca plans using pose(t),
	//and we'd end up having to go back to pose(t) (causes gpucc to spin around chaotically)
	carrotList.clear();
}

void orcaPlanner::carrotListCallback(const raven_pucc::carrots &msg){
	ROS_INFO("Received carrots from ORCA!!!!!!!!!!!!!!!!!!!!!!!");
	Carrot curCarrot;
	carrotList.clear();
	for( std::vector<geometry_msgs::Pose2D>::const_iterator i = msg.carrots.begin(); i !=msg.carrots.end(); ++i){
		geometry_msgs::Pose2D curMsg = *i;
		curCarrot.x = curMsg.x;
		curCarrot.y = curMsg.y;
		carrotList.push_back(curCarrot);
		ROS_INFO("(%f, %f)", curCarrot.x, curCarrot.y);	
	}

	isOrcaProcessingGoal = false;
	isNewCarrot = true;


	//ROS_INFO("%d", msg[0].x);

//	int numCarrots = (sizeof(msg.carrots)/sizeof(*msg.carrots));
//	Carrot[] carrots = msg.carrots;
//	carrotList.assign(msg.carrots, msg.carrots+numCarrots);
}

void orcaPlanner::subscribeCarrotList(ros::NodeHandle nhandle){
	ROS_INFO("Subbed to %s", name.c_str());
	carrotListSub = nhandle.subscribe("/" + name + "/carrotList", 1, &orcaPlanner::carrotListCallback, this);
	//also subscribe to /orca/goal, since use of /GP##/carrotList is equivalent to using /orca/goal
	goalSub = nhandle.subscribe("/orca/goal", 1, &orcaPlanner::goalCallback, this);
}

void orcaPlanner::plan(Pose2D curpose, vector<Obstacle*> obstacles, queue<Pose2D>& wpts, double& velref, double& dpsiref){
	if (!wpts.empty()){
		//ROS_INFO("plan() getting called");
		//ROS_INFO("wpts.size(): %d", wpts.size());
		//empty carrotList. Just woke up, or just finished a goal. Ask ORCA for new carrotList.	
		ROS_INFO("Carrot list size: %d", carrotList.size());
		if (carrotList.size() == 0){
			//don't keep sending the goal if ORCA is trying to process it
			if (!isOrcaProcessingGoal){
				raven_pucc::goal newGoal;
 				newGoal.name = name;
				newGoal.goal.x = wpts.front().x; 
				newGoal.goal.y = wpts.front().y;
				goalPub.publish(newGoal);
				isOrcaProcessingGoal = true;
				ROS_INFO("Called orca with new goal (x,y): (%f, %f)", newGoal.goal.x, newGoal.goal.y);
			}
			//do nothing while ORCA is processing (soon, main() loop will call plan() again to check ORCA's response)
			velref = 0;
			dpsiref = 0;
		} else {
			double timeStepOrca = 0.25f; //TODO: make this update based on orca's value
			//carrotList may change, even if you didn't call ORCA. This happens if another agent calls ORCA, giving all agents a new carrotList
			Carrot carrot = carrotList.front();
			//carrot.vel = max(0.04, 0.3*dist(carrot.x-curpose.x,carrot.y-curpose.y)/0.3);

			if (isNewCarrot){
				tStart = ros::Time::now();
				isNewCarrot = false;
			}

			tLeft = timeStepOrca - (ros::Time::now() - tStart).toSec();
			
			//velocity calculations
			if (tLeft <0.00001f){
				carrot.vel=fabs(SPEED_LIMIT);
			} else if(reachedCarrot(carrot.x-curpose.x, carrot.y-curpose.y)){
				velref = 0;
				dpsiref = 0;
				return;
			} else {
				//numerator and denom. here scale linearly, so this value should be nearly constant
				carrot.vel = dist(carrot.x-curpose.x,carrot.y-curpose.y)/tLeft;
			}

			//ROS_INFO("Checking distance to carrot");
			if (reachedCarrot(carrot.x-curpose.x, carrot.y-curpose.y)) {
				carrotList.pop_front();
				isNewCarrot = true;
				ROS_INFO("Deleted carrot");
				//completed waypoint (delete here to avoid resending same waypoint as goal)
				if (reachedWaypoint(wpts.front().x-curpose.x, wpts.front().y-curpose.y)) {
					ROS_INFO("Waypoint reached and deleted");
					wpts.pop();
					carrotList.clear();
				}
			}
			controllerOrca(carrot, curpose, velref, dpsiref);
		}
	} else {
		velref = 0;
		dpsiref = 0;
	}
	return;
}

void orcaPlanner::plan(Pose2D curpose, vector<Obstacle*> obstacles, double veldes, double dpsides, double& velref, double& dpsiref){
	velref = saturate(veldes, -SPEED_LIMIT, SPEED_LIMIT);
	dpsiref = saturate(dpsides, -TURNRATE_LIMIT, TURNRATE_LIMIT);
	return;
}

//the final carrot in carrotList should, technically, be equal to the waypoint that was send to ORCA
//this is not actually the case. The final carrot is usually slightly different from the true waypoint
//therefore, the waypoint checker should have a higher tolerance than the carrot checker
bool orcaPlanner::reachedCarrot(double dx, double dy){
	return (dist(dx,dy) < WP_TOL*0.5);
}

bool orcaPlanner::reachedWaypoint(double dx, double dy){
	ROS_INFO("WP_TOL: %f", WP_TOL);
	ROS_INFO("dist: %f", dist(dx,dy));
	return (dist(dx,dy) < WP_TOL);
}

/* Note that objects farther away than 'drone_buffer' don't even get penalized */
/*
Carrot orcaPlanner::carrotGen(Pose2D pose, Pose2D goal, std::vector<Obstacle*> obstacles){


	// Planner constants
	double drone_buffer	= 0.1 + 2.1*RADIUS;	// Add drone radius - only penalized inside this buffer
	double wall_buffer 	= 0.03 + RADIUS;	// Add static object radius (wall and around vehicles)
	double t_ahead  	= TLOOK;    		// seconds
	double r_ahead      = t_ahead*SPEED_LIMIT;	// m, 0.2 = max_vel
	double w_risk		= WRISK;		// Risk knob (larger == more risk averse)
	double penalty   	= 20.0;			// Penalty at wall boundary region
	double turnPenalty = 0.01; 			//penalty for turning
	int    N_PTS  		= 64;			////BOB Appears to be # of angular points (64 lines directed out)
	int    R_PTS		= 64;			////BOB Appears to be # of radial point (64 circles radius r_ahead out)
							////Bob can't be both, greedy Bob.

	double best_potential    = numeric_limits<double>::infinity();	// Best cost
	double best_th = 0.0;			// Best theta
	double best_r  = 0.0;			// Best range

	// Carrot starts as unobsructed
	Carrot carrot;

	double dx_err = goal.x-pose.x;
	double dy_err = goal.y-pose.y;
	if ( isBlocked(pose, obstacles) || dist(dx_err, dy_err) < WP_TOL){
		ROS_INFO("isBlocked");
	//if ( dist(dx_err, dy_err) < WP_TOL){
		carrot.x = pose.x;
		carrot.y = pose.y;
		carrot.vel = carrot.dx = carrot.dy = 0;
		carrot.dpsi = 0;
		carrot.psi = goal.psi;
		return carrot;
	}

	//Iterate around radially with resolution 2pi / N_PTS
	for(int i=0;i<N_PTS;i++) {
		for(int j=0;j<R_PTS;j++) { // Iterate outwards
			// Point around you

			double x_i = pose.x + r_ahead*((double)j/((R_PTS-1)*1.0))*cos(2*M_PI*i/(N_PTS*1.0));
			double y_i = pose.y + r_ahead*((double)j/((R_PTS-1)*1.0))*sin(2*M_PI*i/(N_PTS*1.0));

			// Waypoint potential
			// Initial potential field linearly increases around goal, giant bowl shape around goal.
			// Potential at each point i,j is initialized as such.
			double potential = dist(goal.x-x_i,goal.y-y_i);

			// Are we close to any walls? If so, penalize...
			// If we start inside the box - we'll never get out!
			// If we start outside the box - we'll never get in!
			// :O omg omg omg omg
			double d = fabs(x_i-xmin);
			if(d < wall_buffer) {
				potential += penalty;
			}
			d = fabs(x_i-xmax);
			if(d < wall_buffer) {
				potential += penalty;
			}
			d = fabs(y_i-ymin);
			if(d < wall_buffer) {
				potential += penalty;
			}
			d = fabs(y_i-ymax);
			if(d < wall_buffer) {
				potential += penalty;
			}

			// For each obstacle (static/dynamic unimportant, the below is general to any obstacle velocity)
			for(vector<Obstacle*>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
				Obstacle* obst = *it;
				//ROS_INFO("Obstacle %s: x = %.3f, y = %.3f, r = %.3f", obst->name.c_str(), obst->x, obst->y, obst->radius);
				// Pull out obstacle info...
				double d = dist( obst->x-x_i, obst->y-y_i);

				// If we're in the obstacle's halo, penalize...
				if ( (d < (obst->radius + drone_buffer)) && (obst->z < .4) ) { //if obstacle is near ground
					potential += w_risk*penalty/2 * pow(drone_buffer + obst->radius - d,2);
				}

				// Moving?
				double nextx = obst->x + t_ahead*obst->dx;
				double nexty = obst->y + t_ahead*obst->dy;
				d = dist(nextx- x_i, nexty - y_i);

				// If we're in the obstacle's halo and its altitude is low, penalize...
				if( (d < obst->radius + drone_buffer) && (obst->z < .4) ) {
					potential += w_risk*penalty/2 * pow(drone_buffer + obst->radius - d,2);
				}
			}

			double theta = 2.0*M_PI*(double)i/((double)N_PTS);
			potential += abs(theta - pose.psi) * turnPenalty;
//			ROS_INFO("turn potential %f, added %f",potential [i][j], abs(theta - pos.psi) * turnPenalty);

			if(potential < best_potential) {
				//ROS_INFO("Better Potential = %.3f at %d, %d\n",potential[i][j], i, j);
				best_potential    = potential;
				best_th = theta;
				best_r  = r_ahead*((double)j/((R_PTS-1)*1.0));
			}
		}
	}

	// Find the point with minimum potential
	carrot.x = pose.x + best_r*cos(best_th);
	carrot.y = pose.y + best_r*sin(best_th);
	carrot.psi = goal.psi;
	carrot.vel = goal.vel * (best_r / r_ahead);//saturate((SLOW_SPEED/estSpeed*2.0),0.0,1.0);
	//ROS_INFO("Carrot x = %.3f, Carrot y = %.3f, Carrot psi = %.3f, Carrot vel = %.3f, best_th = %.3f, best_r = %.3f, best_potential = %.3f", carrot.x , carrot.y, carrot.psi, carrot.vel, best_th,best_r, best_potential);
	//ROS_INFO("Goal x = %.3f, Goal y = %.3f, Goal psi = %.3f, Goal vel = %.3f", goal.x , goal.y, goal.psi , goal.vel);
	return carrot;
}*/

