#include "Obstacle.h"

Obstacle::Obstacle(std::string name, ros::NodeHandle nhandle){
	this->radius = 0;
	this->name = name;
	posesub = nhandle.subscribe(name+"/pose", 1, &Obstacle::poseCallback, this);
	velsub = nhandle.subscribe(name+"/vel", 1, &Obstacle::velCallback, this); 
}

void Obstacle::poseCallback(const geometry_msgs::PoseStamped &msg){
	x = msg.pose.position.x;
	y = msg.pose.position.y;
	z = msg.pose.position.z;
}

void Obstacle::velCallback(const geometry_msgs::TwistStamped &msg){
	dx = msg.twist.linear.x;
	dy = msg.twist.linear.y;
	dz = msg.twist.linear.z;
}
