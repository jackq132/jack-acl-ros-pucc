//#include <bluetooth/bluetooth.h>
//#include <bluetooth/hci.h>
//#include <bluetooth/hci_lib.h>
//#include <bluetooth/rfcomm.h>

#include <RAVEN_classes.hpp>

//## Integrator TODO: AW_thresh should depend on the type of integrator
Integrator::Integrator()
{
	value = 0;
	AW_thresh = 0.4;//aw_thresh;
	AW_value = 0;
}

Integrator::Integrator(double aw_thresh)
{
	value = 0;
	AW_thresh = aw_thresh;
	AW_value = 0;
}

void Integrator::increment (double inc, double dt)
{
    value+=inc*dt;
    if(value > AW_thresh)
        value = AW_thresh;
    if(value < -AW_thresh)
        value = -AW_thresh;
}

void Integrator::reset()
{
	value = 0;
	AW_value = 0;
}




// Serial Port
SerialPort::SerialPort()
{
	initialized = false;
}

int SerialPort::spInitialize(std::string hwDevice, int baudRate, bool blocking)
{

	struct termios options;

	// Architecture-dependent code: open and configure the serial port
	// Serial port settings:  38400, No parity, 8 data bits, 1 stop bit, no flow control
	if( blocking == true )
		serial = open(hwDevice.c_str(),O_RDWR);
	else
		serial = open(hwDevice.c_str(),O_RDWR | O_NONBLOCK);

	if(serial == -1 ) {
		printf("\tSerial Error :: Couldn't open serial port at %s\n", hwDevice.c_str());
		return -1;
	}

	tcgetattr(serial, &options);
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

	if( baudRate == 38400 ) { // ghetto hack
		cfsetispeed(&options, B38400);
		cfsetospeed(&options, B38400);
	} else if( baudRate == 115200 ) {
		cfsetispeed(&options, B115200);
		cfsetospeed(&options, B115200);
	} else if( baudRate == 19200 ) {
		cfsetispeed(&options, B19200);
		cfsetospeed(&options, B19200);
	} else if( baudRate == 57600 ) {
		cfsetispeed(&options, B57600);
		cfsetospeed(&options, B57600);
	} else {
		cfsetispeed(&options, B9600);
		cfsetospeed(&options, B9600);
	}

	options.c_cflag &= ~CSIZE; /* Mask the character size bits */
	options.c_cflag |= CS8;    /* Select 8 data bits */
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;  // 1 stop bit
	options.c_cflag &= ~CRTSCTS; // No RTC/CTS (hardware) flow control
	options.c_cflag &= ~PARODD;
	options.c_cflag &= ~CRTSCTS;
	options.c_lflag &= ~ECHOCTL;
	options.c_lflag &= ~ECHOKE;
	options.c_lflag &= ~ECHOK;
	options.c_iflag |= IGNBRK;
	options.c_cc[VTIME] = 5;

	if(tcsetattr(serial, TCSANOW, &options) != 0) {
		printf("\tSerial Error :: Couldn't configure serial port\n");
		return -1;
	}
	// End architecture-dependent code

	printf("\tSerial Port %s Initialized\n",hwDevice.c_str());
	initialized = true;
	return 1;
}


int SerialPort::spSend(char* pkt)
{
	return write(serial,pkt,strlen(pkt));
}

int SerialPort::spSend(char* pkt, int sz)
{
	return write(serial,pkt,sz);
}

int SerialPort::spSend(uint8_t* pkt, uint8_t sz)
{
	return write(serial,pkt,sz);
}

int SerialPort::spSend(unsigned char* pkt, int sz)
{
	return write(serial,pkt,sz);
}


unsigned char* SerialPort::spReceive()
{
	int n = read(serial, &buffer, SER_BUF_SZ);
	return buffer;
}

unsigned char* SerialPort::spReceive(int* nbytes)
{
	int n = read(serial, &buffer, *(nbytes));
	*(nbytes) = n;
	return buffer;
}

int SerialPort::spReceive(unsigned char* buf, int nbytes)
{
	return read(serial, buf, nbytes);
}

char SerialPort::spReceiveSingle()
{
	char ch;
	int n = read(serial, &ch, 1);
	return ch;
}

int SerialPort::spSetBaud(int rate)
{
	struct termios options;
	tcgetattr(serial, &options);

	if( rate == 38400 ) { // ghetto hack
		cfsetispeed(&options, B38400);
		cfsetospeed(&options, B38400);
	} else if( rate == 115200 ) {
		cfsetispeed(&options, B115200);
		cfsetospeed(&options, B115200);
	} else if( rate == 19200 ) {
		cfsetispeed(&options, B19200);
		cfsetospeed(&options, B19200);
	} else if( rate == 57600 ) {
		cfsetispeed(&options, B57600);
		cfsetospeed(&options, B57600);
	} else {
		cfsetispeed(&options, B9600);
		cfsetospeed(&options, B9600);
	}

	if( tcsetattr(serial, TCSANOW, &options) != 0 ) {
		printf("\tSerial Error :: Couldn't reset baud rate\n");
		return(-1);
	}
	return(0);
}

void SerialPort::spFlush()
{
	tcflush(serial, TCIFLUSH);
}

void SerialPort::spClose()
{
	close(serial);
	initialized = false;
}

//// Bluetooth port
//int   BTPort::BTInit(std::string macaddr)
//{
//	int status;
//	struct sockaddr_rc addr = { 0 };
//	mac = macaddr.c_str();
//
//	// set the connection parameters (who to connect to)
//	addr.rc_family = AF_BLUETOOTH;
//	addr.rc_channel = (uint8_t) 1;
//	str2ba( mac, &addr.rc_bdaddr );
//
//	// connect to server
//	printf("Connecting to BT device...\n");
//	for(int i=0; i<5; i++)
//	{
//		// allocate a socket
//		s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
//		status = connect(s, (struct sockaddr *)&addr, sizeof(addr));
//		if(status>=0)
//			break;
//		close(s);
//	}
//	//Couldn't connect
//	if( status < 0 ){
//		perror("Couldn't connect to BT device!");
//		exit(0);
//	}
//	printf("Connected to BT MAC %s \n", mac);
//
//	return 1;
//}
//////BOB Compiler warning here; no returned value
//int   BTPort::BTSend(char* pkt, int len)
//{
//	write(s, pkt, len);
//}
//
//uint8_t  BTPort::BTReceiveByte()
//{
//	uint8_t in;
//	recv(s, &in, 1, 0);
//	return in;
//}
//void  BTPort::BTClose()
//{
//	close(s);
//}

// UDP Port
int UDPPort::udpInit(bool bCast, std::string addr, int lport)
{
	return( UDPPort::udpInit(bCast, addr, lport, lport) );	
}

int UDPPort::udpInit(bool bCast, std::string addr, int lport, int bport)
{
	broadcast = bCast;
	portTx    = bport;
	portRx    = lport;

	// Receive port
	socketRx = socket(PF_INET, SOCK_DGRAM, 0);
	bzero(&addressRx, sizeof(addressRx));

	addressRx.sin_family      = AF_INET;
	addressRx.sin_port        = htons(portRx);
	addressRx.sin_addr.s_addr = INADDR_ANY;

	if( bind(socketRx, (struct sockaddr*)&addressRx, sizeof(addressRx)) != 0 ) {
		std::cout << "Couldn't bind to Rx port " << portRx << std::endl;
	}

	std::cout << "UDP listen on port " << portRx << " complete" << std::endl;

	// Broadcast port
	if(broadcast) {
		socketTx = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
		bzero(&addressTx, sizeof(addressTx));

		if(socketTx < 0)
			std::cout << "Couldn't create socket" << std::endl;

		const int on = 1;
		if(setsockopt(socketTx, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on)) < 0)
			std::cout << "Couldn't set SO_BROADCAST" << std::endl;

		addressTx.sin_family = AF_INET;
		addressTx.sin_port   = htons(portTx);

		inet_aton(addr.c_str(), &addressTx.sin_addr);
		std::cout << "UDP broadcast on port " << portTx << " complete" << std::endl;
	}


	return(1);
}


int UDPPort::udpSend(char* pkt)
{

	sendto(socketTx, pkt, strlen(pkt), 0, (struct sockaddr *)&addressTx, sizeof(addressTx));

	return strlen(pkt);

}
int UDPPort::udpSend(Message* msg)
{
	char pkt[1028];
	bzero(pkt, sizeof(pkt));

	sprintf(pkt,"%d %d %d %f %f %f %f %f %f %f %f;",msg->src,msg->dest,msg->cmdID,
			msg->data[0],msg->data[1],msg->data[2],msg->data[3],msg->data[4],msg->data[5],msg->data[6],msg->data[7]);

	return udpSend(pkt);
}

char* UDPPort::udpReceive()
{

	bzero(buffer, sizeof(buffer));

	int addr_len = sizeof(addressRx);

	recvfrom(socketRx, buffer, sizeof(buffer), 0, (struct sockaddr*)&addressRx, (socklen_t*)&addr_len);

	return buffer;
}


void UDPPort::udpClose()
{

	close(socketRx);

	if(broadcast)
		close(socketTx);

}




//// Obstacle list no longer used by carrotgen - using a map instead.
/*
// Obstacle List
ObstacleList::ObstacleList()
{

}

void ObstacleList::add(Obstacle *obj)
{
	// check if obj is already on list
	// if no  - add     : list.push_back(obj);
	// if yes - replace : list.at(indx) = obj;

	bool found = false;
	int sz = list.size();
	//printf("list size = %d\n",sz);

	for( int ii=0; ii<sz; ii++ ) {
		//printf("names are: %s, %s\n",list.at(ii).name.c_str(),obj.name.c_str());
		if( strstr(obj->name,list.at(ii)->name) ) {
			found = true;
			list.at(ii) = obj;
		}
	}

	if( found == false )
		list.push_back(obj);

}

void ObstacleList::getState(char* name, double &x, double &y, double &dx, double &dy, double &t)
{

	bool found = false;
	int sz = list.size();

	for( int ii=0; ii<sz; ii++ ) {
		if( strstr(name,list.at(ii).name) ) {
			found = true;
			x  = list.at(ii).x;
			y  = list.at(ii).y;
			dx = list.at(ii).dx;
			dy = list.at(ii).dy;
			t  = list.at(ii).t;
			break;
		}
	}

	if( found == false ) {
		x  = 0.0;
		y  = 0.0;
		dx = 0.0;
		dy = 0.0;
		t  = 0.0;
	}

}

void ObstacleList::clear()
{

	list.clear();

}

int ObstacleList::size()
{

	return list.size();

}
*/
