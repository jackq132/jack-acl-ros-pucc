function plotting ()
	clc
	close all
	xmin_outer= -7.84
	xmax_outer= -3.93
	ymin_outer= -7.05
	ymax_outer= -1.98
	xmin_inner= -6.85
	xmax_inner= -5.04
	ymin_inner= -5.896
	ymax_inner= -2.98

	%plot the boundary boxes
	xOuter = [xmin_outer xmax_outer xmax_outer xmin_outer xmin_outer];
	yOuter = [ymax_outer ymax_outer ymin_outer ymin_outer ymax_outer];
	xInner = [xmin_inner xmax_inner xmax_inner xmin_inner xmin_inner];
	yInner = [ymax_inner ymax_inner ymin_inner ymin_inner ymax_inner];
	plot(xOuter,yOuter);
	hold on
	plot(xInner,yInner);

	trajId = 0;
	trajId = plotTraj(trajId,[[-4.5, -4.4], [-7.5, -4.4]]);
	trajId = plotTraj(trajId,[[-6, -2.4],[-6, -3.5],[-6.43, -4.2], [-7.5, -4.5]]);
	trajId = plotTraj(trajId,[[-6, -2.4],[-6, -6.4]]);
	trajId = plotTraj(trajId,[[-6, -2.4],[-6, -3.45], [-5.90, -4.7], [-5.45, -5.4], [-4.5, -5.4]]);
	trajId = plotTraj(trajId,[[-4.5, -4.6], [-5, -4.6], [-6.2, -4.6], [-7.1, -3.5]]);
	trajId = plotTraj(trajId,[[-4.5, -4.6], [-5, -4.6], [-6.2, -4.6], [-7.1, -6]]);
endfunction

function trajId = plotTraj(trajId,traj)
	mColor = rainbow(8)(trajId+1,:);
	plot(traj(1:2:end),traj(2:2:end),'color',mColor)
	text(traj(1)+rand()/5,traj(2)+rand()/5,num2str(trajId),'color', mColor,'FontSize',20)
	trajId = trajId + 1;
endfunction
