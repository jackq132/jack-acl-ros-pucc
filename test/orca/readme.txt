Dependencies: RVO2

http://gamma.cs.unc.edu/RVO2/downloads/

Installation:
Compile RVO2
Copy libRVO.a to /usr/local/lib (or your default libs directory)
Now target_link_libraries(orca RVO) will recognize RVO!
