#include "Orca.h"
#include <iostream>

#ifdef _OPENMP
#include <omp.h>
#endif

#ifndef M_PI
const float M_PI = 3.14159265358979323846f;
#endif

Orca::~Orca() {
	for (int i = 0; i < obstacles.size(); i++){
		if (obstacles[i] != NULL){
			delete obstacles[i];
		}
		obstacles[i] = NULL;
	}
}

void Orca::updateObstacleList(ros::NodeHandle nhandle){
	ros::master::V_TopicInfo allTopics;
	ros::master::getTopics(allTopics);
	//go over allTopics, and add any that we don't have
	vector<string> objNames;
	for (int i = 0; i < allTopics.size(); i++){
		string topicName = allTopics[i].name; 
		//only adding GPUCCs as obstacles for now
		if ((topicName.find("/pose") != string::npos) && (topicName.find("/GP") != string::npos)){

			size_t secondSlashIdx = topicName.find("/", 1);
			objNames.push_back(topicName.substr(1, secondSlashIdx-1));
			//search through the list to see if it's already there
			bool alreadySubbed = false;
			for (int j = 0; j < this->obstacles.size(); j++){
				if (strncmp(objNames.back().c_str(), this->obstacles[j]->name.c_str(), strlen(objNames.back().c_str())) == 0){
					alreadySubbed = true;
					break;
				}
			}
			if (!alreadySubbed){
				//constructor call here subscribes to the object's RAVEN stream
				ROS_INFO("Obstacle added: %s", topicName.c_str());

				this->obstacles.push_back(new Obstacle(objNames.back(), nhandle));
			}
		}
	}

	//go over everything we have, and remove any that are not in allTopics. Also delete any obstacle that has X/Y/Z/etc. set to 0.
	//the removal unsubscribes from the object's stream
	vector<Obstacle*>::iterator it = obstacles.begin();
	while (it != obstacles.end()){
		//assumption: any GPUCC publishing its pose is in FoV of vicon.
		//eventually, can replace this if statement with a smarter version of the below to work around this assumption (e.g. if there are other people in the lab using the GPUCCs at same time).		
		//if ( (find(objNames.begin(), objNames.end(), (*it)->name) == objNames.end()) || ( ((*it)->numCallbacksReceived>1) && isZero((*it)->x) && isZero((*it)->y) && isZero((*it)->z))){
		if ( (find(objNames.begin(), objNames.end(), (*it)->name) == objNames.end()) ){
			ROS_INFO("Obstacle removed: %s", (*it)->name.c_str());
			delete *it;
			it = obstacles.erase(it);
		} else {
			++it;
		}
	}
}

/*
void Orca::updateObstacleGoals(ros::NodeHandle nhandle){
	vector<Obstacle*>::iterator it = obstacles.begin();
	int i=1;
	while (it != obstacles.end()){
		if ((*it)->isGoalSet == false){
			(*it)->goalX = 
		}
		++it;
	}
}*/

bool Orca::isZero(double num){
	return ((-0.000001 < num) && (num < 0.000001));
	//return (num == 0);
}

/* Store the goals of the agents */
void Orca::setupScenario(RVO::RVOSimulator *sim)
{
	/* Specify the global time step of the simulation. */
	sim->setTimeStep(0.5f);

	/* Specify the default parameters for agents that are subsequently added. */
	//maxNeighborDist, maxNeighbors, timeHorizon, timeHorizonObstacles, radius, maxSpeed
	//if params don't work, usually maxNeighborDist being too high is the culprit.
	sim->setAgentDefaults(0.5f, 10, 2.5f, 2.5f, 0.2f, 0.2f);

	/*
	 * Add agents, specifying their start position, and store their goals on the
	 * opposite side of the environment.
	 */
	for (size_t i = 0; i < 4; ++i) {
		sim->addAgent(2.0f *
		              RVO::Vector2(std::cos(i * 2.0f * M_PI / 4.0f),
		                           std::sin(i * 2.0f * M_PI / 4.0f)));
		goals.push_back(-sim->getAgentPosition(i));
	}
}

void Orca::setupScenarioOrca(RVO::RVOSimulator *sim, double timeStep){
	/* Specify the global time step of the simulation. */
	sim->setTimeStep(timeStep);

	/* Specify the default parameters for agents that are subsequently added. */
	//maxNeighborDist, maxNeighbors, timeHorizon, timeHorizonObstacles, radius, maxSpeed
	//if params don't work, usually maxNeighborDist being too high is the culprit.
	//maxSpeed MUST be less than or equal to SPEED_LIMIT in raven_pucc/Gpucc.cpp
	sim->setAgentDefaults(1.0f, 10, 2.5f, 2.5f, 0.25f, 0.1f);

	/*
	 * Add agents, specifying their start position, and store their goals on the
	 * opposite side of the environment.
	 */
	
	int i=0;
	goals.clear(); //goals vector contains latest goals for all agents. Clear it, so it can be re-set below.
	vector<Obstacle*>::iterator it = obstacles.begin();
	bool firstRun = true;
	while (it != obstacles.end()){
		(*it)->agentNumber = sim->addAgent(RVO::Vector2((*it)->x,(*it)->y));
		//TODO: UNDO this locking of other agents (testing purposes only)
		/*if (!firstRun) {
			setAgentMaxSpeed(agentNumber,0.0f);
		}*/

		goals.push_back(RVO::Vector2((*it)->goalX,(*it)->goalY));
		/*(*it)->agentNumber = sim->addAgent(1.4f *
		              		RVO::Vector2(std::cos(i * 2.0f * M_PI / 4.0f),
		                        	     std::sin(i * 2.0f * M_PI / 4.0f)));
		goals.push_back(-sim->getAgentPosition(i));*/
		++it;
		i++;
		firstRun = false;
	}
}

void Orca::updateVisualization(RVO::RVOSimulator *sim)
{
	/* Output the current global time. */
	std::cout << sim->getGlobalTime();

	/* Output the current position of all the agents. */
	for (size_t i = 0; i < sim->getNumAgents(); ++i) {
		std::cout << " " << sim->getAgentPosition(i);
		
	}

	std::cout << std::endl;
}

void Orca::updateVisualizationOrca(RVO::RVOSimulator *sim)
{
	/* Output the current global time. */
	std::cout << sim->getGlobalTime();

	Carrot tempCarrot;

	/* Output the current position of all the agents. */
	for (size_t i = 0; i < sim->getNumAgents(); ++i) {
		std::cout << " " << sim->getAgentPosition(i);
	}

	//save the calculated position as a carrot
	vector<Obstacle*>::iterator it = obstacles.begin();
	while (it != obstacles.end()){
		tempCarrot.x = sim->getAgentPosition((*it)->agentNumber).x();
		tempCarrot.y = sim->getAgentPosition((*it)->agentNumber).y();
		(*it)->carrotList.push_back(tempCarrot);
		++it;
	}

	std::cout << std::endl;
}

void Orca::setPreferredVelocities(RVO::RVOSimulator *sim)
{
	/*
	 * Set the preferred velocity to be a vector of unit magnitude (speed) in the
	 * direction of the goal.
	 */

	for (int i = 0; i < static_cast<int>(sim->getNumAgents()); ++i) {
		RVO::Vector2 goalVector =  goals[i] - sim->getAgentPosition(i);

		if (RVO::absSq(goalVector) > 1.0f) {
			goalVector = RVO::normalize(goalVector);
		}

		sim->setAgentPrefVelocity(i, goalVector);
	}
}

bool Orca::reachedGoal(RVO::RVOSimulator *sim)
{
	/* Check if all agents have reached their goals. */
	for (size_t i = 0; i < sim->getNumAgents(); ++i) {
		//ROS_INFO("abs distance squared: %f", RVO::absSq(sim->getAgentPosition(i) - goals[i]));
		if (RVO::absSq(sim->getAgentPosition(i) - goals[i]) > 0.5f * sim->getAgentRadius(i) * sim->getAgentRadius(i)) {//0.005f
			return false;
		}
	}
	return true;
}

void Orca::subscribeGoal(ros::NodeHandle nhandle){
	goalSub = nhandle.subscribe("/orca/goal", 1, &Orca::goalCallback, this);
}

void Orca::goalCallback(const raven_pucc::goal &msg){
	//ORCA's callback function, waiting for plan() to publish to /ORCA/goal
	//01. for each agent, set its current position to /agent/pose [needed since we're going to regenerate carrots for ALL agents, so we don't want the ones in the middle of a process to go backwards]. This is done automatically in Obstacles poseCallback!
	//02. set agents[/ORCA/goal/agentName].goal = /ORCA/goal/goal [set the goal state for the agent that called this callback. all the agents have their past goals stored, or their current location if they don't have a goal]. This is done in goalCallback.
	//set isGoalSet = true; Done in goalCallback.
	//03. run ORCA algorithm, get a ton of carrots guiding each agent to its goal 
	//04. for each agent, publish all its carrots to /agentName/carrotList [why not just for the agent that published to /ORCA/goal? because their new carrotList might be completely different from their old carrot lists, and collisions might happen. This way, we absolutely avoid collisions at the expense of doing some extra publishing]. 

	//find the agent that just received a goal
	vector<Obstacle*>::iterator it = obstacles.begin();
	while (it != obstacles.end()){
		//clear all obstacles' carrotLists, since new goal = new ORCA run
		(*it)->carrotList.clear();
		if (strncmp((*it)->name.c_str(), msg.name.c_str(), strlen((*it)->name.c_str())) == 0){
			ROS_INFO("Received goal (x,y) for obstacle: (%f, %f) %s", msg.goal.x, msg.goal.y, msg.name.c_str());
			(*it)->goalX = msg.goal.x;
			(*it)->goalY = msg.goal.y;
			(*it)->isGoalSet = true; //make sure poseCallback doesn't erase the actual goal
			break;
		}
		++it;
	}

	RVO::RVOSimulator *sim = new RVO::RVOSimulator();
	double timeStep = 0.25f; //delta-t for simulation calculations (very fine)
	//delta-t for the carrotList passed back to the puccs (should be ~1-2 seconds)
	double carrotTimeStep = 2.5f;
	int carrotTimeStepMult = (int) carrotTimeStep/timeStep;

	setupScenarioOrca(sim, timeStep);

	int idxSim = 0;
	//perform the simulation
	do {
		//TODO: undo this for sending back fewer carrots. Tricky though, have to watch out for the final carrot.
		//if (idxSim % carrotTimeStepMult == 0){
			updateVisualizationOrca(sim);
		//}
		setPreferredVelocities(sim);
		sim->doStep();
		idxSim++;
	} while (!reachedGoal(sim) && idxSim<200);
		
	/*TODO: undo this if you undid the if statement above
	//explicitly save the final carrot if it wasn't already
	if ((idxSim-1)%carrotTimeStepMult !=0) {
		updateVisualizationOrca(sim);	
	}
	if (idxSim == 200) {
		ROS_INFO("Could not reach goals!!!!!!!!!!!!!!!");
	}*/	


	//publish all obstacles' carrotLists
	it = obstacles.begin();
	while (it != obstacles.end()){
		raven_pucc::carrots msgCarrotList;

		//fill the msg with carrotList
		for(std::vector<Carrot>::iterator carrotIt = (*it)->carrotList.begin(); carrotIt != (*it)->carrotList.end(); ++carrotIt) {
			geometry_msgs::Pose2D singlePos;
			singlePos.x = (carrotIt)->x;
			singlePos.y = (carrotIt)->y;
			ROS_INFO("Pushed back carrot (x,y): (%f, %f)", singlePos.x, singlePos.y);
			msgCarrotList.carrots.push_back(singlePos);
		}
		(*it)->carrotListPub.publish(msgCarrotList);
		ROS_INFO("Published carrotList for obstacles: %s", (*it)->name.c_str());	
		++it;
	}

	delete sim;
}

void Orca::sampleOrca(){
	/* Create a new simulator instance. */
	RVO::RVOSimulator *sim = new RVO::RVOSimulator();

	/* Set up the scenario. */
	setupScenario(sim);

	/* Perform (and manipulate) the simulation. */
	do {
		updateVisualization(sim);
		setPreferredVelocities(sim);
		sim->doStep();
	}
	while (!reachedGoal(sim));

	delete sim;
}
