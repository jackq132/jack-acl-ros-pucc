#include "Obstacle.h"
#include <iostream>

Obstacle::Obstacle(std::string name, ros::NodeHandle nhandle){
	agentNumber = -1;
	isGoalSet = false;
	numCallbacksReceived = 0;
	this->radius = 0;
	this->name = name;
	posesub = nhandle.subscribe(name+"/pose", 1, &Obstacle::poseCallback, this);
	velsub = nhandle.subscribe(name+"/vel", 1, &Obstacle::velCallback, this); 
	carrotListPub = nhandle.advertise<raven_pucc::carrots>(name+"/carrotList", 1);
}

void Obstacle::poseCallback(const geometry_msgs::PoseStamped &msg){
	x = msg.pose.position.x;
	y = msg.pose.position.y;
	z = msg.pose.position.z;

	if (!isGoalSet){
		ROS_INFO("Goal for %s is set to its current position", name.c_str());
		goalX = x;
		goalY = y;
		//do NOT do isGoalSet = true, we're setting a fake goal here (obstacle.location). 
		//Eventually, we'll receive the actual goal in the ORCA callback, and will THEN do isGoalSet = true
	}

	numCallbacksReceived++;
	//std::cout<<"Obstacle: " << name << " Updated (x,y,z): " << x <<" "<< y << " "<< z <<std::endl;
}

void Obstacle::velCallback(const geometry_msgs::TwistStamped &msg){
	dx = msg.twist.linear.x;
	dy = msg.twist.linear.y;
	dz = msg.twist.linear.z;
}
