#ifndef __OBSTACLE_H
/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 * 
 *
 */

#include "ros/ros.h" // for callbacks, etc
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PoseArray.h"
#include "Pose2D.h"
#include "raven_pucc/carrots.h"
#include <string>
#include <vector>

typedef Pose2D Carrot;
using namespace std;

class Obstacle{
	public:
		Obstacle(std::string name, ros::NodeHandle nhandle);
		std::string name;
		double dx, dy, dz;
		double x, y, z;
		double radius;
		double goalX, goalY;
		bool isGoalSet;
		int numCallbacksReceived;
		vector<Carrot> carrotList;
		int agentNumber;
	    	ros::Publisher carrotListPub;

	protected:
		ros::Subscriber posesub, velsub; 

		void poseCallback(const geometry_msgs::PoseStamped &msg);
		void velCallback(const geometry_msgs::TwistStamped &msg);
};


#define __OBSTACLE_H
#endif /* __OBSTACLE_H */
