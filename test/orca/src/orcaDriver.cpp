
#include<ros/ros.h>
#include "std_msgs/String.h"
#include"Orca.h"


int main(int argc, char *argv[])
{
	Orca mOrca;
	//mOrca.sampleOrca();

	ros::init(argc, argv, "orcaDriver");
	ros::NodeHandle n;

	ros::Rate loop_rate(10);
	mOrca.subscribeGoal(n);

	int count = 0;
	while (ros::ok())
  	{
		//make a list of all other agents (assumes any GP## broadcasted by vicon is an active agent)
		mOrca.updateObstacleList(n);
		//mOrca.updateObstacleGoals(n);
		ros::spinOnce();
		loop_rate.sleep();
		++count;
  	}

	return 0;
}
