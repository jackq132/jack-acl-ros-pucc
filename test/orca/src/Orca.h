#ifndef __ORCA_H
/*
 * Author: Trevor Campbell, Bobby Klein
 * Date: August 2013
 * 
 *
 */

#include "ros/ros.h" // for callbacks, etc
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PoseArray.h"
#include "raven_pucc/goal.h"
#include "orca/carrots.h"
#include "orca/newMsg.h"
#include <string>
#include <vector>
#include <cmath>
#include <cstddef>
#include <iostream>
#include "../../RVO2/src/RVO.h"
#include "Obstacle.h"



using namespace std;

class Orca{
	public:
		~Orca();
		void updateObstacleList(ros::NodeHandle nhandle);
		void setupScenario(RVO::RVOSimulator *sim);
		void setupScenarioOrca(RVO::RVOSimulator *sim, double timeStep);
		void updateVisualization(RVO::RVOSimulator *sim);
		void updateVisualizationOrca(RVO::RVOSimulator *sim);
		void setPreferredVelocities(RVO::RVOSimulator *sim);
		bool reachedGoal(RVO::RVOSimulator *sim);
		void sampleOrca();
		bool isZero(double num);
		//void updateObstacleGoals(ros::NodeHandle nhandle);
		void subscribeGoal(ros::NodeHandle nhandle);


	protected:
		std::vector<RVO::Vector2> goals;
		ros::Subscriber goalSub; 
		void goalCallback(const raven_pucc::goal &msg);

		vector<Obstacle*> obstacles;
};


#define __ORCA_H
#endif /* __ORCA_H */
