#ifndef __POSE2D_H
class Pose2D{
	public:
		double x, y, psi;
		double dx, dy, dpsi;
		double vel;
};
#define __POSE2D_H
#endif /* __POSE2D_H */
