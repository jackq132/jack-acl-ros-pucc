#include <cmath>
#include <cstddef>
#include <iostream>
#include <vector>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "../../RVO2/src/RVO.h"
#include<ros/ros.h>
#include "std_msgs/String.h"

#ifndef M_PI
const float M_PI = 3.14159265358979323846f;
#endif

/* Store the goals of the agents. */
std::vector<RVO::Vector2> goals;

void setupScenario(RVO::RVOSimulator *sim)
{
	/* Specify the global time step of the simulation. */
	sim->setTimeStep(2.0f);

	/* Specify the default parameters for agents that are subsequently added. */
	sim->setAgentDefaults(15.0f, 10, 10.0f, 10.0f, 1.5f, 2.0f);

	/*
	 * Add agents, specifying their start position, and store their goals on the
	 * opposite side of the environment.
	 */
	for (size_t i = 0; i < 4; ++i) {
		sim->addAgent(200.0f *
		              RVO::Vector2(std::cos(i * 2.0f * M_PI / 4.0f),
		                           std::sin(i * 2.0f * M_PI / 4.0f)));
		goals.push_back(-sim->getAgentPosition(i));
	}
}

void updateVisualization(RVO::RVOSimulator *sim)
{
	/* Output the current global time. */
	std::cout << sim->getGlobalTime();

	/* Output the current position of all the agents. */
	for (size_t i = 0; i < sim->getNumAgents(); ++i) {
		std::cout << " " << sim->getAgentPosition(i);
	}

	std::cout << std::endl;
}

void setPreferredVelocities(RVO::RVOSimulator *sim)
{
	/*
	 * Set the preferred velocity to be a vector of unit magnitude (speed) in the
	 * direction of the goal.
	 */

	for (int i = 0; i < static_cast<int>(sim->getNumAgents()); ++i) {
		RVO::Vector2 goalVector =  goals[i] - sim->getAgentPosition(i);

		if (RVO::absSq(goalVector) > 1.0f) {
			goalVector = RVO::normalize(goalVector);
		}

		sim->setAgentPrefVelocity(i, goalVector);
	}
}

bool reachedGoal(RVO::RVOSimulator *sim)
{
	/* Check if all agents have reached their goals. */
	for (size_t i = 0; i < sim->getNumAgents(); ++i) {
		if (RVO::absSq(sim->getAgentPosition(i) - goals[i]) > 0.5f * sim->getAgentRadius(i) * sim->getAgentRadius(i)) {
			return false;
		}
	}
	return true;
}

//ORCA's main(). 
			//might have to keep monitoring in a while loop? or maybe we can just assume ORCA callback will be called for any agents that are moving? no...static obstacles
			//01. parse topics and make a list of all agents (AKA obstacles) and their current location
			//02. if (exists(/agentName/goal))
			//03. 	set agents[agent].goal = /agentName/goal
			//04. else 
			//05. 	set agents[agent].goal = /agent/pose [set its goal to its current position]
			//06. endif

			//ORCA's callback function, waiting for plan() to publish to /ORCA/goal
			//01. for each agent, set its current position to /agent/pose [needed since we're going to regenerate carrots for ALL agents, so we don't want the ones in the middle of a process to go backwards]
			//02. set agents[/ORCA/goal/agentName].goal = /ORCA/goal/goal [set the goal state for the agent that called this callback. all the agents have their past goals stored]
			//03. run ORCA algorithm, you have all the preferred velocities for all agents, get a ton of carrots guiding each agent to its goal 
			//04. for each agent, publish all its carrots to /agentName/carrotList [why not just for the agent that published to /ORCA/goal? because their new carrotList might be completely different from their old carrot lists, and collisions might happen. This way, we absolutely avoid collisions at the expense of doing some extra publishing]. 

void goalCallback(){
	/* Create a new simulator instance. */
	RVO::RVOSimulator *sim = new RVO::RVOSimulator();

	/* Set up the scenario. */
	setupScenario(sim);

	/* Perform (and manipulate) the simulation. */
	do {
		updateVisualization(sim);
		setPreferredVelocities(sim);
		sim->doStep();
	}
	while (!reachedGoal(sim));

	delete sim;
}

int main(int argc, char *argv[])
{
	ros::init(argc, argv, "talker");
	ros::NodeHandle n;
	ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
	ros::Rate loop_rate(10);

	int count = 0;
	while (ros::ok())
  	{
		std_msgs::String msg;
		std::stringstream ss;
		ss << "hello world " << count;
		msg.data = ss.str();

		ROS_INFO("%s", msg.data.c_str());

		chatter_pub.publish(msg);

		ros::spinOnce();

		loop_rate.sleep();
		++count;
  	}

	return 0;
}
