/*
 * PoseListener.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: bob
 *
 *  Note below that we use the term 'obstacle' to refer to any node which
 *  publishes on a 'pose' (or 'vel') topic [eg GP09/pose]
 */

#include "PoseListener.h"
#include "TopicChecker.h"
#include "std_msgs/String.h"
#include <sstream>
#include <ros/callback_queue.h>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include<fstream>

#include <string>

PoseListener::PoseListener() {
}

PoseListener::~PoseListener() {
	// TODO Auto-generated destructor stub
}

double PoseListener::dist(boost::array<double, 2> pt1, boost::array<double,2> pt2){
	return sqrt( (pt1[0]-pt2[0])*(pt1[0]-pt2[0]) + (pt1[1]-pt2[1])*(pt1[1]-pt2[1]) );
}


void PoseListener::poseCallback(const geometry_msgs::PoseStamped &msg){
/*** Check to ensure the subscribed topic is for a valid vehicle ***/
	

	std::string vehName = msg.header.frame_id;

	double psi = 2.0*acos(msg.pose.orientation.w);
	double ssgn = msg.pose.orientation.z;
	if (ssgn < 0){
		psi *= -1.0;
	}
	//put psi in [0, 2pi]
	while (psi < 0)
		psi += 2.0*PI;
	while (psi > 2.0*PI)
		psi -= 2.0*PI;

	//update obstacle corresponding to this vehicle
	// If this obstacle is being added for first time, give it unique id
	// Note that unique id isn't necessary, it only need be > 0 so it is
	// recognized as a non-static obstacle

	knownVehicleMap[vehName]->name = vehName;
	knownVehicleMap[vehName]->id  = atoi(vehName.substr(2,2).c_str()); // unique number for this vehicle	
	knownVehicleMap[vehName]->x   = msg.pose.position.x;
	knownVehicleMap[vehName]->y   = msg.pose.position.y;
	knownVehicleMap[vehName]->z   = msg.pose.position.z;
	knownVehicleMap[vehName]->psi = psi;

	// NOTE here that velocity has not been updated - that is done
	// in a separate callback.

	knownVehicleMap[vehName]->t   = (double)(msg.header.stamp.sec + msg.header.stamp.nsec/1.0e9);
	// TODO that this assumes that all other obstacles are GPUCCs;
	// Would be better to have a generic here.
	knownVehicleMap[vehName]->radius = GPUCC_RADIUS;

//		ROS_DEBUG("Vehicle %s with numObstacles %d updating pose for obstacle %s", robot.name.c_str(), robot.numObstacles, vehName.c_str());
}

void PoseListener::velCallback(const geometry_msgs::TwistStamped &msg){
/*** Check to ensure the subscribed topic is for a valid vehicle ***/
	std::string vehName = msg.header.frame_id;


	double speed = distance2(0,0,msg.twist.linear.x,msg.twist.linear.y);
	knownVehicleMap[vehName]->vel = speed;
	knownVehicleMap[vehName]->dx = msg.twist.linear.x;
	knownVehicleMap[vehName]->dy = msg.twist.linear.y;
}

/* Iterates through allTopics (passed from a prior query to ROS) and subscribes
 * to those obstacles of which it is not yet aware.
 */
void PoseListener::updateObstTopics(ros::NodeHandle nhandle, ros::master::V_TopicInfo &allTopics)
{
	ROS_DEBUG("Updating obstacle topics");
	std::set<std::string> vehNamesFromTopics;
	for(int i=0; i < allTopics.size(); i++){
		std::string topicName = allTopics[i].name; // /uP04/pose
	        std::string namePrefix = topicName.substr(1,2); // takes form /uP etc
		std::string newObstName;
	        Topic_T topic_t = checkVehTopic(namePrefix, newObstName, topicName);
		if(topic_t == POSE || topic_t == VEL) { // This topic is a vehicle topic
			ROS_DEBUG("found vehicle obstacle %s", newObstName.c_str());
			vehNamesFromTopics.insert(newObstName);
			if(knownVehicleMap.count(newObstName) == 0) { // The topic-vehicle is not yet subscribed
				ROS_DEBUG("Subscribing vehicle %s ",newObstName.c_str());
				subscribeObstacle(nhandle,newObstName,checkNameSimulated(newObstName));
			}
			else {
				ROS_DEBUG("Vehicle %s was already subscribed", newObstName.c_str());
			}
	        }
		else {
			ROS_DEBUG("Topic %s was not a vehicle topic.", topicName.c_str());
		}
	}
	removeOldObstacles(vehNamesFromTopics);
}

// subscribe a vehicle to this particular obst (essentially perfect state info of other agents)
// and initialize waypoint publisher
void PoseListener::subscribeObstacle(ros::NodeHandle nhandle, std::string newObstName, bool isSimulated)
{
	if(knownVehicleMap.count(newObstName) > 0) {
		ROS_ERROR("Tried to resubscribe to an obstacle already subscribed!");
		return;
	}
	ROS_INFO("New obst detected and added: %s", newObstName.c_str());
	Obstacle *newObst = new Obstacle();
	newObst->name = newObstName;

	//subscribe to its pose topic
	newObst->posesub = new ros::Subscriber(nhandle.subscribe(newObstName+"/pose", 1, &PoseListener::poseCallback, this));
	newObst->velsub = new ros::Subscriber(nhandle.subscribe(newObstName+"/vel", 1, &PoseListener::velCallback, this));

	knownVehicleMap[newObstName] = newObst;
}

Topic_T PoseListener::checkVehTopic(std::string namePrefix, std::string &obstName, std::string topicName)//inline
{
	std::vector<std::string> tmp;
	boost::split(tmp, topicName, boost::is_any_of("/"), boost::token_compress_on); // of the form: /uP01/pose
    // tmp[0-2] contains /""/<name>/"pose"

	if(tmp.size() < 3) // cannot be a vehicle topic
		return NONE;

    obstName = tmp.at(1);
	if(!tmp.at(2).compare("pose")){ // This is a pose topic
//        	ROS_DEBUG("Topic %s IS a vehicle pose topic.",cstrTopicName);
		return POSE;
	} else if(!tmp.at(2).compare("vel")) {
//        	ROS_DEBUG("Topic %s IS a vehicle vel topic.",cstrTopicName);
		return VEL;
	} else {
//        	ROS_DEBUG("Topic %s is a vehicle topic, non-pose.",cstrTopicName);
	}
    return NONE;
}

void PoseListener::removeOldObstacles(std::set<std::string> vehNamesFromTopics)
{
	ROS_DEBUG("Removing old Obstacles");
// iterate through the subscribers and get the name of each of the vehicles to which we are already subscribing
	std::map <std::string, Obstacle *>::iterator it = knownVehicleMap.begin();
	while(it != knownVehicleMap.end()) {
		std::string subscribedObstName = it->first; // vehicle name
        bool wasRemoved = (std::find(vehNamesFromTopics.begin(),vehNamesFromTopics.end(), subscribedObstName) == vehNamesFromTopics.end()); // do we hit the end of the vehNamesFromTopics vector without finding our obstacle
        // if that vehicle is no longer present in the topics being broadcast, we should delete its associated marker
        if(wasRemoved)
        {
        	ROS_DEBUG("Vehicle %s no longer exists in domain.", subscribedObstName.c_str());
        	Obstacle *obstToDelete = (it->second);
            knownVehicleMap.erase(it++); // erase the obstacle, which calls its destructor automatically.
            if(obstToDelete != NULL) delete(obstToDelete);
        }
        else ++it;
    }
}

// @param  objectName
// @return true if the object exists [and populates &pose parameter], false o/w.
bool PoseListener::getPoseXYZPsi(std::string objectName, boost::array<double, 4> &pose)
{
	if(knownVehicleMap.count(objectName) <= 0) { // vehicle doesn't exist
		ROS_INFO("Attempted to get pose of non-existent vehicle %s", objectName.c_str());
		return false;
	}
	pose[0] = knownVehicleMap[objectName]->x;
	pose[1] = knownVehicleMap[objectName]->y;
	pose[2] = knownVehicleMap[objectName]->z;
	pose[3] = knownVehicleMap[objectName]->psi;
	return true;
}

// @param  objectName
// @return true if the object exists [and populates &vel parameter], false o/w.
bool PoseListener::getVelXYSpeed(std::string objectName, boost::array<double, 3> &vel)
{
	if(knownVehicleMap.count(objectName) <= 0) { // vehicle doesn't exist
		ROS_INFO("Attempted to get pose of non-existent vehicle %s", objectName.c_str());
		return false;
	}
	vel[0] = knownVehicleMap[objectName]->dx;
	vel[1] = knownVehicleMap[objectName]->dy;
	vel[2] = knownVehicleMap[objectName]->vel;
	return true;
}

std::map <std::string, Obstacle *> PoseListener::getAllVehicles()
{
	return knownVehicleMap;
}

void PoseListener::setFreqSample(int freq){
	this->freqSample = freq;	
}

void PoseListener::setMaxNumObs(int mMaxNumObs){
	mObsWrapper.setMaxNumObs(mMaxNumObs);
}

//TEMPORARY
//void PoseListener::sampleCallback(const turtlesim::Pose &msg){
void PoseListener::sampleCallback(const geometry_msgs::PoseStamped &msg){
	double timePassed = ros::Time::now().toSec() - timePrevSample;
	if (trajId==-1){
		mObsWrapper.clearAllObs();
	}
	else if (timePassed >= 1.0f/freqSample){
		if ((msg.pose.position.x < xmin) || (msg.pose.position.x > xmax) || (msg.pose.position.y < ymin) || (msg.pose.position.y > ymax)) {
			//ROS_INFO("Out of bounds!");			
			return;
		}
		//ROS_INFO("Desired Sampling Freq: [%f]", (double)freqSample);
		ROS_INFO("Actual Sampling Freq: [%f]", 1.0f/timePassed);
		timePrevSample = ros::Time::now().toSec();
		mObsWrapper.pushObs(msg.pose);
		mObsWrapper.printAllObs();
		ROS_INFO("TrajId: %d", trajId);

		if (shouldSaveData){
						//timestamp /t trajId /t x /t y /t psi /t ledColor /n
		ROS_INFO("Printing to file (%s): %f\t%d\t%f\t%f\t%f\t%d\n", filename.c_str(), timePrevSample, trajId, msg.pose.position.x, msg.pose.position.y, mObsWrapper.getAngleFromQuat(msg.pose.orientation), ledColor);
		appendToFile(timePrevSample, trajId, msg.pose.position.x, msg.pose.position.y, mObsWrapper.getAngleFromQuat(msg.pose.orientation), ledColor);
		}
	}
}

bool PoseListener::appendToFile(double timePrevSample, int trajId, float x, float y, float orientation, int ledColor){

	std::ofstream fout;
	fout.open(filename.c_str(),std::ios::app | std::ios::out);
	if (fout.is_open()){
		fout<< std::setprecision(14)<< timePrevSample<<"\t"<<trajId<<"\t"<<x<<"\t"<<y<<"\t"<<orientation<<"\t"<<ledColor<<std::endl;
		fout.close();
	} else {
		ROS_INFO("Could not save data to file!");
	}
	return 0;
}

void PoseListener::trajIdCallback(const pose_listener::trajId &msg){
	trajId = msg.id;
	ledColor = msg.ledColor;
}

bool PoseListener::respondToReq(pose_listener::ReturnObsArrays::Request  &req, pose_listener::ReturnObsArrays::Response &res)
{
	ROS_INFO("Sample requested.");

	for(std::deque<ObsWrapper::obs>::const_iterator iterObsDeque = mObsWrapper.obsDeque.begin(); iterObsDeque != mObsWrapper.obsDeque.end(); ++iterObsDeque){
		ROS_INFO("Obs[x]: %f, Obs[y]: %f, Obs[psi]: %f", iterObsDeque->x, iterObsDeque->y, iterObsDeque->psi);
		res.trajId.push_back(trajId);
		res.ledColor.push_back(ledColor);
		res.x.push_back(iterObsDeque->x);
		res.y.push_back(iterObsDeque->y);
		res.psi.push_back(iterObsDeque->psi);
	}
	if (req.clearFlag == true){
		ROS_INFO("Sending back following response and clearing observation:");
		mObsWrapper.clearAllObs();
	} else {
		ROS_INFO("Sending back following response and NOT clearing observation:");
	}
	return true;
}

void processArgv(int argc, char** argv, std::string& puccname, double& xmin, double& xmax, double& ymin, double& ymax, int& freqSample, int& maxNumObs){
	int argvit = 0;
	while (argvit < argc-1){ //-1 since each command line option is of the format --xxx yyy
		if (strncmp(argv[argvit], "--name", 5) == 0){
			puccname = "/";
			puccname.append(argv[argvit+1]);
			argvit += 2;
		} else if (strncmp(argv[argvit], "--xmin", 5) == 0){
			xmin = atof(argv[argvit+1]);
			argvit += 2;
		} else if (strncmp(argv[argvit], "--xmax", 5) == 0){
			xmax = atof(argv[argvit+1]);
			argvit += 2;
		} else if (strncmp(argv[argvit], "--ymin", 5) == 0){
			ymin = atof(argv[argvit+1]);
			argvit += 2;
		} else if (strncmp(argv[argvit], "--ymax", 5) == 0){
			ymax = atof(argv[argvit+1]);
			argvit += 2;
		} else if (strncmp(argv[argvit], "--freqSample", 11) == 0){
			freqSample = atoi(argv[argvit+1]);
			argvit += 2;
		} else if (strncmp(argv[argvit], "--maxNumObs", 10) == 0){
			maxNumObs = atoi(argv[argvit+1]);
			argvit += 2;
		} else {
			//if it isn't any of the normal switches, just increment
			argvit++;
		}
	}
	return;
}

int main(int argc, char **argv){
	ROS_INFO("Started");
	PoseListener mPoseListener;
	std::string puccname;
	std::string trajIdTopicName;

	int freqSample;
	int maxNumObs;
	processArgv(argc, argv, puccname, mPoseListener.xmin, mPoseListener.xmax, mPoseListener.ymin, mPoseListener.ymax, freqSample, maxNumObs);
	std::string serviceName = "return_obs_arrays" + puccname;	
	trajIdTopicName = puccname + "/curTrajId";
	mPoseListener.filename = puccname;
	mPoseListener.filename.erase(0,1).append(".txt");

	puccname.append("/pose");
	ROS_INFO("xmin: %f", mPoseListener.xmin);
	ROS_INFO("xmax: %f", mPoseListener.xmax);
	ROS_INFO("ymin: %f", mPoseListener.ymin);
	ROS_INFO("ymax: %f", mPoseListener.ymax);
	ROS_INFO("freqSample: %d", freqSample);
	ROS_INFO("maxNumObs: %d", maxNumObs);
	
	mPoseListener.setFreqSample(freqSample);
	mPoseListener.setMaxNumObs(maxNumObs);
	mPoseListener.shouldSaveData =true;

	//clear prev data
	/*if (mPoseListener.shouldSaveData){
		std::ofstream fout;
		fout.open(mPoseListener.filename.c_str());
		fout.close();
	}*/

	ROS_INFO("Pucc name: %s", puccname.c_str());

	ros::init(argc, argv, "listener");
	ros::NodeHandle n;
		
	ros::Subscriber sub = n.subscribe(puccname, 1, &PoseListener::sampleCallback, &mPoseListener);
	ros::Subscriber subTrajId = n.subscribe(trajIdTopicName, 1, &PoseListener::trajIdCallback, &mPoseListener);
	
	ROS_INFO("Service name: %s", serviceName.c_str());

	ros::ServiceServer service = n.advertiseService(serviceName, &PoseListener::respondToReq, &mPoseListener);
	ROS_INFO("Ready to publish observations.");

	while (ros::ok()){
		ros::getGlobalCallbackQueue()->callAvailable(ros::WallDuration(0.1));
	}

	return 0;
}

