/*
 * ObsWrapper.cpp
 *
 *  Created on: Sep 11, 2013
 *      Author: shayo
 *
 */

#define _USE_MATH_DEFINES 
#include <cmath>
#include "ObsWrapper.h"
#include "ros/ros.h" 

ObsWrapper::ObsWrapper() {
}

ObsWrapper::~ObsWrapper() {
}

float ObsWrapper::getAngleFromQuat(geometry_msgs::Quaternion mQuaternion){
	float psi;
	float ssgn;

	psi = 2.0f * acos(mQuaternion.w);
	ssgn = mQuaternion.z;

	if (ssgn < 0){
		psi *= -1.0;
	}
	//put psi in [0, 2pi]
	while (psi < -1.0*M_PI)
		psi += 2.0*M_PI;
	while (psi > 1.0*M_PI)
		psi -= 2.0*M_PI;

	return psi;
	
	//float x = mQuaternion.x / sqrt(1-mQuaternion.w*mQuaternion.w);
	//float y = mQuaternion.y / sqrt(1-mQuaternion.w*mQuaternion.w);
	//float z = mQuaternion.z / sqrt(1-mQuaternion.w*mQuaternion.w);
}

void ObsWrapper::pushObs(geometry_msgs::Pose mPose){
	ROS_INFO("Max number of obs: %d", maxNumObs);
	if (obsDeque.size()>=maxNumObs){
		obsDeque.pop_front();
	}
	
	obsDeque.push_back(obs(mPose.position.x, mPose.position.y, getAngleFromQuat(mPose.orientation)));

}

void ObsWrapper::printAllObs(){
	for(std::deque<obs>::const_iterator iterObsDeque = obsDeque.begin(); iterObsDeque != obsDeque.end(); ++iterObsDeque){
		ROS_INFO("Obs[x]: %f, Obs[y]: %f, Obs[psi]: %f", iterObsDeque->x, iterObsDeque->y, iterObsDeque->psi);
	}
}

void ObsWrapper::setMaxNumObs(int mMaxNumObs){
	maxNumObs = mMaxNumObs;
}

void ObsWrapper::clearAllObs(){
	obsDeque.clear();
}

std::deque<ObsWrapper::obs> ObsWrapper::getAllObs(){
	std::deque<obs> obsDequeCopy;
	obsDeque.clear();
	return obsDequeCopy;
}
