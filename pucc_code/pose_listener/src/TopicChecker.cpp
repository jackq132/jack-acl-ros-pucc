/*
 * TopicChecker.cpp
 *
 *  Created on: Sep 12, 2013
 *      Author: shayo
 *
 */

#include "TopicChecker.h"

TopicChecker::TopicChecker() {
	deadlineTimer(io, boost::posix_time::seconds(2));
	deadlineTimer.async_wait(handle_timeout);
	//boost::thread t(boost::bind(&boost::asio::io_service::run, &io));
}

TopicChecker::~TopicChecker() {
}

void TopicChecker::handle_timeout(boost::system::error_code const& cError){
    deadlineTimer.async_wait(handle_timeout);
}
