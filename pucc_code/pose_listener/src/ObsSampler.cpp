#include "ros/ros.h"
#include "pose_listener/ReturnObsArrays.h"
#include <cstdlib>
   
void processArgv(int argc, char** argv, std::string& puccname, bool &clearFlag){
	int argvit = 0;
	while (argvit < argc-1){ //-1 since each command line option is of the format --xxx yyy
		if (strncmp(argv[argvit], "--name", 5) == 0){
ROS_INFO(argv[argvit+1]);
			puccname = "/";
			puccname.append(argv[argvit+1]);
			argvit += 2;
		} else if (strncmp(argv[argvit], "--clearFlag", 5) == 0){
			ROS_INFO(argv[argvit+1]);
			clearFlag = strcmp(argv[argvit+1],"true")==0 ? 1: 0;
			argvit += 2;
		}  else {
			//if it isn't any of the normal switches, just increment
			argvit++;
		}
	}
	return;
}

int main(int argc, char **argv) { 	
	ros::init(argc, argv, "return_obs_arrays_client");
	
	std::string puccname;
	//whether or not to clear the transition list after response is pushed out
	bool clearFlag;
	processArgv(argc, argv, puccname, clearFlag);
	std::string serviceName = "return_obs_arrays/" + puccname;

	ros::NodeHandle n;
	ros::ServiceClient client = n.serviceClient<pose_listener::ReturnObsArrays>(serviceName.c_str());
	pose_listener::ReturnObsArrays srv;
	srv.request.clearFlag = clearFlag;
	
	if (client.call(srv)) {
		ROS_INFO("Trajectory ID: ");
		for( std::vector<int>::const_iterator i = srv.response.trajId.begin(); i != srv.response.trajId.end(); ++i){
			ROS_INFO("%d ", *i);	
		}

		ROS_INFO("Led Color: ");
		for( std::vector<int>::const_iterator i = srv.response.ledColor.begin(); i != srv.response.ledColor.end(); ++i){
			ROS_INFO("%d ", *i);	
		}

		ROS_INFO("Response x: ");
		for( std::vector<float>::const_iterator i = srv.response.x.begin(); i != srv.response.x.end(); ++i){
			ROS_INFO("%f ", *i);	
		}

		ROS_INFO("Response y: ");
		for( std::vector<float>::const_iterator i = srv.response.y.begin(); i != srv.response.y.end(); ++i){
			ROS_INFO("%f ", *i);	
		}

		ROS_INFO("Response psi: ");
		for( std::vector<float>::const_iterator i = srv.response.psi.begin(); i != srv.response.psi.end(); ++i){
			ROS_INFO("%f ", *i);	
		}	
	} else {
		ROS_ERROR("Failed to call service");
		return 1;
	}
   
	return 0;
}
