/*
 * ObsWrapper.h
 *
 *  Created on: Sep 11, 2013
 *      Author: shayo
 */

#ifndef ObsWrapper_H_
#define ObsWrapper_H_

#include <deque>
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Quaternion.h"

class ObsWrapper {
	public:
		ObsWrapper();
		virtual ~ObsWrapper();

		struct obs {
		    	float x;
		    	float y;
	    		float psi;
	
			obs(){}

			obs(float mX, float mY, float mPsi){
				x = mX;
				y = mY;
				psi = mPsi;
		        }
		};

		void setMaxNumObs(int mMaxNumObs);
		void pushObs(geometry_msgs::Pose mPose);
		void clearAllObs();
		void printAllObs();
		float getAngleFromQuat(geometry_msgs::Quaternion mQuaternion);
		std::deque<obs> getAllObs();
		std::deque<obs> obsDeque;

	private:	
		int maxNumObs;
		
};

#endif /* ObsWrapper_H_ */
