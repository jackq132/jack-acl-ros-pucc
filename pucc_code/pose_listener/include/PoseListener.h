/*
 * PoseListener.h
 *
 *  Created on: Apr 10, 2013
 *      Author: bob
 */

#ifndef PoseListener_H_
#define PoseListener_H_

#include "ros/ros.h" // for callbacks, etc
#include <RAVEN_defines.hpp>
#include <RAVEN_classes.hpp> // For "Waypoint" etc
#include <RAVEN_utils.hpp> // For 'wrap'
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Point.h"
#include <ctime>
#include "ObsWrapper.h"
#include "pose_listener/ReturnObsArrays.h"
#include <turtlesim/Pose.h>
#include <pose_listener/trajId.h>

//#include <featuremsgs/posefeature.h>


class ObsWrapper;
enum Topic_T {POSE, VEL, NONE}; // Topic Type when querying master for topics.

class PoseListener {

public:

	PoseListener();
	virtual ~PoseListener();

	double dist(boost::array<double, 2> pt1, boost::array<double,2> pt2);
	void setFreqSample(int freq);
	void setMaxNumObs(int mMaxNumObs);
	
	void sampleCallback(const geometry_msgs::PoseStamped &msg);
	void trajIdCallback(const pose_listener::trajId &msg);
	
	void poseCallback(const geometry_msgs::PoseStamped &msg);
	void velCallback(const geometry_msgs::TwistStamped &msg);
	void updateObstTopics(ros::NodeHandle nhandle, ros::master::V_TopicInfo &allTopics);
	void subscribeObstacle(ros::NodeHandle nhandle, std::string newObstName, bool isSimulated);
	Topic_T checkVehTopic(std::string namePrefix, std::string &obstName, std::string topicName);//inline
	void removeOldObstacles(std::set<std::string> vehNamesFromTopics);
	bool respondToReq(pose_listener::ReturnObsArrays::Request &req, pose_listener::ReturnObsArrays::Response &res);
	bool appendToFile(double timePrevSample, int trajId, float x, float y, float orientation, int ledColor);

	bool getPoseXYZPsi(std::string objectName, boost::array<double, 4> &pose);
	bool getVelXYSpeed(std::string objectName, boost::array<double, 3> &vel);

	std::map <std::string, Obstacle *> getAllVehicles();
	double xmin,xmax,ymin,ymax;
	std::string filename;
	bool shouldSaveData;
private:
	double timePrevSample;
	int freqSample;
	int trajId;
	int ledColor;

	ObsWrapper mObsWrapper;


	std::map <std::string, Obstacle *> knownVehicleMap;
};

#endif /* PoseListener_H_ */
