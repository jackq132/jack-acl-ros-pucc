/*
 * TopicChecker.h
 *
 *  Created on: Sep 12, 2013
 *      Author: shayo
 */

#ifndef TopicChecker_H_
#define TopicChecker_H_


#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class TopicChecker {
	public:
		TopicChecker();
		virtual ~TopicChecker();
		void handle_timeout(boost::system::error_code const& cError);

	private:
		boost::asio::io_service io;
		boost::asio::deadline_timer deadlineTimer;
};

#endif /* TopicChecker_H_ */
