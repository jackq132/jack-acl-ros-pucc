% Bobby Klein
% 11/29/2012

% Note that we use the 'spline' function to find the interpolated values
% of the spline, rather than 'csapi' which would give a nice smooth plot;
% this is intentional, since current gpucc code is waypoint-based, so
% selecting the same discretization here and there will give an accurate
% view of what the gpuccs will actually do.

function trajs = gen_click_points(boundaryInpFile, trajOutFile)

curMouseCoords = [0,0];
curPoints = zeros(2,2); % Note that we allocate 2 cells, since the final one is modified on mouse move

cur_traj_num = 1; % Number of paths
done_execution = false; % when done execution
%% Load all the files

numInterpPoints = 10; % total number of points to interpolate between each selected point.
% the three given for the cubic spline

xLiveSpline = zeros(1,numInterpPoints); % accumulated spline data for this
yLiveSpline = zeros(1,numInterpPoints);     % traj, updated with mouse clicks

% NOTE: assume the form xminouter, xmaxouter, yminouter, ymaxouter,
% xmininner... lazy
fprintf('Loading boundary input file.\n');
boundaries = importdata(boundaryInpFile);
boundaries = boundaries.data;
%boundaries = boundaries{data}
xmin_outer = boundaries(1);
xmax_outer = boundaries(2);
ymin_outer = boundaries(3);
ymax_outer = boundaries(4);
xmin_inner = boundaries(5);
xmax_inner = boundaries(6);
ymin_inner = boundaries(7);
ymax_inner = boundaries(8);
fprintf('Loaded successfully.\n\n');

fprintf('Add as many trajectories as you want:\n')
fprintf('To end a trajectory, click outside the bounding box area.\n')
fprintf('To stop generating trajectories, click twice outside the bounding box area\n\n')

% Anonymous function to determine if in domain
% Returns true if point is within outermost bonuding box, false otherwise
isInDomain = @(xy) ((xy(1) > xmin_outer & xy(1) < xmax_outer) & ...
                    (xy(2) > ymin_outer & xy(2) < ymax_outer));

% Draw the bounding box


% Initialize the figure according to the bounding box
% and draw the bounding
h_fig = figure(1);
hold on, rectangle ('Position', [xmin_inner,ymin_inner,xmax_inner - xmin_inner,ymax_inner-ymin_inner]);
rectangle ('Position', [xmin_outer,ymin_outer,xmax_outer - xmin_outer,ymax_outer-ymin_outer]);
axis ([xmin_outer-1, xmax_outer+1, ymin_outer-1,ymax_outer+1])

h_spline = plot(xLiveSpline, yLiveSpline);

%
% get current figure event functions
h_mouse_motion = get(gcf, 'windowbuttonmotionfcn');
h_mouse_click = get(gcf,'windowbuttondownfcn');  
h_curr_title = get(get(gca, 'Title'), 'String');
% add data to figure handles
handles = guidata(gca);

handles.h_mouse_motion = h_mouse_motion;
handles.h_mouse_click = h_mouse_click;
handles.h_curr_title = h_curr_title;
handles.theState = uisuspend(gcf);
guidata(gca, handles);

pause on;

% Allow the user to pick the first point
fprintf('Choosing new trajectory %d\n',cur_traj_num)
fprintf('Select first point:\n')
curPoints(1,:) = ginput(1);
curPoints(1,:)
if(~isInDomain(curPoints(1,:))) % check for bizarre input
    fprintf('You clicked outside the bounding box before assigning anything!\n')
    fprintf('Thats how you terminate a trajectory after assigning points, or\n')
    fprintf('if no points have been assigned yet, terminate the program.\n')
    return
end

% set event functions
% Now the program execution is out of our hands... up to the callbacks.
set(gcf,'Pointer','crosshair');
set(gcf, 'windowbuttonmotionfcn', @mouseMoveCallback);
set(gcf, 'windowbuttondownfcn', @mouseClickCallback);  

%% Generate paths

% After user selects first 2 points of a path, value is assumed for
% third point, interpolated spline plotted between three points at timer
% callback intervals (which updates plot data for that handle)
% When user clicks, point is added to that trajectory, permanently draw one
% spline on the graph, and update handle to temporary data to discard
% oldest point and add the newest realtime one.



    function mouseMoveCallback(src, event)
        % executed on each mouse move        
        % read coordinates
        % curMouseCoords = get(0,'PointerLocation'); % pixel coordinates
        curMouseCoords = get(gca, 'CurrentPoint'); % x,y,z coordinates (2 rows for 6 cells..?)
        curMouseCoords = curMouseCoords(1,1:2); % only want items 1 and 2
        curPoints(end,:) = curMouseCoords; % Replace final entry with current mouse position
        %fprintf('Current mouse coords: %d, %d, and current traj:', curMouseCoords(1), curMouseCoords(2));
        [xLiveSpline, yLiveSpline] = interpSpline(curPoints, numInterpPoints);
        %refreshdata % SLOW and not working we've updated the source data for the plot, refresh it.
        set(h_spline,'XData',xLiveSpline,'YData',yLiveSpline);
        
    end

    function mouseClickCallback(src, event)
        mouseCoords = get(gca, 'CurrentPoint'); % x,y,z coordinates (2 rows for 6 cells..?)
        mouseCoords = mouseCoords(1,1:2); % only want items 1 and 2
        if(isInDomain(mouseCoords)) % Add this point to list of current traj points
            fprintf('added point at %4.2f, %4.2f\n', mouseCoords(1), mouseCoords(2));
            curPoints(end,:) = mouseCoords;% Assign final spline point
            curPoints(end+1,:) = zeros(1,2); % Extend by 1 cell (last cell gets modified by mouseMoveCallback)
            fprintf(['Select remaining waypoints; click outside the bounding '...
                'box to end input\n']);
            
        else % Finished with this trajectory
            set(gcf, 'windowbuttonmotionfcn', ''); % suspend mouse motion callback while we do this
            
            finishTraj()
            fprintf('Choosing new trajectory %d\n',cur_traj_num)
            fprintf('Select first point:\n')
            curPoints(1,:) = ginput(1);
            if(~isInDomain(curPoints(1,:))) % terminate program
                set(gcf, 'windowbuttonmotionfcn', ''); % stop listening for mouse
                set(gcf, 'windowbuttondownfcn', '');
                done_execution = true;
                % All trajectories finished, write the file.
                fprintf('\n\n***Finished assigning all %d trajectories***\n\n', length(trajs));
                generateTrajFile(trajs, trajOutFile)
                fprintf('\n\nDone!');
                fprintf('You may now close the figure.');
                return
            end
            set(gcf, 'windowbuttonmotionfcn', @mouseMoveCallback); % resume mouse motion callback
        end
    end

    function [interpX, interpY] = interpSpline(manyPoints, numInterps)
        interpX = linspace(manyPoints(1,1), manyPoints(end,1), numInterps * length(manyPoints));
        interpY = spline(manyPoints(:,1), manyPoints(:,2), interpX);
    end
        

    function finishTraj()
    % We've collected our data for 1 traj, now plot it permanently.
        finalTrajPoints = curPoints(1:end-1,:); % last cell is 0,0
        fprintf('Finished assigning trajectory %d.\n',cur_traj_num);
        length(finalTrajPoints)
        if(length(finalTrajPoints) < 2)
            fprintf('Attempted to finish Trajectory of size < 2!\n')
            return;
        end
        trajs{cur_traj_num} = finalTrajPoints;
        [finalXSplineData{cur_traj_num} finalYSplineData{cur_traj_num}] = interpSpline(finalTrajPoints, numInterpPoints);
        plot(finalXSplineData{cur_traj_num}, finalYSplineData{cur_traj_num}); % plot permanently
        xLiveSpline = zeros(1,numInterpPoints); % reset live spline data
        yLiveSpline = zeros(1,numInterpPoints);
        curPoints = zeros(2,2); % Note that we allocate 2 rows, since the final one is modified on mouse move
        cur_traj_num = cur_traj_num + 1;
    end
    
    function generateTrajFile(trajData, trajFile)
        fprintf('Writing trajectory file %s\n',trajFile);
        fileID = fopen([trajOutFile], 'w');

        fprintf(fileID,['#NB: These MUST be explicitly doubles. i.e. every number', ...
            'should have a decimal. cause XmlRpc is ridiculous.\n']);

        for trajNum = 1: length(trajs)
            fprintf(fileID, 'traj%03d:\n',trajNum-1);
            fprintf(fileID, ' traj: [');
            curTrajPoints = trajData{trajNum};
            numWaypoints = length(curTrajPoints);
            for waypointNum = 1: numWaypoints
                fprintf(fileID, '[%5.3f, %5.2f], ',curTrajPoints(waypointNum,1),curTrajPoints(waypointNum,2));
            end
            fprintf(fileID, '[%5.3f, %5.2f]]\n',curTrajPoints(numWaypoints,1),curTrajPoints(numWaypoints,2));
            fprintf(fileID, ' std: .01\n');
        end
        fclose(fileID);
        fprintf('Finished writing trajectories.\n')
    end

keyboard
return
end


