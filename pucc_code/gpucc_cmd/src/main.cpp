#include <sys/time.h>
#include <semaphore.h>
#include <signal.h>
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include "ros/ros.h"
#include "XmlRpcValue.h"
#include <raven_pucc/waypoint.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include "featuremsgs/posefeature.h"
#include <RAVEN_classes.hpp>
#include <RAVEN_defines.hpp>
#include <RAVEN_utils.hpp>
////#include "ctf_gpucc_cmd/tag.h" //// Could not locate this header
//// the bool 'tagged' causes the gpucc to be treated as 'idle', immediately running for a wall
//// being 'tagged' causes a panic.
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>

#include <time.h>
#include <ros/time.h>
//#include "TimePublisher.h"
//#include "rosgraph_msgs/Clock.h"

using namespace std;

struct TrajFeatStruct{
	vector<int> trajs;
	vector<double> feats;
};

std::vector<RedAgent> redagents;
#define PI (3.14159265358979323)

#define TIME_PUB_FREQ 1000
#define TIME_SCALE 1.0

//TimePublisher timePublisher;

bool isInsideRect(double agentX, double agentY, double xmin, double xmax, double ymin, double ymax){
	if (agentX <= xmax && agentX >= xmin && agentY <= ymax && agentY >= ymin)
		return true;
	return false;
}

double dist(boost::array<double, 2> pt1, boost::array<double,2> pt2){
	return sqrt( (pt1[0]-pt2[0])*(pt1[0]-pt2[0]) + (pt1[1]-pt2[1])*(pt1[1]-pt2[1]) );
}

void featCallback(const featuremsgs::posefeature &msg) {
	std::string vehName = msg.name;

	if(vehName.length() < 1) {
	//		ROS_DEBUG("vehName NULL in poseCallback");
			return;
	}

	for (std::vector<RedAgent>::iterator it = redagents.begin(); it != redagents.end(); it++){
		if (vehName.compare(it->name) == 0){
			it->idle = bool(msg.feats[IDLE_FEAT]);
			it->x = msg.pose.position.x;
			it->y = msg.pose.position.y;
			it->psi = quaternionTo2DAngle(msg.pose.orientation.z, msg.pose.orientation.w);
			break;
		}
	}
}

void parseTrajConfigFile(std::vector< std::vector<boost::array<double, 2> > > &trajectories, std::vector<double> &stddevs) {
	const string trajbase = "/ctf/trajectories/traj";
	int trajitr = 0;
	while(true){ //// In truth, only iterating until if statement below
		ostringstream convert;
		convert << trajbase;
		convert.width(3);
		convert.fill('0');
		convert << trajitr;
		XmlRpc::XmlRpcValue trajectoryXML;
		if (!ros::param::get(convert.str(), trajectoryXML) ){
			break;
		} else { //// Try to parse the trajectory from xml file
			std::vector<boost::array<double, 2> > trajectory;
			ROS_ASSERT(trajectoryXML.getType() == XmlRpc::XmlRpcValue::TypeStruct);
			for(int i = 0; i < trajectoryXML["traj"].size(); i++){
				if(trajectoryXML["traj"][i].getType() != XmlRpc::XmlRpcValue::TypeArray){
					ROS_FATAL("Trajectory %d is not a nested list; has to be [[a,b],[c,d],....,[y,z]]", trajitr);
					exit(0);
				}
				if (trajectoryXML["traj"][i].size() != 2){
					ROS_FATAL("Trajectory %d has a waypoint with != 2 components.", i);
					exit(0);
				}
				if (trajectoryXML["traj"][i][0].getType() != XmlRpc::XmlRpcValue::TypeDouble
					  || trajectoryXML["traj"][i][1].getType() != XmlRpc::XmlRpcValue::TypeDouble){
					ROS_FATAL("Trajectory %d has a non-double datatype. Make sure every number is of the form 0.0", i);
					exit(0);
				}
				boost::array<double, 2> trajpt;
				trajpt[0] = trajectoryXML["traj"][i][0];
				trajpt[1] = trajectoryXML["traj"][i][1];	
				trajectory.push_back(trajpt);
			}
			trajectories.push_back(trajectory);

			if (trajectoryXML["std"].getType() != XmlRpc::XmlRpcValue::TypeDouble){
				ROS_FATAL("Std deviation %d has a non-double datatype. Make sure every number is of the form 0.0", trajitr);
				exit(0);
			}
			stddevs.push_back((double)trajectoryXML["std"]);
		}
		trajitr++;
	}
	if (trajitr == 0){
		ROS_FATAL("No trajectories found - did you make sure to:");
		ROS_FATAL("1) Load /ctf_gpucc_cmd/config/trajectories.yml, giving them /ctf/trajectories/traj*** parameter names?");
		ROS_FATAL("2) Number them starting at 000 with padding width 3?");
		exit(0);
	}


	/* DEBUGGING
	ROS_INFO("Here are the trajectories we loaded:");
	for (int i = 0; i < trajectories.size(); i++){
		ROS_INFO("Trajectory %d", i);
		for (int j = 0; j < trajectories[i].size(); j++){
			std::cout << trajectories[i][j][0] << " " << trajectories[i][j][1] << std::endl;
		}	
	}*/
}
void parseClassConfigFile(vector<TrajFeatStruct> &classes) {
	string classbase = "/ctf/classes/class";
	int classitr = 0;
		ROS_INFO("parsing classes");
	while(true){
		ostringstream convert;
		convert << classbase;
		convert.width(3);
		convert.fill('0');
		convert << classitr;
		XmlRpc::XmlRpcValue classXML;
		ROS_INFO("parsing classes");
		if (!ros::param::get(convert.str(), classXML) ){
			break;
		} else {
			ROS_ASSERT(classXML.getType() == XmlRpc::XmlRpcValue::TypeStruct);
			if (classXML.hasMember("trajs") && classXML.hasMember("feat")){
				TrajFeatStruct tmpstr;
				for (int i = 0; i < classXML["trajs"].size(); i++){
					tmpstr.trajs.push_back(classXML["trajs"][i]);
				}
				for (int i = 0; i < classXML["feat"].size(); i++){
					tmpstr.feats.push_back(classXML["feat"][i]);
				}
				classes.push_back(tmpstr);
			} else {
				ROS_ERROR("Class %d was found to be of invalid format.", classitr);
				ROS_ERROR("Skipping...");
			}
		}
		classitr++;
	}
	if (classitr == 0){
		ROS_FATAL("No classes found - did you make sure to:");
		ROS_FATAL("1) Load /ctf_gpucc_cmd/config/classes.yml, giving them /ctf/classes/class*** parameter names?");
		ROS_FATAL("2) Number them starting at 000 with padding width 3?");
		exit(0);
	}
}

void interpolateWaypoints(std::vector< std::vector< boost::array<double,2> > > &trajectories) {
	double wp1X, wp1Y, wp2X, wp2Y, distBtwnWP,wpRatio;

		//Fill in extra waypoints between points
		for(int i = 0; i < trajectories.size(); i++) {
			std::vector<boost::array<double, 2> > *trajectory = &trajectories[i];
			if((*trajectory).size() > 1) {
				// Note the "+1" in the loop initialization below; we check the current iterator
				// waypoint against the one behind (before) it, don't want to go out of bounds.
				// Proceed through loop, checking waypoint at itr to waypoint preceding it in itr-1.
				// If the distance between the waypoints is too great, insert a new waypoint that
				// lies between them, and do the check again beginning with the point just added.
				for (std::vector<boost::array<double, 2> >::iterator itr = (*trajectory).begin()+1; itr != (*trajectory).end(); ++itr) {
					boost::array<double, 2> wp1 = *(itr-1);
					boost::array<double, 2> wp2 = *itr;
					distBtwnWP = dist(wp1, wp2);
					wpRatio = distBtwnWP / MAX_WP_DIST;
					if(wpRatio > 1) {
						boost::array<double, 2> trajpt;
						trajpt[0] = wp1[0] + (wp2[0] - wp1[0]) / wpRatio;
						trajpt[1] = wp1[1] + (wp2[1] - wp1[1]) / wpRatio;
						itr = (*trajectory).insert(itr, trajpt); // Since the iterator was invalidated
						//ROS_INFO("Too sparse, for (%f,%f) to (%f,%f), added wp (%f,%f)",wp1[0],wp1[1],wp2[0],wp2[1],trajpt[0],trajpt[1]);
					}
				}
			}
		}
}


// Main block
int main( int argc, char *argv[] )
{
	srand(time(NULL));
	ros::init(argc, argv, "ctf_gpucc_cmd");
	ros::NodeHandle nhandle;
	ROS_INFO("Starting CTF gpucc command module...");
	ROS_INFO("Getting the list of trajectories from the parameter server...");
	ROS_INFO("Note - max allowable number of trajectories in the YAML file is 999.");
	ROS_INFO("This can be extended by doing smarter trajectory name parsing.");

	//get trajectories

	std::vector< std::vector<boost::array<double, 2> > > trajectories;
	std::vector< double > stddevs;
	vector<TrajFeatStruct> classes;

	parseTrajConfigFile(trajectories, stddevs);
	parseClassConfigFile(classes);

	ROS_INFO("Getting domain boundaries...");
	double xmin_outer, xmax_outer, ymin_outer, ymax_outer;
	double xmin_inner, xmax_inner, ymin_inner, ymax_inner;
	double outerlanebuffer; //// Below 'boundaries' is the yml file in config/
	if (!ros::param::get("/ctf/boundaries/xmin_outer", xmin_outer) || 
	    !ros::param::get("/ctf/boundaries/xmin_inner", xmin_inner) ||
	    !ros::param::get("/ctf/boundaries/xmax_outer", xmax_outer) ||
	    !ros::param::get("/ctf/boundaries/xmax_inner", xmax_inner) ||
	    !ros::param::get("/ctf/boundaries/ymin_outer", ymin_outer) || 
	    !ros::param::get("/ctf/boundaries/ymin_inner", ymin_inner) ||
	    !ros::param::get("/ctf/boundaries/ymax_outer", ymax_outer) ||
	    !ros::param::get("/ctf/boundaries/ymax_inner", ymax_inner) ||
	    !ros::param::get("/ctf/boundaries/outerlanebuffer", outerlanebuffer) ){
		ROS_FATAL("Couldn't find all of the required boundary constants on the parameter server.");
		ROS_FATAL("/ctf/boundaries/[xmin_outer, xmin_inner, xmax_outer, xmax_inner, ymin_outer, ymin_inner, ymax_outer, ymax_inner] and outerlanebuffer required");
		exit(0);
	}
	
	ROS_INFO("Checking trajectory validity...");
	ROS_INFO("(Each trajectory must have a starting point and ending point outside inner rect and inside outer rect.)");
	for (int i = 0; i < trajectories.size(); i++){
		boost::array<double, 2> startpt = trajectories[i].front();
		boost::array<double, 2> endpt = trajectories[i].back();
		if (isInsideRect(startpt[0], startpt[1], xmin_inner, xmax_inner, ymin_inner, ymax_inner) ||
		    !isInsideRect(startpt[0],startpt[1], xmin_outer, xmax_outer, ymin_outer, ymax_outer) ){
			ROS_FATAL("Trajectory %d did not satisfy the above criteria.", i);
			exit(0);
		}
	}

	interpolateWaypoints(trajectories);

	////REMOVED; NO 'tag.h'
////	ros::Subscriber tagsub = nhandle.subscribe("ctf_gpucc_cmd/tag", 1000, tagCallback);

	ROS_INFO("Trajectories are valid, starting main loop.");
/*	ros::Time start_time_ = ros::WallTime::now();
	timePublisher.setTime(start_time);
	timePublisher.setTimeScale(TIME_SCALE);
	timePublisher.setPublishFrequency(TIME_PUB_FREQ);
*/

	ros::Rate loop_timer(10);
	
	// TODO Move this somewhere more reasonable, eg to the same place as the map from
	// Vehicle names to numbers
	std::map<std::string, std::string> aprilTags;
	aprilTags["GP04"] = "2";
	aprilTags["GP09"] = "0";
	aprilTags["GP07"] = "4";
	bool firstRun = true;


	boost::mt19937 RNG(time(NULL));
	while(ros::ok()){
		//process all callbacks	
		ros::spinOnce();

		//****
		//The following code finds new gpuccs dynamically, subscribes to their topics, and releases them when no longer there
		//****


		//query master for list of nodes
		  ros::V_string nodenames;
		  ros::master::getNodes(nodenames);
		  //add any nodes that we are missing
		  for (int i = 0; i < nodenames.size(); i++){
			//get the name of the node
			if (nodenames[i].find("/GP_controller_") != std::string::npos){
				std::string agentname = nodenames[i].substr(15, nodenames[i].length()+1); //15 because "/GP_controller_" is 15 chars long
				//loop over agents we know about and see if it's there
				bool found = false;
				for (int j = 0; j < redagents.size(); j++){
					if (redagents[j].name.compare(agentname) == 0){
						found = true;
						break;
					}
				}
				if (!found){
					ROS_INFO("New agent detected and added: %s", agentname.c_str());
					RedAgent newagent;
					newagent.name = agentname;
					newagent.x = newagent.y = newagent.psi = -1e10; //// Recall pt[0], pt[1] is x,y
					newagent.idle = false; //intialize idle to 0
                    //initialize team to 0; checked on each msg in case of dynamic team
                    newagent.team = 0; 
////					newagent.tagged = false; ////REMOVED; NO 'tag.h'
					//subscribe to its feature topic (which includes self-reported pose)
                    std::string topicname = "/"+agentname;
					topicname.append("/features");
					ROS_INFO("Subscribing to: %s", topicname.c_str());
				 	newagent.featsub = nhandle.subscribe(topicname, 1000, &featCallback);

					//publish to its waypoint topic
				 	topicname = "/"+agentname;
					topicname.append("/waypts");
					ROS_INFO("Advertising on: %s", topicname.c_str());
					newagent.wptpub = nhandle.advertise<raven_pucc::waypoint>(topicname, 1000);

					redagents.push_back(newagent);
				}
			}
		   }

		   //remove any nodes that we no longer need
		   //// Nested since compare all combinations of agents and nodenames
		   for (int i = 0; i < redagents.size(); i++){
		      bool found = false;
		      for (int j = 0; j < nodenames.size(); j++){
			  //get the name of the node
			  if (nodenames[j].find("/GP_controller_") != std::string::npos){
				  std::string agentname = nodenames[j].substr(15, nodenames[j].length()); //15 because "/GP_controller_" is 18 chars long
				  if (agentname.compare(redagents[i].name)==0){
					found = true;
					break;
				  }
			  }
		      }
		      if (!found){ //// If, for this redagent, iterated over all nodenames without match:
			 //subscribers automatically unsubscribe when they go out of scope
			 //.....who knows if that actually works....
			  ROS_INFO("Agent lost and deleted: %s", redagents[i].name.c_str());
			 redagents[i].featsub.shutdown();
			 redagents.erase(redagents.begin()+i);
			i--;
		      }
		   }

		//******
		//End of gpucc subscription maintenance code
		//****** 
		

		//here we assign gpuccs trajectories, and monitor positions to see when they're done moving along their trajectories
		//once they're done, we recycle them and move them around the border to another location
		//so that they re-emerge elsewhere with a new trajectory
		for (int i = 0; i < redagents.size(); i++){
			if (isInsideRect(redagents[i].x, redagents[i].y, xmin_inner, xmax_inner, ymin_inner, ymax_inner) ){
				if (redagents[i].idle == true /*|| redagents[i].tagged*/){ ////REMOVED; NO 'tag.h'
					//panic! i'm in the middle of the domain and doing absolutely nothing
					//find the closest boundary and shoot right for it
					vector< boost::array<double, 3> > dests;
					boost::array<double, 3> destL = {xmin_inner-outerlanebuffer, redagents[i].y, PI};
					dests.push_back(destL);
					boost::array<double, 3> destR = {xmax_inner+outerlanebuffer, redagents[i].y, 0};
					dests.push_back(destR);
					boost::array<double, 3> destB = {redagents[i].x, ymin_inner-outerlanebuffer, -PI/2.0};
					dests.push_back(destB);
					boost::array<double, 3> destT = {redagents[i].x, ymax_inner+outerlanebuffer, PI/2.0};
					dests.push_back(destT);
					double dists[4] = {abs(redagents[i].x-xmin_inner),
							   abs(redagents[i].x-xmax_inner),
							   abs(redagents[i].y-ymin_inner),
							   abs(redagents[i].y-ymax_inner)};
					double mindist = numeric_limits<double>::max();
					int minind = -1;
					for (int j = 0 ; j < dests.size(); j++){
						if (dists[j] < mindist){
							minind = j;
							mindist = dists[j];
						}
					}

					//give the robot the waypoint of a lifetime.
					raven_pucc::waypoint msg;
					msg.header.stamp = ros::Time::now();
					msg.name = redagents[i].name;
					msg.x = dests[minind][0];
					msg.y = dests[minind][1];
					msg.psi = dests[minind][2];
					msg.speed = 0.2; //roughly the fastest the gpucc can go
					msg.clear = WPTS_CLEAR; //0 append, 1 replace
					msg.feats.resize(9, 0.0); //fill with all 0 features
					redagents[i].wptpub.publish(msg);
					redagents[i].idle = false; // immediately overwritten by feature callback
					ROS_INFO("GPUCC %s was idle/tagged inside rect, gave it a panic trajectory.", redagents[i].name.c_str());

				} 
				//otherwise, i am following a trajectory. just leave me alone for now
			} else { //i'm outside the inner rectangle
				ROS_INFO("%s says: Outside the inner rectangle!!!", redagents[i].name.c_str());
				if ((redagents[i].idle == true)&&(firstRun==true)){
firstRun = false;
					ROS_INFO("%s says: I'm done the trajectory. Assigning a new one!!!", redagents[i].name.c_str());
					//i'm outside the domain waiting for something to do.
					//assign a trajectory
					//just do random choice based on class
					int numclasses = classes.size();
					int newclass = rand()%numclasses;
					int numtrajs = classes[newclass].trajs.size();
					int newtraj = rand()%numtrajs;
					newtraj = classes[newclass].trajs[newtraj];

					boost::array<double, 2> newstart = trajectories[newtraj].front();
					int ccwindexafternewstart;
					int cwindexafternewstart;
					if (newstart[0] < xmin_inner){
						ccwindexafternewstart = 1;
						cwindexafternewstart = 0;
					} else if (newstart[0] > xmax_inner){
						ccwindexafternewstart = 3;
						cwindexafternewstart = 2;
					} else if (newstart[1] < ymin_inner){
						ccwindexafternewstart = 2;
						cwindexafternewstart = 1;
					} else {
						ccwindexafternewstart = 0;
						cwindexafternewstart = 3;
					}


					//decide whether clockwise or counterclockwise is the best thing to do
					//construct an outer path to append to that trajectory to get from where i am now to the traj
					//the below dests are the 4 corners + buffer (in ccw order starting from top left
					vector< boost::array<double, 2> > corners;
					boost::array<double, 2> destTL = {xmin_inner-outerlanebuffer, ymax_inner+outerlanebuffer};
					corners.push_back(destTL);
					boost::array<double, 2> destBL = {xmin_inner-outerlanebuffer, ymin_inner-outerlanebuffer};
					corners.push_back(destBL);
					boost::array<double, 2> destBR = {xmax_inner+outerlanebuffer, ymin_inner-outerlanebuffer};
					corners.push_back(destBR);
					boost::array<double, 2> destTR = {xmax_inner+outerlanebuffer, ymax_inner+outerlanebuffer};
					corners.push_back(destTR);

					int ccworder[4];
					if (redagents[i].x < xmin_inner){ //// outside the L boundary
						ccworder[0] = 1;
						ccworder[1] = 2;
						ccworder[2] = 3;
						ccworder[3] = 0;
					} else if (redagents[i].x > xmax_inner){//// outside R boundary
						ccworder[0] = 3;
						ccworder[1] = 0;
						ccworder[2] = 1;
						ccworder[3] = 2;
					} else if (redagents[i].y < ymin_inner){//// outside B boundary
						ccworder[0] = 2;
						ccworder[1] = 3;
						ccworder[2] = 0;
						ccworder[3] = 1;
					} else {				//// outside T boundary
						ccworder[0] = 0;
						ccworder[1] = 1;
						ccworder[2] = 2;
						ccworder[3] = 3;
					}

					//set up rng
//					boost::normal_distribution<> tmpnormdist(0, stddevs[newtraj]);
//					boost::variate_generator< boost::mt19937&, boost::normal_distribution<> > 
//										normrndgen(RNG, tmpnormdist);
					//check counterclockwise distance first
					double ccwdist = 0, cwdist = 0;
					vector< boost::array<double, 2> > ccwdests, cwdests;
					boost::array<double, 2> initialDest = {redagents[i].x, redagents[i].y};
					ccwdests.push_back(initialDest);
					for (int j = 0 ; j < 4; j++){
						if (ccwindexafternewstart == ccworder[j]){
							//we're on the same section of the border as the start, just add the waypoint.
							ccwdist += dist(newstart, ccwdests.back());
							for (int k = 0; k < trajectories[newtraj].size(); k++){
								//add noise to the waypoints here.
								boost::array<double, 2> tmppt = trajectories[newtraj][k];
//								tmppt[0] += normrndgen(); 
//								tmppt[1] += normrndgen();
								ccwdests.push_back(tmppt);
//								ROS_INFO("Added ccw point (%f,%f) to traj for robot %s.",tmppt[0],tmppt[1],redagents[i].name.c_str());
							}
							break;	
						} else {
							ccwdist += dist( corners[ ccworder[j] ], ccwdests.back());
							ccwdests.push_back(corners[ ccworder[j] ]);
						}
					} 
					//now clockwise
					cwdests.push_back(initialDest);
					for (int j = 3; j >= 0; j--){
						if (cwindexafternewstart == ccworder[j]){
							//we're on the same section of the border as the start, just add the waypoint.
							cwdist += dist(newstart, cwdests.back());
							for (int k = 0; k < trajectories[newtraj].size(); k++){
								//add noise to the waypoints here.
								boost::array<double, 2> tmppt = trajectories[newtraj][k];
//								tmppt[0] += normrndgen(); 
//								tmppt[1] += normrndgen();
								cwdests.push_back(tmppt);
//								ROS_INFO("Added cw point (%f,%f) to traj for robot %s.",tmppt[0],tmppt[1],redagents[i].name.c_str());
							}
							break;	
						} else {
							cwdist += dist( corners[ ccworder[j] ], cwdests.back());
							cwdests.push_back(corners[ ccworder[j] ]);
						}
					}

					/******* For now, always go clockwise ******/

//					//choose the path with the minimum distance
//					if (ccwdist < cwdist){
//
//						for (int j = 0; j < ccwdests.size(); j++){
//							pucc::waypoint msg;
//							msg.header.stamp = ros::Time::now();
//							msg.header.frame_id = redagents[i].name;
//							msg.name = redagents[i].name;
//							msg.x = ccwdests[j][0];
//							msg.y = ccwdests[j][1];
//							msg.psi = 0;
//							msg.speed = 0.2; //roughly the fastest the gpucc can go
//							msg.clear = 0; //0 append, 1 replace
//							msg.feats = classes[newclass].feats;
//							redagents[i].wptpub.publish(msg);
//						}
//					} else {
						for (int j = 0; j < cwdests.size(); j++){
							raven_pucc::waypoint msg;
							msg.header.stamp = ros::Time::now();
							msg.header.frame_id = redagents[i].name;
							msg.name = redagents[i].name;
							msg.x = cwdests[j][0];
							msg.y = cwdests[j][1];
							msg.psi = 0;
							msg.speed = 0.2; //roughly the fastest the gpucc can go
							msg.clear = 0; //0 append, 1 replace
							msg.feats = classes[newclass].feats;
							redagents[i].wptpub.publish(msg);
						}
//					}
					redagents[i].idle = false;
////					redagents[i].tagged = false;
//					ROS_INFO("GPUCC %s was idle outside rect.\n Gave it a recycle trajectory followed by class %d, trajectory %d.", redagents[i].name.c_str(), newclass, newtraj);
					ROS_INFO("%s,%s,%d,%d",redagents[i].name.c_str(), (aprilTags[redagents[i].name]).c_str(),newclass, newtraj);
					ROS_INFO("%s says: I assigned a new one!!!", redagents[i].name.c_str());
				}
				//otherwise i'm on my way to the next trajectory entrance, leave me be
			}//end if/else inside rect
		} //end for agents to do path checking
		
		loop_timer.sleep();
    }//while rosok
	ROS_INFO("ctf cmd shutting down.");
	
	//## Return
	return(1);
}

