/*
 * Gpucc.cpp
 *
 *  Created on: Oct 11, 2012
 *      Author: bobby
 *
 *    Modified: March 2013
 *
 *
 * Contains all functions specific to the 'gpucc' iRobot Create.
 * See specific comments for each function.
 *
 * Note that client code for gpuccs belongs in GpuccMain.cpp - Gpucc.cpp
 * code primarily performs hardware functions and should not need modification.
 */

#ifndef GPUCC_H_
#define GPUCC_H_

#include "ros/ros.h"
#include <RAVEN_defines.hpp> // located in 'include' directory of this project
#include <RAVEN_classes.hpp>
#include <RAVEN_utils.hpp>
#include "../Pucc.h"
#include "createoi.hpp" // For things like LED_PLAY


// no batt voltage
// no bt
// additional camera
// sensor list

class Gpucc : public Pucc
{

protected:
    SensorList*           sensor;	// Sensor list, via createoi.hpp
    SerialPort*           serial;  	// serial port

    unsigned char playLEDIntensity;	// Current intensity of the playLED, 255 max, 0 off.
    unsigned char playLEDColor;		// 0-255 color setting for LED
    double togglePeriod;			// The period between flashes of the light
    ros::Timer ledToggleTimer;		// Manages the time between on/off toggling of LED

//    JoyXbox joystick;             // Legacy variable - no additional includes s/b needed

public:
    Gpucc();
    ~Gpucc();

    /* Used for initialization and shutdown of Open Interface (OI) to gpucc */
        int  start_OI();
		bool setupHardware(int argc, char *argv[], int argcnt, std::string puccname, std::string usbPortLoc);
        int  stop_OI();


	/* *********** Accessor and assignment methods *********** */
		int* getAllSensors();
		int  readSensor(oi_sensor packet);
		void readSensors();
		int  readRawSensor(oi_sensor packet, unsigned char* buffer, int size);
		std::string getPuccType();

    /* ******************** Mutator methods ******************** */
        int  setLED(oi_led lflags, unsigned char clr, unsigned char intensity);
        int  setBaud(int rate);
        int  setDigitalOuts(oi_output oflags);
        int  setPWMLowSideDrivers(unsigned char pwm0, unsigned char pwm1, unsigned char pwm2);
        int  setLowSideDrivers(oi_output oflags);
        int  sendIRbyte(unsigned char irbyte);
        int  writeSong(unsigned char number, unsigned char length, unsigned char* song);
        int  playSong(unsigned char number);
        void sendDriveCommands();


    /* ****************** Computation methods ******************* */
        int  getWheelBase() { return GPUCC_WHEELBASE;}
        void simulateDynamics(double dt);
        void scaleDriveCommands();
        /* TODO UNTESTED */
//		void joyCommands();

	/* **************** Located in GpuccMain.cpp **************** */
							/* CLIENT CODE */
        void puccMain(ros::NodeHandle &nhandle);
		void assignLEDs(ros::NodeHandle nhandle);
		void updateLEDDuration(ros::NodeHandle &nhandle);
		void toggleLEDCallback(const ros::TimerEvent& timerEvent);



};

#endif
