/*
 * Gpucc.cpp
 *
 *  Created on: Oct 11, 2012
 *      Author: bobby
 *
 *    Modified: March 2013
 *
 *
 * Contains all functions specific to the 'gpucc' iRobot Create.
 * None of these methods should need modification - client code
 * belongs in GpuccMain.cpp.
 *
 * See specific comments for each function.
 */

#include "ros/ros.h"
#include "Gpucc.h"


Gpucc::Gpucc() : Pucc()
{
    togglePeriod = 1.0; 		// Default to 1.0 second flashing
    playLEDIntensity = 255;	  	// Current intensity of the playLED, 255 max, 0 off.
    playLEDColor = 254; 		// Set the initial color to green
}

Gpucc::~Gpucc()
{
	if (!this->isSimulated()){
        stop();
		stop_OI();
		serial->spClose();
	}
}

/* Used to control the robot remotely. */
/*
void GPUCC::joyCommands()
{
	double throttle = joystick.axis_left_y*500;
	double steering = joystick.axis_left_x*500;

	if(fabs(steering) < 50) {
		steering = 0.0;
	}
	if(fabs(throttle) < 50) {
		throttle = 0.0;
	}

	throttle = saturate(throttle,-500,500);
	steering = saturate(steering,-500,500);

	//simulate min turning radius constraint
	if (fabs(steering) > fabs(throttle)*2*GPUCC_RADIUS/MIN_RO) {
		if (steering >= 0.0 ) {
			steering = fabs(throttle)*2*GPUCC_RADIUS/MIN_RO;
		}
		else { //ugly way of doing signum
			steering = -1.0*fabs(throttle)*2*GPUCC_RADIUS/MIN_RO;
		}
	}

	control.vL = throttle + steering/2.0;
	control.vR = throttle - steering/2.0;

	double temp;
	if (throttle < 0) {
		temp = control.vL;
		control.vL = control.vR;
		control.vR = temp;
	}

}
*/


/* Start serial communications with the robot.
 * Always return 1, automatic success */
int Gpucc::start_OI()
{
	// enterPassiveMode();
	int retval;
	unsigned char cmd[1];
	cmd[0] = OPCODE_START;
	retval = serial->spSend(cmd,1);
	if(retval < 0 )
		ROS_ERROR("ERROR - Could not send first serial command");
	else ROS_INFO("SENT SERIAL CMD");
	usleep(200000);

	// enterSafeMode ();
	cmd[0] = OPCODE_FULL; //OPCODE_SAFE;
	retval = serial->spSend(cmd,1);
	if(retval < 0 )
		ROS_ERROR("ERROR - Could not send second serial command");

	// set the led
	setLED(LED_PLAY,10,255);

	usleep(200000);

	/* TODO - this does not work
	// start the sensor thread
	double tmp = 0.0;
	sensor->shut_down = 0;
	Thread* thSensor = new Thread;
	thSensor->start_member<void,GPUCC,double*,&GPUCC::readSensors>(this,&tmp);
	usleep(100000);
	*/

	return(1);
}

/* Halt the robot and terminate communications.
 * Always returns 1, automatic success */
int Gpucc::stop_OI()
{
	// stop the thing
	stop();

	// stop the sensor thread
//	sensor->shut_down = 1;

	// wait a bit
	usleep(200000);

	// enterPassiveMode();
	unsigned char cmd[1];
	cmd[0] = OPCODE_START;
	serial->spSend(cmd,1);

	return(1);
}

/* If necessary (ie a non-simulated robot), initialize the usb and serial port,
 * return false for failure, return true for success. */
bool Gpucc::setupHardware(int argc, char *argv[], int argcnt, std::string puccname, std::string usbPortLoc)
{
	if(! this->isSimulated()) { // hardware
		ROS_DEBUG("This (%s) is a Hardware Gpucc", puccname.c_str());
	    sensor = new SensorList;
		serial = new SerialPort();
		std::string usbPortLoc;
		if(argcnt+3 < argc){
			usbPortLoc = argv[argcnt+4];
		}
		else {
			ROS_FATAL("Specified hardware gpucc, but failed to provide usb port location");
			return false;
		}
		bool serialPortInitialized = this->serial->spInitialize(usbPortLoc,57600,true);
		if(serialPortInitialized != 1) {
			ROS_INFO("Failed to initialize Serial Port at %s", usbPortLoc.c_str());
			return false;
		}
		else {
			ROS_INFO("Initialized serial port at baudrate 57600");
			this->start_OI();
		}
	} // else take no action
	else ROS_DEBUG("This (%s) is a simulated Gpucc", puccname.c_str());
	return true;
}



/* Update the state variables (x,y,psi) /
 * derived state variables (vel, dx,dy,dpsi) based on timestep of length dt. */
void Gpucc::simulateDynamics(double dt){
	// keep args within Create limits
	control.vR = saturate(control.vR,-500,500);
	control.vL = saturate(control.vL,-500,500);

	//simulate for dt
	double vR = control.vR/1000.0;
	double vL = control.vL/1000.0;
	vel = (vL+vR)/2.0;
	dpsi = (vR-vL)/(GPUCC_WHEELBASE); // is there such a #define?
	vx = vel*cos(psi);
	vy = vel*sin(psi);
	x = x + vx*dt;
	y = y + vy*dt;
	psi = psi + dpsi*dt;
}

/* Send wheel commands via serial port. */
void Gpucc::sendDriveCommands()
{
	// keep args within Create limits

	control.vR = saturate(control.vR,-500,500);
	control.vL = saturate(control.vL,-500,500);

	short vL = (short)control.vL;
	short vR = (short)control.vR;


	unsigned char drive_cmd[5];
	drive_cmd[0] = OPCODE_DRIVE_DIRECT;
	drive_cmd[1] = (vR >> 8) & 0x00FF;
	drive_cmd[2] = vR & 0x00FF;
	drive_cmd[3] = (vL >> 8) & 0x00FF;
	drive_cmd[4] = vL & 0x00FF;

	//printf("drive commands %2x, %2x, %2x, %2x, %2x \n", drive_cmd[0],drive_cmd[1],drive_cmd[2],drive_cmd[3],drive_cmd[4]);

	//ROS_INFO("control.vR: %2.2d--,  control.vL: %2.2d",control.vR,control.vL);
	if( serial->spSend(drive_cmd,5) < 0 ) {
		ROS_ERROR("ERROR - Could not issue 'drive' command!");
	}
}


/* Set the LED(s) specified by lflags (see creatoi.hpp) to the color and intensity
 * specified. */
int Gpucc::setLED(oi_led lflags, unsigned char color, unsigned char intensity)
{
	unsigned char cmd[4];
	cmd[0] = OPCODE_LED;
	cmd[1] = lflags;
	cmd[2] = color;
	cmd[3] = intensity;

	return( serial->spSend(cmd,4) );
}

/* Set Baud rate; return 0 for success, -1 for failure */
int Gpucc::setBaud(int baudRate)
{
	unsigned char cmd[2];
	cmd[0] = OPCODE_BAUD;

	// used to set baud for PC same as baud for Create
	switch (baudRate) {
	case 9600:
	case 19200:
	case 38400:
	case 57600:
		cmd[1] = baudRate;
		break;
	default:
		ROS_ERROR("Could not set baud: Invalid argument");
		return(-1);
	}


	if( serial->spSend(cmd, 2) < 0) {
		ROS_ERROR("Could not set baud");
		return(-1);
	}

	serial->spSetBaud(baudRate);
	usleep (100000);	// sleep for 100ms
	return 0;
}

/* Set digital outputs on iRobot Create (see iRobot Create documentation).
 * Return 0 for success, -1 for failure. */
int Gpucc::setDigitalOuts(oi_output oflags)
{
	unsigned char cmd[2];
	cmd[0] = OPCODE_DIGITAL_OUTS;
	cmd[1] = oflags;

	if( serial->spSend(cmd, 2) < 0) {
		ROS_ERROR("Could not set digital outs");
		return(-1);
	}
	return(0);

}


int Gpucc::setPWMLowSideDrivers(unsigned char pwm0, unsigned char pwm1, unsigned char pwm2)
{
	unsigned char cmd[4];

	//max value is 128
	pwm0 = saturate(pwm0,0,128);
	pwm1 = saturate(pwm1,0,128);
	pwm2 = saturate(pwm2,0,128);

	cmd[0] = OPCODE_PWM_LOW_SIDE_DRIVERS;
	cmd[1] = pwm2;
	cmd[2] = pwm1;
	cmd[3] = pwm0;

	if( serial->spSend(cmd, 4) < 0) {
		ROS_ERROR("Could not set low side driver duty cycle");
		return(-1);
	}
	return(0);

}

int Gpucc::setLowSideDrivers(oi_output oflags)
{
	unsigned char cmd[2];
	cmd[0] = OPCODE_LOW_SIDE_DRIVERS;
	cmd[1] = oflags;

	if( serial->spSend(cmd, 2) < 0) {
		ROS_ERROR("Could not set low side driver state");
		return(-1);
	}
	return(0);
}

int Gpucc::sendIRbyte(unsigned char irbyte)
{
	unsigned char cmd[2];
	cmd[0] = OPCODE_SEND_IR;
	cmd[1] = irbyte;

	if( serial->spSend(cmd, 2) < 0) {
		ROS_ERROR("Could not send IR byte");
		return(-1);
	}
	return(0);
}

/* Write the song with notes stored in song[i], and associate it with song number
 * 'number'.  Length passed as parameter, watch out for segfaults!
 * Return 0 for success, -1 for failure. */
int Gpucc::writeSong(unsigned char number, unsigned char length, unsigned char* song)
{
	unsigned char cmd[3 + 2*length];
	int i;

	cmd[0] = OPCODE_SONG;
	cmd[1] = number;
	cmd[2] = length;

	for (i = 0; i < 2*length; i++)
		cmd[i + 3] = song[i];

	if( serial->spSend(cmd, 3+2*length) < 0) {
		ROS_ERROR("Could not write new song");
		return(-1);
	}
	return(0);
}

/* Play the song associated with 'number' wit the onboard beeper
 * Return -1 for serial failure, 0 for success. */
int Gpucc::playSong(unsigned char number)
{
	unsigned char cmd[2];
	cmd[0] = OPCODE_PLAY_SONG;
	cmd[1] = number;

	if( serial->spSend(cmd, 2) < 0) {
		ROS_ERROR("Could not play song");
		return(-1);
	}
	return(0);
}

/* Read the sensor associated with the sensor id 'packet', return an int
 * containing the value of this sensor.  Note that the size varies for
 * different sensor types, so the client must be careful to cast the int
 * as necessary to signed and unsigned 1-2 byte variables.
 * Return INT_MIN for invalid sensor requests. */
int Gpucc::readSensor(oi_sensor packet)
{
	int result = 0;
	unsigned char* buffer;

	switch (packet) {
		//all one-byte unsigned sensors
	case SENSOR_BUMPS_AND_WHEEL_DROPS:
	case SENSOR_WALL:
	case SENSOR_CLIFF_LEFT:
	case SENSOR_CLIFF_FRONT_LEFT:
	case SENSOR_CLIFF_FRONT_RIGHT:
	case SENSOR_CLIFF_RIGHT:
	case SENSOR_VIRTUAL_WALL:
	case SENSOR_OVERCURRENT:
	case SENSOR_INFRARED:
	case SENSOR_BUTTONS:
	case SENSOR_CHARGING_STATE:
	case SENSOR_DIGITAL_INPUTS:
	case SENSOR_CHARGING_SOURCES_AVAILABLE:
	case SENSOR_OI_MODE:
	case SENSOR_SONG_NUMBER:
	case SENSOR_SONG_IS_PLAYING:
	case SENSOR_NUM_STREAM_PACKETS:
		buffer = (unsigned char*) malloc (sizeof(unsigned char));
		if (NULL == buffer)
			return INT_MIN;
		*buffer = 0;
		if (-1 == readRawSensor (packet, buffer, 1)) {
			free (buffer);
			return INT_MIN;
		}
		result = *buffer;
		break;

		//one-byte signed sensor
	case SENSOR_BATTERY_TEMP:
		buffer = (unsigned char*) malloc (sizeof(char));
		if (NULL == buffer)
			return INT_MIN;
		*buffer = 0;
		if (-1 == readRawSensor (packet, buffer, 1)) {
			free (buffer);
			return INT_MIN;
		}
		result += (char) *buffer;
		break;

		//two-byte signed sensors
	case SENSOR_DISTANCE:
	case SENSOR_ANGLE:
	case SENSOR_CURRENT:
	case SENSOR_REQUESTED_VELOCITY:
	case SENSOR_REQUESTED_RADIUS:
	case SENSOR_REQUESTED_RIGHT_VEL:
	case SENSOR_REQUESTED_LEFT_VEL:
		buffer = (unsigned char*) malloc (2 * sizeof(unsigned char));
		if (NULL == buffer)
			return INT_MIN;
		buffer[0] = 0;
		buffer[1] = 0;
		if (-1 == readRawSensor (packet, buffer, 2)) {
			free (buffer);
			return INT_MIN;
		}
		result += (short) (buffer[1] | (buffer[0] << 8));
		break;

		//any other input is invalid (including packet groups)
	default:
		return INT_MIN;
	}

	free (buffer);
	return result;
}

/* Reads all sensors into the 'sensor' struct associated with this gpucc. */
void Gpucc::readSensors()
{
	int done = 0;
	sensor->distance = 0;
	sensor->angle = 0;

	while( !done ) {
		int *sensors = getAllSensors();

		sensor->time_stamp            = 0.0;
		sensor->distance             += sensors[12];
		sensor->angle                += sensors[13];
		sensor->velocity              = sensors[32];
		sensor->turning_radius        = sensors[33];
		sensor->bumps_and_wheel_drops = sensors[0];
		sensor->cliff_left            = sensors[2];
		sensor->cliff_front_left      = sensors[3];
		sensor->cliff_front_right     = sensors[4];
		sensor->cliff_right           = sensors[5];
		sensor->wall                  = sensors[1];
		sensor->charge                = sensors[18];
		sensor->capacity              = sensors[19];
		sensor->overcurrent           = sensors[7];
		done                          = sensor->shut_down;
	}

}

// TODO: THIS WILL SEGFAULT.  DO NOT RETURN POINTER TO LOCAL OBJECT int result[]
// [[perhaps use syntax << int *result = new int[36]; >> instead. ]]
int* Gpucc::getAllSensors()
{
	unsigned char buf[52];
	int result[36];
	int i, numread;

	if (NULL == result) {
		ROS_ERROR("Could not get all sensors:  Memory allocation failed");
		return NULL;
	}

	bzero(buf, sizeof(buf));
	bzero(result, sizeof(result));

	numread = readRawSensor(SENSOR_GROUP_ALL, buf, 52);
	if (numread < 52) {
		ROS_ERROR("Could not get all sensors:  Read %d bytes of 52",numread);
		return(result);
	}

	//Bumps And Wheel Drops to Buttons
	for (i = 0; i < 12; i++)
		result[i] = buf[i];

	result[12] = (short) ((buf[12] << 8) | buf[13]); // Distance
	result[13] = (short) ((buf[14] << 8) | buf[15]); // Angle
	result[14] = buf[16];			   	             // Charging State
	result[15] = (buf[17] << 8) | buf[18];		     // Voltage
	result[16] = (short) ((buf[19] << 8) | buf[20]); // Current
	result[17] = (char) buf[21];		 	         // Battery Temp

	//Battery Charge to Cliff Right Signal
	for( i=0; i<=6; i++ )
		result[i + 18] = (buf[22 + 2*i] << 8) | buf[23 + 2*i];

	result[25] = buf[36];				        // Cargo Bay DI
	result[26] = (buf[37] << 8) | buf[38];		// Cargo Bay Analog

	// Charging Sources to Number Of Stream Packets
	for (i = 0; i <= 4; i++)
		result[i + 27] = buf[39 + i];

	// Request sensors
	for (i = 0; i <= 3; i++)
		result[32 + i] = (short) ((buf[44 + 2*i] << 8) | buf[45 + 2*i]);

	return(result);
}

int Gpucc::readRawSensor( oi_sensor packet, unsigned char* buffer, int size )
{
	int numread = 0;
	unsigned char cmd[2];
	cmd[0] = OPCODE_SENSORS;
	cmd[1] = packet;

	if( serial->spSend(cmd, 2) < 0) {
		ROS_ERROR("Could not request sensor");
		return(-1);
	}

	numread = serial->spReceive(buffer, size);
	if( numread < 0) {
		ROS_ERROR("Could not read sensor");
		return(-1);
	}

	return(numread);
}

/* This method is ONLY used to get a string representation of this ACTUAL
 * pucc object, and is really for debugging;
 * the preferred way to determine a pucc's type is using the
 * 'getVehIDFromName(std::string vehName)' method in Pucc.cpp, which should
 * produce identical behavior unless a pucc was given a name which did not
 * conform to the standards outlined in RAVEN_defines and elsewhere. */
std::string Gpucc::getPuccType()
{
	return "Gpucc";
}

/* Motion plans are computed for gpuccs by default, so no scaling of motor
 * commands is required. */
void Gpucc::scaleDriveCommands()
{
	return;
}
