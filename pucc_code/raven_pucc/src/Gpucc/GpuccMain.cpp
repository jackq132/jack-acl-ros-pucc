/*
 * GpuccMain.cpp
 *
 * Author: Bobby Klein
 * Oct 2012
 *
 * The function contained here is called on each loop of the Pucc execution code
 * for any Pucc robot specified as a Gpucc -
 * THIS is the place for client code; the Pucc.cpp, main.cpp, etc. should
 * primarily remain "black-boxed."
 *
 * Note that you still must place declarations in Gpucc.h.
 *
 *
 * Place anything unique to a Gpucc here.
 */

#include "ros/ros.h"
#include "Gpucc.h"

/* The 2 variables robot.playLEDIntensity and robot.playLEDColor are redundant
 * with the features vector, since they should always be the same. */
void Gpucc::assignLEDs(ros::NodeHandle nhandle)
{
	// check to make sure the features array is big enough to assign these variables
	if(this->features.size() > LED_INTENSITY_FEAT && this->features.size() > LED_COLOR_FEAT) {
		// double below to emphasize that the feature is stored as a double, though value itself cannot have decimal
		this->features[LED_INTENSITY_FEAT] = (double)(this->playLEDIntensity);
		this->features[LED_COLOR_FEAT] = this->playLEDColor;
	}
}

/* When the timer set according to the member variable 'togglePeriod' triggers
 * its callback, this method is called, flipping the LED intensity between
 * 0 and 254. */
void Gpucc::toggleLEDCallback(const ros::TimerEvent& timerEvent)
{
	// The casts below are to remind you that the data types are a bit wonky
	if(this->playLEDIntensity > 0) this->playLEDIntensity = 0;
	else this->playLEDIntensity = (unsigned char)(254);
//	this->playLEDIntensity = (unsigned char)(255);
	ROS_DEBUG("Set LED for robot %s to be %d", this->getName().c_str(), this->playLEDIntensity);
	// Only actually physically set the LED if not simulated
	if(!this->isSimulated()) this->setLED(LED_PLAY, this->playLEDColor, this->playLEDIntensity);
}

/* Updates the timer which toggles the LED on and off, based on the
 * 'features' vector. */
void Gpucc::updateLEDDuration(ros::NodeHandle &nhandle)
{
	if(this->features.size() > 0) {// Make sure the robot has been assigned features
		double LEDDur = this->features[LED_DURATION_FEAT];
		if(this->togglePeriod != LEDDur) {
			//ROS_INFO("Setting new toggle period to be %f", LEDDur);
			this->togglePeriod = LEDDur;
			ros::Duration toggleDur = ros::Duration(LEDDur);
			this->ledToggleTimer = nhandle.createTimer(toggleDur, &Gpucc::toggleLEDCallback, this);
			this->playLEDColor = (unsigned char)(this->features[LED_COLOR_FEAT]);
		}
	}
}

/* This method is called on every loop iteration.
 * This is where client code for Gpuccs should go. */
void Gpucc::puccMain(ros::NodeHandle &nhandle)
{
	// LED duration (blinking) can change at any time based on 'features' of robot,
	// set by features field of pose
	updateLEDDuration(nhandle);
	assignLEDs(nhandle);
}
