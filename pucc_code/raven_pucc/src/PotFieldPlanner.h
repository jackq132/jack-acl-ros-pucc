/*
 * PotFieldPlanner.h
 *
 *  Created on: Oct 11, 2012
 *      Author: bobby, modified from old lab code
 *
 *    Modified: March 2013
 *  (Formerly carrotGen)
 *
 *  Potential field implementation of collision avoidance.
 *  Not particularly robust, since potential field assumes all objects are
 *  static, and is not loop-free.
 *  It is very simple in concept though.
 *
 *  Note that objects farther away than 'droneBuffer' in carrotGen() don't
 *  even get penalized
 *
 *  Note that if desired, the functions here could be easily made static by
 *  isolating the member 'carrot' in plan() and the members Ihdg, Irange in
 *  controller()
 */

#ifndef POTFIELDPLANNER_H_
#define POTFIELDPLANNER_H_

#include "ros/ros.h" // ONLY NECESSARY if carrots are published, which at moment, they are not.
#include "Pucc.h"
#include "MotionPlanner.h"

class PotFieldPlanner : public MotionPlanner {
public:
    Integrator            Ihdg;			// For integral portion of PI heading controller at low speed
    Integrator            Irange;		// For integral portion of PI distance controller at low speed
    Waypoint carrot;

    double velFilt;
    bool aligned;
	//TODO try bringing vel_integrator_const back up to ~ 1, same with hdg

	double SECOND_SPEED_LIMIT; // Attempting to eliminate some magic numbers in carrotgen
	double HDG_SPEED_SAT_RATIO;
	double VEL_ERROR_SAT;
	double VEL_INTEGRATOR_CONST; // 1.0 good for regular speed, 2.0 good for 1-2 x speed
	double HDG_INTEGRATOR_CONST;
	double Kp_hdg;
	double MAX_WHEEL_VEL;
	double SPEED_LIMIT;
	double MIN_RO;
	double WP_TOL;
	double KR;
	double KTH;
	double TLOOK;
	double WRISK;
	double VEL_MIN;

	ros::Publisher carrotpub;


	PotFieldPlanner(ros::NodeHandle &nhandle, std::string newRobotName);
	virtual ~PotFieldPlanner();
	// returns drive command computed by the PotFieldPlanner
	virtual boost::array<short,2> plan(Waypoint curState, Waypoint goal, int vehID);
	virtual bool reachedWaypoint(double dx, double dy);

	/* Get parameters from the parameters server necessary for this motion planner:
	 * for example, waypoint tolerance before declaring victory, gains, etc.
	 */
	virtual void getParameters(std::string robotName);

	void carrotGen(Waypoint pos, Waypoint goal, std::map<std::string, Obstacle *> obstacleMap, int myID);
	boost::array<short,2> controller(Waypoint pos, Waypoint goal);

/*
	// Upucc Constants
	static double SECOND_SPEED_LIMIT = 0.11; // Attempting to eliminate some magic numbers in carrotgen
	static double HDG_SPEED_SAT_RATIO = 5.0/11.0; // to agree with upucc
	static double VEL_ERROR_SAT = 0.1;
	static double VEL_INTEGRATOR_CONST = 1.00; // 1.0 good for regular speed, 2.0 good for 1-2 x speed
	static double HDG_INTEGRATOR_CONST = 1.00;
	static double Kp_hdg = 0.08;
*/

};

#endif /* POTFIELDPLANNER_H_ */
