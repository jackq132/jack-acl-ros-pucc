/*
 * Pucc.cpp
 *
 *  Created on: Oct 11, 2012
 *      Author: bobby
 */

#include "ros/ros.h"
#include "Pucc.h"
#include <boost/algorithm/string.hpp>

std::map <std::string, double> Pucc::vehicleRadii;
StrMap Pucc::vehicleTypes;
std::string Pucc::PARAM_NAMESPACE = "";// As specified in launch file
//which loads params onto server. Currently we have no namespace.

Pucc::Pucc() {
	cmdtype = WAYPOINT_FOLLOW;


//	lastVisitedWaypointId=-1;

	x = y = psi = 0;
	vx = vy = dpsi = 0;
	vel = dpsiref = velref = 0;

	name = "N/A";
	ID   = -1;
	uniqueID = 0;

	idle = true;

	control.vel = control.psi = control.vL = control.vR   = 0;

//	turnMode = 0;
	followMode = -1;

//	vfd_past = 1;

	//target = new Waypoint;
	target.x = target.y = target.psi = target.speed = 0;

	isSimul = true;

	lastWaypoint.x = 999;
	lastWaypoint.y = 999;
    lastWaypoint.psi = 999;
	lastReceivedWaypointId=-1;

	features.resize(9, 0.0); // initialize with all zeros
	features[IDLE_FEAT] = double(true); // Start with idle robot


	// add any static obstacles
	/* e.g. the pole
	Obstacle pole;
	strcpy(pole.name,"pole");
	pole.id     = -1; // -1 indicates static obstacle
	pole.x      = -0.0105;
	pole.y      = 3.3155;
	pole.vx     = 0.0;
	pole.vy     = 0.0;
	pole.radius = 0.3305;
	// obstacles.add(pole);
	 */

}

Pucc::~Pucc() {
	if(motionPlanner != NULL) delete(motionPlanner);
	// delete(obstacleMap);
	// Obstaclemap should be automatically deallocated (no 'new' keyword)
	// and the obstacles contained in it are automatically destructed.
}



void Pucc::setName(std::string str)
{
	name = str;
}

std::string Pucc::getName()
{
	return name;
}

void Pucc::setID(int id)
{
	ID = id;
}

int Pucc::getID()
{
	return ID;
}

void Pucc::setSim(bool simValue)
{
	isSimul = simValue;
}

void Pucc::getState(double &tx, double &ty, double &tpsi, double &tvel)
{
	tx   = x;
	ty   = y;
	tpsi = psi;
	tvel = vel;
}

void Pucc::getState(Waypoint &curState)
{
	curState.x = x;
	curState.y = y;
	curState.psi = psi;
	curState.speed = vel;
}

void Pucc::getVel(double &tvx, double &tvy, double &tdpsi)
{
	tvx = vx;
	tvy = vy;
	tdpsi = dpsi;
}

Command_T Pucc::getCommandType()
{
	return cmdtype;
}

bool Pucc::isSimulated()
{
	return isSimul;
}

void Pucc::getGoal(Waypoint &goal)
{
	goal = goalsVect.at(0);
}

void Pucc::getTarget(double &tx, double &ty, double &tpsi, double &tvel)
{

	tx   = this->target.x;
	ty   = this->target.y;
	tpsi = this->target.psi;
	tvel = this->target.speed;

}

void Pucc::setTarget(double tx, double ty, double tpsi, double tvel)
{

	this->target.x = tx;
	this->target.y = ty;
	this->target.psi= tpsi;
	this->target.speed = tvel;

}


void Pucc::setPose(double tx, double ty, double tpsi){
	x    = tx;
	y    = ty;
	psi  = tpsi;
}


void Pucc::setVel(double vtx, double vty, double dtpsi, double tvel){
	vel  = tvel;
	vx   = vtx;
	vy   = vty;
	dpsi = dtpsi;
}

/* Sets the reference velocity to v and reference angular velocity to psidot.
 * Automatically changes cmdtype from whatever its old value was (eg waypoints)
 * to velocity / angular velocity following.
 */
void Pucc::setVPsidotRef(double v, double psidot){
	cmdtype = V_PSI_DOT_REF; //adding a reference v/dpsi automatically causes v/psidot control
	velref = v;
	dpsiref = psidot;
}

/* Adds a new waypoint; automatically changes cmdtype from from whatever its old
 * value was (eg V_PSI_DOT) to waypoint following.
 * If wptAddingMode parameter == WPTS_CLEAR, remove all previous goals and add this one.
 * If wptAddingMode parameter == WPTS_APPEND, add this waypoint to the tail of our waypoint queue.
 */
void Pucc::addWaypoint(Waypoint_Clear_T wptAddingMode, double tx, double ty, double tpsi, double tspeed, std::vector<double> feats)
{
	cmdtype = WAYPOINT_FOLLOW; //adding a waypoint automatically sets control to waypoint
	idle = false;
	Waypoint new_goal;
	new_goal.x = tx;
	new_goal.y = ty;
	new_goal.psi = tpsi;
	new_goal.speed = tspeed;
	new_goal.id=++lastReceivedWaypointId;
	new_goal.feats = feats;

	if(wptAddingMode == WPTS_APPEND) { // add this waypiont to the end of the goalsVect queue
		if(goalsVect.size() < MAX_NUM_WAYPOINTS) {
			goalsVect.push_back(new_goal);
		}
		else {
			ROS_INFO("Maximum # of waypoints (%d) reached\n", MAX_NUM_WAYPOINTS);
		}
	}
	else if(wptAddingMode == WPTS_CLEAR){ // clear all previous waypoints, add this one
		goalsVect.clear();
		goalsVect.push_back(new_goal);
	}
	else ROS_ERROR("Attempted to add waypoint: ++ x=%.2f, y=%.2f, ++ with an unrecognized waypoint adding mode.  WPT_APPEND and WPTS_CLEAR are the only supported.",new_goal.x, new_goal.y);
}

/* Calculate the commands for this robot, using the collision avoidance algorithm
 * specified in [[]] TODO
 */

// NOTE that we could fold this into the MotionPlanner class if we do path-following
// instead of waypoint following in the future.

void Pucc::updateGoalWaypoint()
{
	if (cmdtype == WAYPOINT_FOLLOW){ //control type: waypoints
		// get current state as a waypoint
		Waypoint pos;
		Waypoint target;
		Waypoint goal;
		getGoal(goal); // get the actual goal variable reference
		bool robotPassedWP = false; // Would reaching this waypoint require the robot to backtrack, relative to the next one?

		getState(pos.x,pos.y,pos.psi,pos.speed);
		getTarget(target.x,target.y,target.psi,target.speed);

		// distance to current goal
		double dx = goal.x - pos.x;
		double dy = goal.y - pos.y;

		// Absolute (global) angle of path
		if(goalsVect.size() > 1) {
			double pathAbsAngle = atan2(goal.y - lastWaypoint.y, goal.x - lastWaypoint.x);
			double robotGoalAngle = atan2(dy, dx);

			if(fabs(wrap(pathAbsAngle - robotGoalAngle)) > CHECKPT_ANGLE) {
//				robotPassedWP = true;
//				ROS_INFO("Passed our other waypoint, %f", fabs(wrap(pathAbsAngle - robotGoalAngle)));
			}
		}

		//// We have reached the present goal waypoint - we go to motionPlanner
		// since it is aware of the waypoint tolerances.
		if( motionPlanner->reachedWaypoint(dx, dy) || robotPassedWP) {
			ROS_INFO("We have reached a waypoint at distance %f %f", dx, dy);
			if( goalsVect.size() > 1 ) {
				lastWaypoint.x = goal.x;
				lastWaypoint.y = goal.y;
				lastWaypoint.psi = goal.psi;
				// remove the current goal
				goalsVect.erase(goalsVect.begin());
				getGoal(goal);

				// With the exception of light intensity, and 'idle' status,
				// robot takes on features of its next goal. ['idle' determined by code in this file]
				double oldIntensityValue = 0.0;
				// Make sure we aren't initializing this robot's features for first time
				// Otherwise segfault occurs when accessing features[]
				if(features.size() > 2) {
					ROS_INFO("Features was big enough");
					oldIntensityValue = features[LED_INTENSITY_FEAT];
				}
				else ROS_INFO("Features wasn't big enough!");
				features = goal.feats;
				if(! features.empty()) {// When we first place the robot, features is empty.
					ROS_INFO("Goal feature vector was populated");
					features[LED_INTENSITY_FEAT] = oldIntensityValue;
					features[IDLE_FEAT] = double(idle); // s/b false always at this point of execution [still have more waypoints]
				}
				else ROS_INFO("The goal feature vector is empty!");
			}
			else{ // we have reached our final goal
				// replace the current goal with current location
				ROS_INFO("Final goal reached");
				goal.x   = pos.x;
				goal.y   = pos.y;
				idle = true;
				if(features.empty()) {
					ROS_INFO("Reached our goal, but features is empty there!");
					features.resize(9,0.0);
				}
				features[IDLE_FEAT] = double(idle);

				/*if(searchMode){
					printf("I'm at the spin point!\n");
					turnMode = 1;
					searchMode = false;
				}*/
				//goal.psi = pos.psi;
				//goal.vel = pos.vel;
			}
//			lastVisitedWaypointId=lastWaypoint.id;
		}

		// control.vR and .vL are later checked in client for validity with
		// actual type of Pucc (Gpucc or Upucc).
	} else if (cmdtype == V_PSI_DOT_REF){ //control type: v/psidot
		control.vR = (short)(1000*(2.0*velref + dpsiref*getWheelBase())/2.0);
		control.vL = (short)(1000*(2.0*velref - dpsiref*getWheelBase())/2.0);
		ROS_INFO("VL: %hd VR: %hd", control.vL, control.vR);
	}
}

void Pucc::stop()
{
	control.psi = 0.0;
	control.vel = 0.0;

	control.vL = 0;
	control.vR = 0;

	sendDriveCommands();
}

void Pucc::simulateDynamics(double dt){
	// keep args within Create limits
	control.vR = saturate(control.vR,-500,500);
	control.vL = saturate(control.vL,-500,500);

	//simulate for dt
	double vR = control.vR/1000.0;
	double vL = control.vL/1000.0;
	vel = (vL+vR)/2.0;
	dpsi = (vR-vL)/(getWheelBase());
	vx = vel*cos(psi);
	vy = vel*sin(psi);
	x = x + vx*dt;
	y = y + vy*dt;
	psi = psi + dpsi*dt;
}

void Pucc::setMotionPlanner(MotionPlanner *newMP)
{
	motionPlanner = newMP;
}

void Pucc::setDriveCommands(boost::array<short,2> new_vL_vR)
{
	control.vL = new_vL_vR[0];
	control.vR = new_vL_vR[1];
}


/* Receive position updates, either from vicon for hardware puccs or from spoofed
 * data generated by the puccs themselves
 */
void Pucc::poseGenericCallback(const geometry_msgs::PoseStamped &msg){
/*** Check to ensure the subscribed topic is for a valid vehicle ***/
	std::string vehName = msg.header.frame_id;
	if(!hasValidVehiclePrefix(vehName)) {
		ROS_ERROR_STREAM("Vehicle name prefix for-- "+vehName+" -- is not recognized.  Perhaps you haven't added it yet?");
	}
	double psi = quaternionTo2DAngle(msg.pose.orientation.z, msg.pose.orientation.w);

	if(vehName.compare(getName()) == 0) {// We are calling back the pose of THIS gpucc
//		ROS_DEBUG("Pose Callback on self");
		setPose(msg.pose.position.x,msg.pose.position.y, psi);
	}

	else {
		// update obstacle corresponding to this vehicle.
		// If this obstacle is being added for first time, give it unique id
		// Note that prioritization code used by the potential field relies
		// on obstacle IDS correctly matching their vehicle ID.
		if (vehicleNumber.count(vehName) == 0) {
			int obstIDNum = getVehIDFromName(vehName);
			vehicleNumber[vehName] = obstIDNum;
			motionPlanner->numObstacles++;
			ROS_DEBUG("Number of Obstacles: %d", motionPlanner->numObstacles);
		}

		motionPlanner->obstacleMap[vehName]->name = vehName;
		motionPlanner->obstacleMap[vehName]->id  = vehicleNumber[vehName]; // unique number for this vehicle
		motionPlanner->obstacleMap[vehName]->x   = msg.pose.position.x;
		motionPlanner->obstacleMap[vehName]->y   = msg.pose.position.y;
		motionPlanner->obstacleMap[vehName]->z   = msg.pose.position.z;
		motionPlanner->obstacleMap[vehName]->psi = psi;
		motionPlanner->obstacleMap[vehName]->t   = (double)(msg.header.stamp.sec + msg.header.stamp.nsec/1.0e9);
		motionPlanner->obstacleMap[vehName]->radius = vehicleRadii[getVehicleType(vehName)];
		// NOTE here that velocity of the obstacle has not been updated;
		// that is done in a separate callback.
//		ROS_DEBUG("Vehicle %s with numObstacles %d updating pose for obstacle %s", getName().c_str(), motionPlanner->numObstacles, vehName.c_str());
	}
}

void Pucc::velGenericCallback(const geometry_msgs::TwistStamped &msg){
/*** Check to ensure the subscribed topic is for a valid vehicle ***/
	std::string vehName = msg.header.frame_id;
	if(!hasValidVehiclePrefix(vehName)) {
		ROS_ERROR_STREAM("Vehicle name prefix for-- "+vehName+" -- is not recognized.  Perhaps you haven't added it yet?");
	}

	double tx, ty, tpsi, tvel;
	getState(tx, ty, tpsi, tvel);
	double speed = distance2(0, 0, msg.twist.linear.x, msg.twist.linear.y);
	double velHdg = atan2(msg.twist.linear.y, msg.twist.linear.x);
	double diff = wrap(velHdg-tpsi);

	if(fabs(diff) > PI/2.0)
	    speed *= -1.0;

	if(vehName.compare(getName()) == 0) {// We are calling back the vel of THIS gpucc
		//ROS_INFO("dx: %f dy: %f dth: %f spd: %f", msg.twist.linear.x, msg.twist.linear.y, msg.twist.angular.z, speed);
		setVel(msg.twist.linear.x,msg.twist.linear.y,msg.twist.angular.z, speed);
	}
	else {
		motionPlanner->obstacleMap[vehName]->vel = speed;
		motionPlanner->obstacleMap[vehName]->dx = msg.twist.linear.x;
		motionPlanner->obstacleMap[vehName]->dy = msg.twist.linear.y;
//		ROS_DEBUG("Vehicle %s with numObstacles %d updating vel for obstacle %s", getName().c_str(), numObstacles, vehName.c_str());
	}
}

//receive waypoint updates
void Pucc::wayptCallback(const raven_pucc::waypoint::ConstPtr& msg){
ROS_INFO("wayptCallback called");
//	turnMode = 0;
	followMode = -1;
//	searchMode = false;
	addWaypoint((Waypoint_Clear_T)(msg->clear), msg->x, msg->y, msg->psi, msg->speed, msg->feats); //msg->clear: 0 is append, 1 is clear and add new
}

void Pucc::vpsidotCallback(const raven_pucc::vrotcmd::ConstPtr& msg){
	setVPsidotRef(msg->v, msg->psidot);
}

std::vector<double> Pucc::getFeats() {
	return features;
}

//TODO the below should really be in MotionPlanner - need to provide a way for
// motionPlanner to access these variables

/* Iterates through allTopics (passed from a prior query to ROS) and subscribes
 * this pucc to those obstacles of which it is not yet aware.
 */
void Pucc::updateObstTopics(ros::NodeHandle nhandle, ros::master::V_TopicInfo &allTopics)
{
	std::set<std::string> vehNamesFromTopics;
    for(int i=0; i < allTopics.size(); i++){
        std::string topicName = allTopics[i].name; // /uP04/pose
        std::string namePrefix = topicName.substr(1,2); // takes form /uP etc
        std::string newObstName;
        Topic_T topic_t = checkVehTopic(namePrefix, newObstName, topicName);
		if(topic_t == POSE || topic_t == VEL) { // This topic is a vehicle topic
//			ROS_DEBUG("%s found new vehicle obstacle %s", getName().c_str(), newObstName.c_str());
			vehNamesFromTopics.insert(newObstName);
	        if(newObstName.compare(getName()) != 0) { // The topic-vehicle does not belong to this robot
				if(!motionPlanner->isObstacleSubscribed(newObstName)) { // The topic-vehicle is not yet subscribed
					ROS_DEBUG("Subscribing vehicle %s to obstacle %s",getName().c_str(), newObstName.c_str());
					subscribeObstacle(nhandle,newObstName,checkNameSimulated(newObstName));
				}
			}
        }
    }
    motionPlanner->removeOldObstacles(vehNamesFromTopics);
}

// subscribe a vehicle to this particular obst (essentially perfect state info of other agents)
void Pucc::subscribeObstacle(ros::NodeHandle nhandle, std::string newObstName, bool isSimulated)
{
	if(newObstName.compare(getName()) == 0) {
		ROS_ERROR("Attempted to subscribe vehicle %s as its own obstacle!", getName().c_str());
		return;
	}

	if(motionPlanner->isObstacleSubscribed(newObstName)) {
		ROS_ERROR("Tried to resubscribe to an obstacle already subscribed!");
		return;
	}
    ROS_INFO("New obst detected and added: %s", newObstName.c_str());
	Obstacle *newObst = new Obstacle();
	newObst->name = newObstName;
	newObst->id = getVehIDFromName(newObstName);

//subscribe to its pose topic
	ROS_INFO("Vehicle --%s-- subscribing to obstacle: --%s--",getName().c_str(),newObstName.c_str());
	newObst->posesub = new ros::Subscriber(nhandle.subscribe(newObstName+"/pose", 1, &Pucc::poseGenericCallback, this)); //s/b 200 if we aren't using the callback
	newObst->velsub = new ros::Subscriber(nhandle.subscribe(newObstName+"/vel", 1, &Pucc::velGenericCallback, this));
	motionPlanner->addObstacle(newObst);
}

// TODO this should be on a parameter server or something, shared by all.
void Pucc::initMaps()
{
// Initialize map of vehicle types
	vehicleTypes.insert(std::pair<std::string, std::string>("uP","Upucc"));
	vehicleTypes.insert(std::pair<std::string, std::string>("BQ","Quad"));
	vehicleTypes.insert(std::pair<std::string, std::string>("mQ","Miniquad"));
	vehicleTypes.insert(std::pair<std::string, std::string>("GP","Gpucc"));

	vehicleRadii.insert(std::pair<std::string, double>("Gpucc", GPUCC_RADIUS));
	vehicleRadii.insert(std::pair<std::string, double>("Upucc", UPUCC_RADIUS));
//	vehicleRadii.insert("Miniquad", GPUCC_RADIUS); // TODO update this
//	vehicleRadii.insert("Quad", GPUCC_RADIUS); // TODO update this

}

/* Determines if the topic "topicName" corresponds to that of a vehicle's pose
 * or velocity, where the 2-letter prefix of the name is checked against a
 * list of known / desired vehicles types.
 *
 * Returns POSE or VEL if so and assigns the variable obstName, or returns
 * NONE and makes no assignment.
 */
Topic_T Pucc::checkVehTopic(std::string namePrefix, std::string &obstName, std::string topicName)//inline
{
	std::vector<std::string> tmp;
	boost::split(tmp, topicName, boost::is_any_of("/"), boost::token_compress_on); // of the form: /uP01/pose
	if(tmp.size() < 3) // cannot be a vehicle topic
		return NONE;
	// tmp[0-2] contains /""/<name>/"pose"
    obstName = tmp.at(1);
    const char *cstrTopicName = topicName.c_str();
    if (vehicleTypes.count(namePrefix) != 0){ // This is a valid vehicle
        if(!tmp.at(2).compare("pose")){ // This is a pose topic
//        	ROS_DEBUG("Topic %s IS a vehicle pose topic.",cstrTopicName);
            return POSE;
        } else if(!tmp.at(2).compare("vel")) {
//        	ROS_DEBUG("Topic %s IS a vehicle vel topic.",cstrTopicName);
        	return VEL;
        } else {
//        	ROS_DEBUG("Topic %s is a vehicle topic, non-pose.",cstrTopicName);
        }
    }
    else {
  	ROS_INFO("Topic %s with prefix %s does not correspond to a known vehicle type.",cstrTopicName,namePrefix.c_str());
    }
    return NONE;
}

/* Check vehName against map of allowed vehicle prefixes, return false if invalid.
 */
bool Pucc::hasValidVehiclePrefix(std::string vehName)
{
	std::string namePrefix = vehName.substr(0,2); // first two letters of name -- define robot type
	return vehicleTypes.count(namePrefix) != 0;
}

std::string Pucc::getVehicleType(std::string vehName)
{
	std::string namePrefix = vehName.substr(0,2); // first two letters of name -- define robot type
	return vehicleTypes[vehName];
}


 /* namespace Pucc */

