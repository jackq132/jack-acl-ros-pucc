/*
 * PotFieldPlanner.cpp
 *
 *  Created on: Oct 11, 2012
 *      Author: bobby, modified from old lab code
 *
 *  Potential field implementation of collision avoidance.
 *  Not particularly robust, since potential field assumes all objects are
 *  static, and is not loop-free.
 *  It is very simple in concept though.
 *
 *
 */

#include "PotFieldPlanner.h"


PotFieldPlanner::PotFieldPlanner(ros::NodeHandle &nhandle, std::string newRobotName) : MotionPlanner(newRobotName) {
    // NOTE that we must call getParameters() here since parent is unaware of this subclass, cannot call there.
	getParameters(newRobotName);
	Ihdg.reset();
    Irange.reset();
    velFilt = 0;
    aligned = false;

//    nhandle.advertise<raven_pucc::carrot>(robotName+"/carrots", 1000);
}

PotFieldPlanner::~PotFieldPlanner() {
	if(carrotpub != NULL) carrotpub.shutdown();
}

/* NOTE that robotName should not have an 's' appended if simulated;
 * this is the actual name of the robot, with a parameter file which should
 * have already been loaded onto the parameter server.
 */
void PotFieldPlanner::getParameters(std::string robotName)
{
	MotionPlanner::getParameters(robotName);

	// See launch file (e.g. GP09param.launch) for descriptions of parameters
	MAX_WHEEL_VEL 	= param(Pucc::PARAM_NAMESPACE+robotName+"/MAX_WHEEL_VEL");
	SPEED_LIMIT 	= param(Pucc::PARAM_NAMESPACE+robotName+"/SPEED_LIMIT");
	SECOND_SPEED_LIMIT = param(Pucc::PARAM_NAMESPACE+robotName+"/SECOND_SPEED_LIMIT");
	VEL_MIN 		= param(Pucc::PARAM_NAMESPACE+robotName+"/VEL_MIN");
	MIN_RO 			= param(Pucc::PARAM_NAMESPACE+robotName+"/MIN_RO");
	WP_TOL 			= param(Pucc::PARAM_NAMESPACE+robotName+"/WP_TOL");
	KR 				= param(Pucc::PARAM_NAMESPACE+robotName+"/KR");
	KTH 			= param(Pucc::PARAM_NAMESPACE+robotName+"/KTH");
	TLOOK 			= param(Pucc::PARAM_NAMESPACE+robotName+"/TLOOK");
	WRISK 			= param(Pucc::PARAM_NAMESPACE+robotName+"/WRISK");

	HDG_SPEED_SAT_RATIO = param(Pucc::PARAM_NAMESPACE+robotName+"/HDG_SPEED_SAT_RATIO");
	VEL_ERROR_SAT 		= param(Pucc::PARAM_NAMESPACE+robotName+"/VEL_ERROR_SAT");
	VEL_INTEGRATOR_CONST= param(Pucc::PARAM_NAMESPACE+robotName+"/VEL_INTEGRATOR_CONST");
	HDG_INTEGRATOR_CONST= param(Pucc::PARAM_NAMESPACE+robotName+"/HDG_INTEGRATOR_CONST");
	Kp_hdg 				= param(Pucc::PARAM_NAMESPACE+robotName+"/Kp_hdg");
}

/* Compute the appropriate control action, assign control.vL and control.vR
 * Note these are assigned assuming a Gpucc: values are later scaled by the
 * Upucc (or other vehicles as necessary) to reflect their limitations.
 */
boost::array<short,2> PotFieldPlanner::controller(Waypoint pos, Waypoint goal)
{
    double vfd, vth;
	double th_ref = 0.0;
	double pos_err = 0.0;

	boost::array<short,2> control_vL_vR = {0, 0};

	// Obstruction check
	if(carrot.obstructed)
	{
//		ROS_INFO("Carrot Obstructed");
		control_vL_vR[0] = 0;
		control_vL_vR[1] = 0; // optional statement for clarity
        return control_vL_vR;
		// Implicitly sendDriveCommands() after this method, so this is fine.
    }

    // left handed frame
    //    th_ref = atan2(carrot.x - pos.x, carrot.y - pos.y);
    //right handed frame
    th_ref = atan2(carrot.y - pos.y,carrot.x - pos.x);
    pos_err = distance2(pos.x,pos.y,carrot.x,carrot.y); // distance to goal

    ROS_INFO("Goal: %f, %f Carrot: %f, %f  Pos: %f, %f", goal.x, goal.y, carrot.x, carrot.y, pos.x, pos.y);
    //ROS_INFO("Thref: %f Poserr: %f", th_ref*180/M_PI, pos_err);

    // Reset aligned flag if new waypoint error is large
    // TODO - commented this; is it necessary?
//    if(distance2(goal.x,goal.y,robot->lastWaypoint.x,robot->lastWaypoint.y)>0.05 || fabs(robot->lastWaypoint.psi-goal.psi)>0.05){
        //printf("%s no longer aligned! \n", name.c_str());
        //printf("Goal0x: %0.2f Goal0y: %0.2f Goal0psi: %0.2f \n",goal.x,goal.y,goal.psi);
        //printf("lwx: %0.2f lwy: %0.2f lwpsi: %0.2f \n",lastWaypoint.x,lastWaypoint.y,lastWaypoint.psi);
        //printf("pos err: %f psi err: %f \n",distance2(goal.x,goal.y,lastWaypoint.x,lastWaypoint.y), fabs(lastWaypoint.psi-goal.psi));
//        aligned = false;
//    }

    if(aligned)
    {
//    	ROS_INFO("Aligned, setting control to zero");
    	control_vL_vR[0] = 0;
    	control_vL_vR[1] = 0; // optional statement for clarity
        return control_vL_vR;
    }
	double dx_err = goal.x - pos.x;
	double dy_err = goal.y - pos.y;


	// Are we on the waypoint?  If so, stay still.
	if(carrot.speed > SECOND_SPEED_LIMIT)
	{
		if(carrot.obstructed) {
//			ROS_INFO("Carrot obstructed");
			control_vL_vR[0] = 0;
			control_vL_vR[1] = 0; // optional statement for clarity
		}
		if( sqrt(dx_err*dx_err + dy_err*dy_err) < WP_TOL) {
//			ROS_INFO("Waypoint reached, setting control to 0");
			control_vL_vR[0] = 0;
			control_vL_vR[1] = 0; // optional statement for clarity
			Ihdg.reset();
			Irange.reset();
		    //printf("%s aligned fast! \n",name.c_str());
			return control_vL_vR;
		}
		if( sqrt(dx_err*dx_err + dy_err*dy_err) < 4*WP_TOL ) {
			ROS_INFO("Approaching waypoint, reducing speed");
			carrot.speed = SECOND_SPEED_LIMIT+0.01;
		}
	// TODO - upucc-specific constant 0.02? Eliminate this code?
	}else if( sqrt(dx_err*dx_err + dy_err*dy_err) < 0.02) {
//		ROS_INFO("Moving fast, directly on top of goal");
		control_vL_vR[0] = 0;
		control_vL_vR[1] = 0; // optional statement for clarity
		Irange.reset();
		// Fix Heading mod
		double th_err = wrap(goal.psi - pos.psi);
		if( fabs(th_err) < 0.15 || goal.psi > 50){
			ROS_INFO("setting aligned true; th_err is small");
		    Ihdg.reset();
		    //printf("%s aligned slow! \n",name.c_str());
//		    aligned = true;

		}else{
		    double hdgSat = SECOND_SPEED_LIMIT * HDG_SPEED_SAT_RATIO;
	        th_err = saturate(th_err, -hdgSat,hdgSat);
	        Ihdg.increment(th_err,1.0/20.0);
	        vth = saturate(Kp_hdg*(th_err) + 0.1*Ihdg.value,-MAX_WHEEL_VEL,MAX_WHEEL_VEL);
//	        ROS_INFO("assigning nonzero wheelspeeds, very close to goal: %d",vth);
	        control_vL_vR[0] = (short)(-1000*vth);
	        control_vL_vR[1] = (short)(1000*vth);
		}
		return control_vL_vR;
	}

	// Compute heading error and check for wrap-around
	// left handed frame
	double th_err = wrap(th_ref - pos.psi);

	// Low-pass velocity filter
	velFilt = velFilt + (1.0/20.0 * (pos.speed-velFilt));
	double vel_err = carrot.speed - velFilt;
	//printf("VelFilt: %f \n",velFilt);
	//ROS_INFO("Therr: %f Velerr: %f", th_err*180/M_PI, vel_err);

	// Simple proportional heading controller (inner loop -- faster gain)
	vth = saturate(KTH*(th_err),-MAX_WHEEL_VEL,MAX_WHEEL_VEL);

	// Simple proportional range controller (outer loop -- slower gain)
	double speedLim = saturate(carrot.speed,-SPEED_LIMIT,SPEED_LIMIT);
	vfd = saturate(KR*pos_err,-speedLim,speedLim);

	// Special low-speed controller
	if(carrot.speed <= SECOND_SPEED_LIMIT)
	{
		if(carrot.obstructed) {
			control_vL_vR[0] = 0;
			control_vL_vR[1] = 0;
			return control_vL_vR;
		}
	    double  hdgSat = SECOND_SPEED_LIMIT * HDG_SPEED_SAT_RATIO;
		th_err = saturate(th_err, -hdgSat,hdgSat);
		Ihdg.increment(th_err,HDG_INTEGRATOR_CONST/20.0);
		Irange.increment(vel_err,VEL_INTEGRATOR_CONST/20.0);
		vfd = saturate(1.5 * vel_err + 1.0*Irange.value,0.0,VEL_ERROR_SAT);
		vth = saturate(Kp_hdg*(th_err) + 0.02*0.0*Ihdg.value,-MAX_WHEEL_VEL,MAX_WHEEL_VEL);
		
		ROS_INFO("Low Speed ctrller; vfd = %f, vth = %f; 1.5vel_err %f, Irange %f", vfd, vth, 1.5*vel_err,Irange.value);
	}else{
		if(vel_err > 0)
	        vel_err = saturate(vel_err, VEL_ERROR_SAT,  sqrt(dx_err*dx_err + dy_err*dy_err)/4.0);
	    if(vel_err < 0)
	        vel_err = saturate(vel_err, -sqrt(dx_err*dx_err + dy_err*dy_err)/4.0, -VEL_ERROR_SAT);
	    if(fabs(th_err)>0.4)
		    vel_err = 0.05;
		//vfd = saturate(3.0*vel_err, -carrot.speed*0.5, carrot.speed*0.5);
		vfd = saturate(3.0*vel_err, VEL_ERROR_SAT, carrot.speed*0.5);
		Irange.reset();
		Ihdg.reset();
		// (Terrible way) to ensure the robots dont entirely stop permanently.
		if(vfd < VEL_MIN)
			vfd = VEL_MIN;
		ROS_INFO("Normal Controller, no correction made to theta, vfd = %f",vfd);
	}


	double vL = vfd - vth;
	double vR = vfd + vth;

	ROS_INFO("Final Command vL, vR; vfd, vth %f, %f; %f, %f",vL, vR, vfd, vth);

	control_vL_vR[0] = (short)(1000*vL);
	control_vL_vR[1] = (short)(1000*vR);
	return control_vL_vR;
	//ROS_INFO("VL: %hd VR: %hd", control.vL, control.vR);
}

boost::array<short,2> PotFieldPlanner::plan(Waypoint curPos, Waypoint goal, int vehID)
{

	carrot.obstructed = false;
	carrotGen(curPos, goal, obstacleMap, vehID);
	//printf("pos x: %g, pos y: %g, carrot x: %g, carrot y: %g \n",pos.x,pos.y,carrot.x,carrot.y);
	return controller(curPos, goal);
}

bool PotFieldPlanner::reachedWaypoint(double dx, double dy)
{
	return distance2(0,0,dx,dy) < WP_TOL;
}

/* Note that objects farther away than 'drone_buffer' don't even get penalized */
void PotFieldPlanner::carrotGen(Waypoint pos, Waypoint goal, std::map<std::string, Obstacle *> obstacleMap, int myID)
{
	// Planner constants
	double drone_buffer	= 0.1 + 4.0*GPUCC_RADIUS;	// Add drone radius - only penalized inside this buffer
	double stop_buffer = 0.01 + 2.0*GPUCC_RADIUS; // buffer in which GPUCC stops
	double wall_buffer 	= 0.03 + GPUCC_RADIUS;	// Add static object radius (wall and around vehicles)
	double t_ahead  	= TLOOK;    		// seconds
	double r_ahead      = t_ahead*SPEED_LIMIT;	// m, 0.2 = max_vel
	double w_risk		= WRISK;		// Risk knob (larger == more risk averse)
	double penalty   	= 20.0;			// Penalty at wall boundary region
	int    N_PTS  		= 64;			////BOB Appears to be # of angular points (64 lines directed out)
	int    R_PTS		= 64;			////BOB Appears to be # of radial point (64 circles radius r_ahead out)

	double best    = 1000000000;	// Best cost
	double best_th = 0.0;			// Best theta
	double best_r  = 0.0;			// Best range

	double turnPenalty = 0.01;

	// Carrot starts as unobsructed
	carrot.obstructed = false;


	// For each of n points around the vehicle
	double potential[N_PTS][R_PTS];

	/* 	DEBUG
	for(std::map<std::string, Obstacle *>::iterator it = obstacleMap.begin();it != obstacleMap.end(); ++it) {
		double d = distance2(pos.x, pos.y, (it->second)->x, (it->second)->y);
			ROS_INFO("NAME: %s, gpuccRadius: %g, separationDistance: %g\n",it->first.c_str(),GPUCC_RADIUS,d);
	}
	*/


	/********* Check if a robot needs to stop *********/
	for(int i=0;i<N_PTS;i++) { // Iterate radially over the front half
		for(std::map <std::string, Obstacle *>::iterator it = obstacleMap.begin(); it != obstacleMap.end(); ++it) {
			double robot_relative_hdg = 1.0*PI*(i- N_PTS/2)/(N_PTS*1.0);
			double theta_relative_hdg = wrap(pos.psi + robot_relative_hdg); // relative to current heading
			double x_i = pos.x;
			double y_i = pos.y;
			std::string obstName = it->first;
			Obstacle *currentObst = it->second;
			// Pull out obstacle info...
			double d = distance2(x_i, y_i, currentObst->x, currentObst->y);
			// Check if static or dynamic
			//	ROS_INFO("%d Must yield to %d, distance %f of reqd %f",myID, currentObst->id,d,currentObst->radius + stop_buffer);
			double relAngleToObst = wrap(pos.psi - atan2(currentObst->y - pos.y, currentObst->x - pos.x));
			bool isBehind = relAngleToObst > PI/2.0 || relAngleToObst < -PI/2.0;
			//ROS_INFO("Robot %d is behind %d: %s",myID,currentObst->id, isBehind ? "true" : "false");
			if ( (d < (currentObst->radius + 0.05 + stop_buffer * cos(robot_relative_hdg))) && (currentObst->z < .4) && !(isBehind)) { // need large distance ahead, small on sides
				if(myID > currentObst->id) {// we need to yield
//					ROS_INFO("Carrot for robot %d obstructed by obstacle id %d at angle %f", myID, currentObst->id, robot_relative_hdg);
					carrot.x = pos.x;
					carrot.y = pos.y;
					carrot.psi = goal.psi;
					carrot.speed = 0;//saturate((SLOW_SPEED/estSpeed*2.0),0.0,1.0);
					carrot.obstructed = true;
					return;
				}
			}
		}
	}
	//Iterate around radially with resolution 2pi / N_PTS
	for(int i=0;i<N_PTS;i++) {

		for(int j=0;j<R_PTS;j++) { // Iterate outwards
			carrot.obstructed = false;
			// Point around you

			// start j == 1

			double x_i = pos.x + r_ahead*((double)j/((R_PTS-1)*1.0))*cos(2*PI*i/(N_PTS*1.0));
			double y_i = pos.y + r_ahead*((double)j/((R_PTS-1)*1.0))*sin(2*PI*i/(N_PTS*1.0));

			// for the close in points maybe think about setting an angle if you aren't going to move
			//double x_i = pos.x + r_ahead*((double)(j+1)/((R_PTS)*1.0))*cos(2*PI*i/(N_PTS*1.0));
			//double y_i = pos.y + r_ahead*((double)(j+1)/((R_PTS)*1.0))*sin(2*PI*i/(N_PTS*1.0));

			//		printf("i=%d, j=%d, x_i=%.3f, y_i=%.3f, goalx=%.3f, goaly=%.3f",i,j,x_i,y_i,goal.x,goal.y);
			// Waypoint potential
			// Initial potential field linearly increases around goal, giant bowl shape around goal.
			// Potential at each point i,j is initialized as such.
			potential[i][j] = distance2(x_i,y_i,goal.x,goal.y);

			// TODO - Check these measurements for the walls
			double xmin = XMIN;
			double xmax = XMAX;
			double ymin = YMIN;
			double ymax = YMAX;

			// Are we close to any walls? If so, penalize...
			// If we start inside the box - we'll never get out!
			// If we start outside the box - we'll never get in!
			double d = fabs(x_i-xmin);
			if(d < wall_buffer) {
				potential[i][j] += penalty;
			}
			d = fabs(x_i-xmax);
			if(d < wall_buffer) {
				potential[i][j] += penalty;
			}
			d = fabs(y_i-ymin);
			if(d < wall_buffer) {
				potential[i][j] += penalty;
			}
			d = fabs(y_i-ymax);
			if(d < wall_buffer) {
				potential[i][j] += penalty;
			}

			// For each obstacle
			for(std::map <std::string, Obstacle *>::iterator it = obstacleMap.begin(); it != obstacleMap.end(); ++it) {
				std::string obstName = it->first;
				Obstacle *currentObst = it->second;
				// Pull out obstacle info...
				double d = distance2(x_i, y_i, currentObst->x, currentObst->y);


				// Check if static or dynamic
				if (currentObst->id < 0) { ////BOB Object is static if id < 0
					// If we're in the static obstacle's halo, penalize...
					if ( d < currentObst->radius + wall_buffer ) { // so statement below always positive, higher penalty for smaller d, static obj
						{
								potential[i][j] += penalty*(wall_buffer + currentObst->radius - d);
						}
					}
				} else {

                    // If there is a dynamic obstacle sitting directly at our goal point,
                    // mark carrot as obstructed
                    double d_obs = distance2(goal.x, goal.y, currentObst->x, currentObst->y);
                    if(d_obs < GPUCC_RADIUS && d < WP_TOL){ // if(distance btwn goal and obst < GPUCC_RADIUS && dist btwn this sample point and obstacle < WP_TOL)
// Note that the above code assumes that all vehicles are GPUCC's - better to use
// a more generic distance term.  Better than a magic number though.
                      //  carrot.obstructed = true; //turning off carrot obstruction
                    }

					// If we're in the dynamic obstacle's halo, penalize...
					if ( (d < (currentObst->radius + drone_buffer)) && (currentObst->z < .4) ) { //if obstacle is near ground
						potential[i][j] += w_risk*penalty/2 * pow(drone_buffer + currentObst->radius - d,2);
					}

					//use this to allow high flying obstacles at their static position
					//if (( d < obstacles->list.at(jj).radius + drone_buffer ) && (obstacles->list.at(jj).z < .4) )){
					//        potential[i][j] += w_risk*penalty*(drone_buffer + obstacles->list.at(jj).radius - d);
					//}


					// Moving? ////BOB Reason for 1/2 in expr below; penalize again if still in halo on dyn obj's next timestep.
					double x = currentObst->x + t_ahead*currentObst->dx;
					double y = currentObst->y + t_ahead*currentObst->dy;
					d = distance2(x_i, y_i, x, y);

					// If we're in the obstacle's halo and its altitude is low, penalize...
					if( (d < currentObst->radius + drone_buffer) && (currentObst->z < .4) ) {
						potential[i][j] += w_risk*penalty/2 * pow(drone_buffer + currentObst->radius - d,2);
					}
				}
			}

			double theta = 2*PI*i/(N_PTS*1.0);
			potential[i][j] += abs(theta - pos.psi) * turnPenalty;
//			ROS_INFO("turn potential %f, added %f",potential [i][j], abs(theta - pos.psi) * turnPenalty);

			if(potential[i][j] <= best) {
				//ROS_INFO("Better Potential = %.3f at %d, %d\n",potential[i][j], i, j);
				best    = potential[i][j];
				best_th = theta;
				best_r  = r_ahead*((double)j/((R_PTS-1)*1.0));
			}
		}
	}

	//// BOB TODO - maybe apply a slight penalty here for turning, to favor a slowdown instead -
	//// also use cross-product of relative obstacle velocities to determine if a slowdown should occur
	//// (could also use to enforce a certain turning direction but that's already taken care of by
	//// potential field)


	//        best_r = saturate(best_r, -(SLOW_SPEED*dLen*1.0/20.0-dist), SLOW_SPEED*dLen*1.0/20.0-dist); // Slow-mo mod for DCAP

	// Find the point with minimum potential
	carrot.x = pos.x + best_r*cos(best_th);
	carrot.y = pos.y + best_r*sin(best_th);
	carrot.psi = goal.psi;
	carrot.speed = goal.speed * (best_r / r_ahead);//saturate((SLOW_SPEED/estSpeed*2.0),0.0,1.0);
	//carrot.vel = goal.vel
	ROS_INFO("Carrot x = %.3f, Carrot y = %.3f, best_th = %.3f, best_r = %.3f\n", carrot.x , carrot.y, best_th,best_r);
}
