#include <RAVEN_utils.hpp>
#include "ros/ros.h" // ONLY for the param() function

// This is a static function

double param(std::string n)
{
	double out = 0.0;
	if(ros::param::get(n, out))
		ROS_INFO("Got param: %s - %f",n.c_str(),out);
	else {
		ROS_FATAL("Failed to get param %s",n.c_str());
		exit(1);
	}
	return out;
}

//## Enforces +pi <--> -pi
double wrap(double ang)
{
	while( ang > PI )
		ang -= 2*PI;
	while( ang <= -PI )
		ang += 2*PI;

	return ang;
}

//## Enforces 0 <--> 2*pi
double unwrap(double ang)
{
	while( ang >= 2*PI )
		ang -= 2*PI;
	while( ang < 0 )
		ang += 2*PI;

	return ang;
}

//## saturates val at vmin and vmax
double saturate(double val, double vmin, double vmax)
{
	if( val > vmax )
		val = vmax;
	else if( val < vmin )
		val = vmin;

	return val;
}

//## saturates val at vmin and vmax
short saturate(short val, short vmin, short vmax)
{
	if( val > vmax )
		val = vmax;
	else if( val < vmin )
		val = vmin;

	return val;
}

//## saturates val at vmin and vmax
unsigned char saturate(unsigned char val, unsigned char vmin, unsigned char vmax)
{
	if( val > vmax )
		val = vmax;
	else if( val < vmin )
		val = vmin;

	return val;
}


double distance2(double x1, double y1, double x2, double y2)
{
	//pt1 = [x,y]
	return sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
}

int min(double* in, int sz)
{
	int index = 0;
	double min_value = in[0];
	for( int i=0; i<sz; i++ ) {
		if(in[i]<min_value) {
			index = i;
			min_value = in[i];
		}
	}
	return index;
}

// takes 3-D quaternion values z,w and returns a 2-D
// angle rotation on 0-2pi
double quaternionTo2DAngle(double z, double w)
{
	double psi = 2.0*acos(w);
	double ssgn = z;
	if (ssgn < 0){
		psi *= -1.0;
	}
	//put psi in [0, 2pi]
	psi = unwrap(psi);
	return psi;
}

/* Checks if the vehicle name vehName meets the standard name convention for
 * simulated vehicles, the last letter being 's' [usu. appended to a
 * number eg GP04s]
 */
// This is a static function
bool checkNameSimulated(std::string vehName) //inline
{
    return vehName[vehName.length()-1] == 's';
}

/* From:
 * ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html#The%20C%20Code
 * Basically, draw a horizontal line from well outside the polygon, count the
 * number of lines you cross; if odd, then you're inside the polygon.
 */
bool isInsidePolygon(int nvert, double *vertx, double *verty, double testx, double testy)
{
  int i, j;
  bool c = false;
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>testy) != (verty[j]>testy)) &&
	 (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       c = !c;
  }
  return c;
}

/* Returns the vehicle number (ID) associated with this vehicle name in the standard
 * format, with two letters preceding two digits (eg GP04s, uP02, etc)
 */
int getVehIDFromName(std::string vehName)
{
	return atoi(vehName.substr(2,2).c_str());
}
