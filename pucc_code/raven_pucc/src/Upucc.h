/*
 * Upucc.h
 *
 *  Created on: Oct 11, 2012
 *      Author: bobby
 */

#ifndef UPUCC_H_
#define UPUCC_H_

namespace Upucc {

class Upucc {
public:
	Upucc();
	virtual ~Upucc();
};

} /* namespace Upucc */
#endif /* UPUCC_H_ */
