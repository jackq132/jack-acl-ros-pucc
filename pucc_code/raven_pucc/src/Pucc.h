/**
* pucc.h
* Author: Bobby, modified from upucc.hpp
* Oct. 11, 2012

* This pucc superclass simplifies code shared between upuccs and gpuccs.
*
* NOTE That at present, ID contains the two digits following the pucc identifier,
* eg ID = 5 for GP05.  Thus, problems could occur if uP05 is used and given the same ID
* where code relies on unique IDs for each pucc.
* If writing clients that require unique IDs, use the 'uniqueID' field instead, which
* is unused at present.
* For example, give upuccs uniqueID = ID, and gpuccs uniqueID = 1000 + ID
* eg GP05.ID = 5, GP05.uniqueID = 1005
*
* Pucc navigation is waypoint-based, with collision avoidance executed
* according to --PotentialField, --ORCA (at present).
*
* See main.cpp in this pucc package for further information.
*/

#ifndef PUCC_H_
#define PUCC_H_

#include "ros/ros.h" // for callbacks, etc
#include <RAVEN_defines.hpp>
#include <RAVEN_classes.hpp> // For "Waypoint" etc
#include <RAVEN_utils.hpp> // For 'wrap'
#include "MotionPlanner.h"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Point.h"
#include "raven_pucc/waypoint.h"
#include "raven_pucc/vrotcmd.h"

enum {UNIQUE_ID_SPACER = 1000, MAX_NUM_WAYPOINTS = 50}; // Space unique

enum Command_T {WAYPOINT_FOLLOW, V_PSI_DOT_REF};
enum MotionPlan_T {POTENTIAL_FIELD, ORCA};

enum Topic_T {POSE, VEL, NONE}; // Topic Type when querying master for topics.

typedef std::map <std::string, std::string> StrMap;

class Pucc {
public:
	static StrMap vehicleTypes;
	static std::map <std::string, double> vehicleRadii;
	static std::string PARAM_NAMESPACE; // As specified in launch file which loads params onto server
	    										// Currently we directly specify name

	//map from vehicle names to their obstacle objects
	MotionPlanner *motionPlanner;
	std::map <std::string, int> vehicleNumber; // Maps a vehicle name to its associated number

	// functions
	Pucc();
	virtual ~Pucc();
	void setName(std::string);
	std::string getName();


	static void initMaps();
	static Topic_T checkVehTopic(std::string namePrefix, std::string &obstName, std::string topicName);
	static std::string getVehicleType(std::string vehName);
	static bool hasValidVehiclePrefix(std::string vehName);


	void poseGenericCallback(const geometry_msgs::PoseStamped &msg);
	void velGenericCallback(const geometry_msgs::TwistStamped &msg);
	void wayptCallback(const raven_pucc::waypoint::ConstPtr& msg);
	void vpsidotCallback(const raven_pucc::vrotcmd::ConstPtr& msg);

	void setID(int id);
	int  getID();

	void setPose(double x, double y, double psi);
	void setVel(double vx, double vy, double dpsi, double speed);

	void getState(double &x, double &y, double &psi, double &speed);
	void getState(Waypoint &curState);
	void getVel(double &tvx, double &tvy, double &tdpsi);
	void setTarget(double tx, double ty, double tpsi, double tvel);
	void getTarget(double &tx, double &ty, double &tpsi, double &tvel);
	Command_T getCommandType();
	bool isSimulated();
	void getGoal(Waypoint &goal);
	std::vector<double> getFeats();

	void setVPsidotRef(double v, double psidot);
	void addWaypoint(Waypoint_Clear_T wptAddingMode, double x, double y, double psi, double speed, std::vector<double> feats);

	void updateGoalWaypoint();

	/* Scale motor commands to the vehicle being used; they are computed for
	 * gpuccs by default. */
	virtual void scaleDriveCommands() = 0;
	virtual void sendDriveCommands() = 0;
	virtual void simulateDynamics(double dt) = 0;
	virtual int getWheelBase() = 0;
	virtual std::string getPuccType() = 0;
	virtual bool setupHardware(int argc, char *argv[], int argcnt, std::string puccname, std::string usbPortLoc) = 0;
	virtual void puccMain(ros::NodeHandle &nhandle)=0;

	void stop();

	void setSim(bool simValue);
	void setMotionPlanner(MotionPlanner *newMP);
	void setDriveCommands(boost::array<short,2> new_vL_vR);

	void updateObstTopics(ros::NodeHandle nhandle, ros::master::V_TopicInfo &allTopics);
	void subscribeObstacle(ros::NodeHandle nhandle, std::string newObstName, bool isSimulated);


protected:
	std::string name;		// Pucc name (uP01, uP02, ...)
	int ID;           			// ID Number for this PUCC (NOTE: upuccs and gpuccs
						// might have the same ID number, eg GP05.ID = 5 = uP05.ID
	int uniqueID;				// Internally represented
	double x, y, psi;       	// x,y location of the pucc in RAVEN (m) and its heading, psi (rad)
//	double carrotx, carroty;	// x,y location of the carrot
	double vx, vy, dpsi;   		// inertial velocities & turn rate (m/s, rad/s)
	double vel;    				// velocity (+forward, -backwards) (m/s)
	double velref, dpsiref; 	// desired v and psidot (only used if cmdtype = 0, following waypoints)
//	int vfd_past; // moved to upucc only
	int followMode;        		// < 0 = default waypoint following, > 9 = follow the vehicle with this vicon ID
	bool isSimul;				// Is this pucc simulated or real?
//	int lastVisitedWaypointId;	// ID of the previously visited waypoint, ie, the previous goal

	Waypoint target;			// Current targetted waypoint; for the present implementation,
						// this should always be equal to goalsVect.at(0), or the current position.
	Waypoint lastWaypoint;		//
	int lastReceivedWaypointId; // ID of the last received waypoint; used to determine the
							// next id to assign to a newly added waypoint


	bool idle;					// Is the robot idle, ie, does it need to be given a task

	Command_T cmdtype;
	std::vector<double> features;
	Controls control; 			// parameters of controller
	std::vector<Waypoint> goalsVect;       // queue of goal waypoints, index 0 (front) has current target.

};

#endif /* PUCC_H_ */
