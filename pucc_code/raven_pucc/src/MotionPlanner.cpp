/*
 * MotionPlanner.cpp
 *
 *  Created on: Oct 30, 2012
 *      Author: bobby
 */

#include "MotionPlanner.h"
#include <boost/algorithm/string.hpp>

MotionPlanner::MotionPlanner(std::string newRobotName)
{
	robotName = newRobotName;
	numObstacles = 0;
}

MotionPlanner::~MotionPlanner()
{

}

/* NOTE that robotName should not have an 's' appended if simulated;
 * this is the actual name of the robot, with a parameter file which should
 * have already been loaded onto the parameter server.
 */
void MotionPlanner::getParameters(std::string robotName)
{
	// Could specify domain limits for each robot separately by loading a yaml
	// file into parameter server in launch file, like with boundaries.yml.
	// Instead, we opt to use a single common domain.
//	XMIN = param(robotName + "/XMIN");
//	XMAX = param(robotName + "/XMAX");
//	YMIN = param(robotName + "/YMIN");
//	YMAX = param(robotName + "/YMAX");

	// Use physical boundaries in RAVEN as limits, loaded by raven_param.launch
	XMIN = param(std::string("raven") + "/XMIN");
	XMAX = param(std::string("raven") + "/XMAX");
	YMIN = param(std::string("raven") + "/YMIN");
	YMAX = param(std::string("raven") + "/YMAX");

//	XMIN = param("/ctf/boundaries/xmin_outer");
//	XMAX = param("/ctf/boundaries/xmax_outer");
//	YMIN = param("/ctf/boundaries/ymin_outer");
//	YMAX = param("/ctf/boundaries/ymax_outer");
}

Waypoint MotionPlanner::getRandomLocation()
{
	double xfrac = (double)(rand()%100)/100.0, yfrac = (double)(rand()%100)/100.0;
	Waypoint newpos;
	newpos.x = XMIN + (XMAX-XMIN)*xfrac;
	newpos.y = YMIN + (YMAX-YMIN)*yfrac;
	newpos.psi = newpos.speed = 0;
	return newpos;
}

bool MotionPlanner::isObstacleSubscribed(std::string obstacleName)
{
	return obstacleMap.count(obstacleName) != 0;
}

/** Check for Obstacles which correspond to topics no longer being published;
 *  Remove those Obstacles from the obstacleMap and destruct them.
 *  Note we place this here and note in main() since there is no interaction with subscribers,
 *  everything is internal to the pucc.
 */
void MotionPlanner::removeOldObstacles(std::set<std::string> vehNamesFromTopics)
{
// iterate through the subscribers and get the name of each of the vehicles to which we are already subscribing
	std::map <std::string, Obstacle *>::iterator it = obstacleMap.begin();
	while(it != obstacleMap.end()) {
		std::string subscribedObstName = it->first; // vehicle name
        bool wasRemoved = (std::find(vehNamesFromTopics.begin(),vehNamesFromTopics.end(), subscribedObstName) == vehNamesFromTopics.end()); // do we hit the end of the vehNamesFromTopics list without finding our obstacle
        // if that vehicle is no longer present in the topics being broadcast, we should delete its associated marker
        if(wasRemoved)
        {
        	Obstacle *obstToDelete = (it->second);
            obstacleMap.erase(it++); // erase the obstacle, which calls its destructor automatically.
            if(obstToDelete != NULL) delete(obstToDelete);
            ROS_INFO("Obstacle --%s-- was removed from consideration by vehicle --%s--",subscribedObstName.c_str(), robotName.c_str());
        }
        else ++it;
    }
}

void MotionPlanner::addObstacle(Obstacle *newObst)
{
	obstacleMap[newObst->name] = newObst;
}
