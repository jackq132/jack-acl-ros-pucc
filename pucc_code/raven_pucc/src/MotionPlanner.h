/*
 * MotionPlanner.h
 *
 *  Created on: Oct 25, 2012
 *      Author: bobby
 */

#ifndef MOTIONPLANNER_H_
#define MOTIONPLANNER_H_

#include "RAVEN_utils.hpp" // Necessary for param() and checkVehPoseTopic
#include "RAVEN_classes.hpp"
#include "ros/ros.h" // just for the VTopicInfo data structure; if want to remove this include, rewrite that datastructure

class Pucc;

class MotionPlanner {
public:

	/******* ENVIRONMENTAL CONSTANTS ********/
	double XMIN, XMAX, YMIN, YMAX;
	std::string robotName;
	Pucc *myRobot;
	int numObstacles; // number of obstacles this vehicle is aware of
	std::map <std::string, Obstacle *> obstacleMap;



	MotionPlanner(std::string newRobotName);
	virtual ~MotionPlanner();
	virtual boost::array<short,2> plan(Waypoint curState, Waypoint goal, int vehID) = 0;
	virtual bool reachedWaypoint(double dx, double dy) = 0;
	virtual void getParameters(std::string robotName);
	Waypoint getRandomLocation();
	bool isObstacleSubscribed(std::string obstacleName);
	void addObstacle(Obstacle *newObst);
	void removeOldObstacles(std::set<std::string> vehNamesFromTopics);

};



#endif /* MOTIONPLANNER_H_ */
