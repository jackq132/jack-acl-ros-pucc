#include <sys/time.h>
#include <semaphore.h>
#include <signal.h>
#include <iostream>
#include <vector>
#include <string>
#include "ros/ros.h"
#include "RAVEN_defines.hpp"
#include "RAVEN_classes.hpp"
#include "RAVEN_utils.hpp"
#include "MotionPlanner.h"
#include "PotFieldPlanner.h"
#include "Pucc.h"
#include "Gpucc/Gpucc.h"
#include "raven_pucc/waypoint.h"
#include "raven_pucc/carrot.h"
#include "raven_pucc/vrotcmd.h"
#include "featuremsgs/posefeature.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include <cmath> // for trig operations etc
#include <boost/algorithm/string.hpp>

/**
 * All topics below have <name>/ prepended, eg GP05/pose or GP05/features
 *
 * EACH PUCC PUBLISHES ON:
 *
 * /features: 	the current features exhibited by the robot, which with some
 * 				exceptions, are identical to the features associated with the
 * 				goal the robot is moving towards.
 * 				See RAVEN_defines.hpp for what each feature index contains
 * 				(enumerated	type with _FEAT appended, eg LED_DURATION_FEAT = 0 )
 *
 * Simulated puccs also publish on:
 * /pose:		Current pose of the robot, see pose.msg for all fields.
 * 				Essentially 6DOF position/angular state of robot + quaternions
 *
 * /vel:		Same as /pose but with a time derivative.
 */


/**
 * EACH PUCC SUBSCRIBES TO:
 *
 * /waypts:		Each pucc listens to its /waypts topic, to which other nodes
 * 				may publish waypoints at any time; waypoints are queued by
 * 				the pucc, unless the CLEAR_FLAG is set in the message,
 * 				in which case the current queue is cleared before adding the
 * 				waypoint.
 *
 *
 */


Pucc *robot;
enum {MAX_NAME_LENGTH = 100};
enum {LOOP_RATE = 200, TOPIC_UPDATE_RATE = 10};
// TODO is there a way to typdef above in a single place so that both pucc.hpp and main.cpp are aware?

// Main block
int main( int argc, char *argv[] )
{
	// ******* Using rosrun: ******** //
	//argv[0] = rosrun
	//argv[1] = pucc
	//argv[2] = gpucc_controller
	//argv[3] = -s for simulation, -h for hardware
	//argv[4] = name
	//argv[5] = <collision avoidance type>  eg --potential or --orca
	//argv[6] = <USB port location>			eg dev/ttyUSB1

	// OR

	// ******* Using roslaunch ******* //
    //argv[0] = /.../gpucc_controller
	//argv[1] = {GP, UP}					Pucc Type Prefix
	//argv[2] = XX <number>					2-digit ID number eg 01, 02 ...
	//argv[3] = {-s, -h}					Sim/Hardware
	//argv[4] = {--PotentialField, --ORCA}	Collision Avoidance Algorithm
	//argv[5] = <USB port location>			eg dev/ttyUSB1
	//argv[6] = __name:=...  [only for launchfile]
	//argv[7] = __log:=...   [only for launchfile]

	//Debugging code to see what argc and argv are
	ROS_INFO("argc is %d", argc);
	for(int i = 0; i < argc; i++){
		ROS_INFO("argv %d is %s", i, argv[i]);
	}

	std::string puccname;
	std::string usbPortLoc = "NONE_GIVEN";
	MotionPlan_T motionPlan;
	//boost::function<void (void)> plan;

	// TODO: I didn't know about factories at this point.
	// There should be a Pucc factory here

	int argcnt = 0;

	if (argc < 3){
		ROS_FATAL("Not enough arguments to instantiate pucc.");
	} else{
		bool validPucc = false;
		while(argcnt < argc){
			if (strncmp(argv[argcnt], "GP", 2) == 0){
				robot = new Gpucc();
				validPucc = true;
				break;
			}
			else if (strncmp(argv[argcnt], "UP", 2) == 0){
//				robot = new Upucc();
//				validPucc = true;
//				break;
			}
			argcnt++;
		}
		if(validPucc) { // specified either 'Gpucc' or 'Upucc' above
			if (argcnt+2 < argc){ // At least enough arguments to specify name and isSimulated
				std::string puccPrefix = argv[argcnt];
				std::string puccID = argv[argcnt+1];
				puccname = puccPrefix + puccID;
				// This is a simulated pucc
				if(strncmp(argv[argcnt+2],"-s", 2) == 0) {
					if(!checkNameSimulated(puccname)) {
						ROS_INFO("You specified a simulated pucc, but did not"
								"append an 's' to its name, e.g. GP02s");
						puccname.append("s");
					}
					ROS_INFO("Setting pucc %s to simulated.", puccname.c_str());
					robot->setSim(true);
				}
				else if(strncmp(argv[argcnt+2],"-h", 2) == 0) {
					if(checkNameSimulated(puccname)) {
						ROS_FATAL("You specified a hardware pucc, but appended"
								"an 's' to its name, e.g. GP02s");
						return EXIT_FAILURE;
					}
					else {
						ROS_INFO("Setting pucc %s to hardware mode",puccname.c_str());
						robot->setSim(false);
					}

					if(argcnt+4 <= argc){
						usbPortLoc = argv[argcnt+4];
					}
					else {
						ROS_ERROR("Specified hardware gpucc, but not enough arguments to specify usb port location.");
					}
				}
				else {
					ROS_FATAL("Following the pucc type selection (\"GP\" or \"UP\"),"
							"and ID number (e.g. \"01\" or \"02\") you must "
							"instantiate either a hardware pucc with \"-h\" "
							"or a simulated one with \"-s\".");
					return EXIT_FAILURE;
				}
			} // end if(argcnt+2 < argc) [At least enough arguments to specify name and isSimulated]

			if(argcnt+3 < argc) {// Specified motion planner type
				const char *motionPlannerType = argv[argcnt+3];
				if(strncmp(motionPlannerType,"--PotentialField",16) == 0) {
					// Take the planner method of choice, bind it with the unchanging input
					//(a reference to this robot) and assign it to our function pointer.
					//plan = boost::bind(&PotFieldPlanner::plan,*robot);
					//plan = &PotFieldPlanner::plan;
					motionPlan = POTENTIAL_FIELD;
				}
				else if(strncmp(motionPlannerType,"--ORCA",6) == 0) {
					// Take the planner method of choice, bind it with the unchanging input
					//(a reference to this robot) and assign it to our function pointer.
					//plan = boost::bind(&ORCAPlanner::plan, *robot);
					//plan = &ORCAPlanner::plan;
					motionPlan = ORCA;
				}
				else {
					ROS_FATAL("You did not select a valid Motion Planner Type.  Enter"
							"\"--PotentialField\" or \"--ORCA\".");
					return EXIT_FAILURE;
				}
			}
			else {
				ROS_FATAL("Not enough arguments: you did not specify a motion planner type."
						"Enter \"PotentialField\" or \"ORCA\".");
			}
		} // end if(validPucc)
		else {
			ROS_FATAL("You must specify a known vehicle prefix, either \"GP\" or \"UP\" when instantiating a pucc.");
			return EXIT_FAILURE;
		}
	}
	// TODO Unclear if there actually is any software limitation anywhere on name length
    if (puccname.length() > MAX_NAME_LENGTH){
		ROS_FATAL("Gpucc names cannot be greater than 100 characters.");
		return EXIT_FAILURE;
	}

    // At this point, robot has been initialize as sim or hardware, hardware / comms
    // have been setup as-required.
    Pucc::initMaps();
	robot->setName(puccname);
	robot->setID(getVehIDFromName(puccname));
	ROS_INFO("Pucc %s has ID %d", robot->getName().c_str(), robot->getID());

	//initialize ROS and subscribe/advertise topics
	std::string nodename = robot->getPuccType();
	nodename.append("_controller_");
	nodename.append(puccname);
	ros::init(argc, argv, nodename);
	ros::NodeHandle nhandle;
	ros::Subscriber posesub, velsub; // used for actual hardware
	ros::Publisher featpub;
    ros::Publisher poseSpooferPub, velSpooferPub; // used during simulation

	// Note that we wait until this stage to actually set the motion planner, since
	// some implementations (such as PotFieldPlanner) require the use of a nodeHandle
	switch (motionPlan) {
	case POTENTIAL_FIELD:
		robot->setMotionPlanner(new PotFieldPlanner(nhandle, puccname));
		break;
	case ORCA:
// TODO		robot->setMotionPlanner(new ORCAPlanner(robot));
		break;
	default:
		ROS_FATAL("Invalid selection for Motion Planner, not caught in argument parsing");
		return EXIT_FAILURE;
	}

	if (!robot->isSimulated()) {
		posesub = nhandle.subscribe(puccname+"/pose", 1000, &Pucc::poseGenericCallback, robot);
		velsub = nhandle.subscribe(puccname+"/vel", 1000, &Pucc::velGenericCallback, robot); // ,robot
	}
    else { // We need to advertise our own pose for visualization, spoofing vicon output
	    poseSpooferPub = nhandle.advertise<geometry_msgs::PoseStamped>(puccname+"/pose", 1000);
	    velSpooferPub = nhandle.advertise<geometry_msgs::TwistStamped>(puccname+"/vel", 1000);
    }

	if(! robot->setupHardware(argc, argv, argcnt, puccname, usbPortLoc)) {
		ROS_FATAL("Failed to initialize robot communication.");
		return EXIT_FAILURE;
	}

	featpub = nhandle.advertise<featuremsgs::posefeature>(puccname+"/features", 1000);

	ros::Subscriber vpsidotsub = nhandle.subscribe(puccname+"/vpsidot", 1000, &Pucc::vpsidotCallback, robot); //, robot
	ros::Subscriber wptsub = nhandle.subscribe(puccname+"/waypts", 1000, &Pucc::wayptCallback, robot); // ,robot

	//## Welcome msg
	ROS_INFO("Starting %s pucc_controller for %s...", (robot->isSimulated() ? "simulation" : "hardware") , puccname.c_str() );

	if(robot->isSimulated()){ // The pucc needs a position
		Waypoint randomPos = robot->motionPlanner->getRandomLocation();
		robot->setPose(randomPos.x, randomPos.y, randomPos.psi);
		robot->setVel(0,0,0,0);
	}



	//## Give an initial goal at current location
	double robvx, robvy, robdpsi;
	Waypoint curPos;
	robot->getState(curPos);
	Waypoint goal;
	ros::spinOnce(); // Get first waypoint (current robot position)
	ROS_INFO("Robotx: %2.2f, Roboty: %2.2f", curPos.x, curPos.y);

	std::vector<double> tmpFeats;
	tmpFeats.resize(9, 0.0); // fill with all zero features
	tmpFeats[IDLE_FEAT] = double(true); // Initially, pucc is idle.
	//TODO DOES CHANGING THE FINAL DOUBLE MAGIC NUMBER DO ANYTHING? - setting the velocity of the goal.
	robot->addWaypoint(WPTS_APPEND, curPos.x, curPos.y, curPos.psi, 0.0 , tmpFeats); // Magic Number 0.0 is the desired velocity at goal, arbitrary here since goal is current robot position.


	//**************************************************
	//## MAIN LOOP
	//**************************************************
	ROS_INFO("Starting main control loop for %s...", puccname.c_str());
	ros::Time curTime, prevTime;
	prevTime = ros::Time::now();
	ros::Rate loop_timer(LOOP_RATE);
	const unsigned int TOPIC_UPDATE_PERIOD = LOOP_RATE / TOPIC_UPDATE_RATE;
	unsigned int loopCount = 0;

	ros::master::V_TopicInfo allTopics;



	// TODO limit the number of times we query the master per loop


	while( ros::ok() ) {
		// Update obstacles less frequently
		if (loopCount % TOPIC_UPDATE_PERIOD == 0) {
			ros::master::getTopics(allTopics);
			robot->updateObstTopics(nhandle,allTopics);
		}

		ros::spinOnce(); //do callbacks
		//run the controller and send commands or simulate
		robot->updateGoalWaypoint();
		robot->getGoal(goal);
		if(robot->getCommandType() == WAYPOINT_FOLLOW) {
			robot->setDriveCommands(robot->motionPlanner->plan(curPos, goal, robot->getID()));
		}
		else {
			ROS_INFO("%s did not execute new plan, not on waypoint following.",robot->getName().c_str());
		}
		robot->scaleDriveCommands(); // Scale drive commands returned by the motion
								// planner, which isn't aware what kind of robot this is.
		if (!robot->isSimulated()){
			robot->sendDriveCommands();
		} else {
			//simulate
			curTime = ros::Time::now();
			double dt = curTime.sec + curTime.nsec/1.0e9 - prevTime.sec - prevTime.nsec/1.0e9;
			prevTime = curTime;
			robot->simulateDynamics(dt);
		}

		robot->getState(curPos);
		robot->getVel(robvx, robvy, robdpsi);

        if(robot->isSimulated()) {
			//publish the spoofed pose for this simulated bot.
			geometry_msgs::PoseStamped posemsgSpoof;
			posemsgSpoof.header.stamp = ros::Time::now();
			posemsgSpoof.header.frame_id = puccname;
			posemsgSpoof.pose.position.x = curPos.x;
			posemsgSpoof.pose.position.y = curPos.y;
			posemsgSpoof.pose.position.z = 0;
			posemsgSpoof.pose.orientation.x = 0;
			posemsgSpoof.pose.orientation.y = 0;
			posemsgSpoof.pose.orientation.z = sin(curPos.psi/2.0); // Conversions to quaternion representation
			posemsgSpoof.pose.orientation.w = cos(curPos.psi/2.0); // see http://wiki.alioth.net/index.php/Quaternion

			poseSpooferPub.publish(posemsgSpoof);

			geometry_msgs::TwistStamped velmsgSpoof;
			velmsgSpoof.header.stamp = ros::Time::now();
			velmsgSpoof.header.frame_id = puccname;
			velmsgSpoof.twist.linear.x = robvx;
			velmsgSpoof.twist.linear.y = robvy;
			velmsgSpoof.twist.linear.z = 0;
			velmsgSpoof.twist.angular.x = 0;
			velmsgSpoof.twist.angular.y = 0;
			velmsgSpoof.twist.angular.z = sin(robdpsi/2.0); // Conversions to quaternion representation

			velSpooferPub.publish(velmsgSpoof);
        }

		//publish the features associated with this pucc
		featuremsgs::posefeature featmsg;
		featmsg.feats = robot->getFeats();
		featmsg.header.stamp = ros::Time::now();
		featmsg.mutable_id = robot->getID(); // TODO mutable_id is now a misnomer, maybe just ID
//		featmsg.obsname = puccname; // TODO unclear what this field is intended to be
		featmsg.name = puccname;
		featmsg.pose.position.x = curPos.x;
		featmsg.pose.position.y = curPos.y;
		featmsg.pose.position.z = 0;
		featmsg.pose.orientation.x = 0;
		featmsg.pose.orientation.y = 0;
		featmsg.pose.orientation.z = sin(curPos.psi/2.0); // Conversions to quaternion representation
		featmsg.pose.orientation.w = cos(curPos.psi/2.0); // see http://wiki.alioth.net/index.php/Quaternion
		featpub.publish(featmsg);

		robot->puccMain(nhandle);
		++loopCount;
		loop_timer.sleep();

	} // end while(rosok)

	//## Close up shop
	ROS_INFO("Shutting down pucc_controller for %s", puccname.c_str());
	delete(robot);
	//## Return
	return(EXIT_SUCCESS);
}
