/*
 * Upucc.cpp
 *
 *  Created on: Oct 11, 2012
 *      Author: bobby
 */

#include "Upucc.h"


Upucc::Upucc() : Pucc()
{
	battVoltage = -1;
}

Upucc::~Upucc()
{
	stop();
	bt->BTClose();
}

/* If necessary, initialize the usb and serial port, return false for failure,
 *  return true for success.
 */
bool Upucc::setupHardware(int argc, char *argv[], int argcnt, std::string puccname)
{
	if(! this->isSimulated()) { // hardware
		ROS_INFO("Getting bluetooth mac address for %s...", puccname.c_str());
		std::string btparam = "/BTpairings/";
		btparam.append(puccname);
		std::string macaddr;
		bool res = ros::param::get(btparam, macaddr);
		if (!res){
			ROS_FATAL("ERROR: Could not find %s on the param server!\n Did you make sure to load upucc/config/BTpairings.yml?", btparam.c_str());
			exit(0);
		}
		ROS_INFO("Opening bluetooth for %s...", puccname.c_str());
		openBT(macaddr);
	} // else take no action
	return true;
}

void Upucc::openBT(std::string& macaddr)
{
	bt = new BTPort;
	bt->BTInit(macaddr);

	// Start listen thread
//	pthread_t thread;
//	pthread_create(&thread, NULL, BTlisten, (void *)this);
	return;

}

void Upucc::sendDriveCommands()
{
	// keep args within Create limits
	control.vR = saturate(control.vR,-500,500);
	control.vL = saturate(control.vL,-500,500);

	//printf("L: %f R: %f \n", control.vL, control.vR);

	uint8_t l,r;
	l = (uint8_t)(64.0+62.0*control.vL/500.0);
	r = (uint8_t)(192.0+62.0*control.vR/500.0);

	bt->BTSend((char *)&l,1);
	bt->BTSend((char *)&r,1);

}

void Upucc::simulateDynamics(double dt){
	// keep args within Create limits
	control.vR = saturate(control.vR,-500,500);
	control.vL = saturate(control.vL,-500,500);

	//simulate for dt
	double vR = control.vR/1000.0;
	double vL = control.vL/1000.0;
	vel = (vL+vR)/2.0;
	dpsi = (vR-vL)/(UPUCC_WHEELBASE);
	vx = vel*cos(psi);
	vy = vel*sin(psi);
	x = x + vx*dt;
	y = y + vy*dt;
	psi = psi + dpsi*dt;
}

/* TODO At the moment, does not appear that upuccs require scaling, happens
 * during calculation of controls based on wheelbase.  Check this in hardware.
 */
void Upucc::scaleDriveCommands()
{

}

/* This method is ONLY used to get a string representation of this ACTUAL
 * pucc object, and is really for debugging;
 * the preferred way to determine a pucc's type is using the
 * 'getVehIDFromName(std::string vehName)' method in Pucc.cpp, which should
 * produce identical behavior unless a pucc was given a name which did not
 * conform to the standards outlined in RAVEN_defines and elsewhere.
 */
std::string Upucc::getPuccType()
{
	return "Upucc";
}

void * BTlisten(void * arg)
{
	Upucc * gpucc = (Upucc *) arg;

	// The uPucc sends back one byte every second. It is the raw 8-bit ADC voltage
	// which corresponds to 1/2 the battery voltage.

	/*
	while(1)
	{
		gpucc->battVoltage = ((double)gpucc->bt->BTReceiveByte())/255.0*5.0*2.0;
		if(gpucc->battVoltage < 7.2)
		{
			printf("WARNING: Batt at %0.2f \n",gpucc->battVoltage);
			exit(0);
		}
		//printf("Batt: %0.2f \n",gpucc->battVoltage);
	}
	*/

}

