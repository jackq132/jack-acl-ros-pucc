#ifndef UPUCC_H_
#define UPUCC_H_

#include "../RAVEN_defines.hpp"
#include "../RAVEN_classes.hpp"
#include "../RAVEN_utils.hpp"
#include "../Pucc.h"
#include "ros/ros.h"

// TODO make sure upucc agrees with everything in pucc
class Upucc : public Pucc
{

public:
        double battVoltage;

        // functions
        Upucc();

        BTPort* 			  bt;		  // bluetooth port

        /* *********** Accessor and assignment methods *********** */
        std::string getPuccType();

        bool setupHardware(int argc, char *argv[], int argcnt, std::string puccname);
        void controller(Waypoint pos, Waypoint carrot);
        void sendDriveCommands();
        int getWheelBase() { return UPUCC_WHEELBASE;}
        void simulateDynamics(double dt);

        void scaleDriveCommands()

        void openBT(std::string& macaddr);
};

void * BTlisten(void * arg);


#endif
