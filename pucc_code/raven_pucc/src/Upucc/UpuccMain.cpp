#include <sys/time.h>
#include <semaphore.h>
#include <signal.h>
#include <iostream>
#include <vector>
#include <string>
#include "RAVEN_defines.hpp"
#include "RAVEN_classes.hpp"
#include "RAVEN_utils.hpp"
#include "pucc.hpp"
#include "gpucc.hpp"
#include "ros/ros.h"
#include "pucc/waypoint.h"
#include "pucc/carrot.h"
#include "pucc/vrotcmd.h"
#include "gpucc/state.h" // Allow different puccs to have different states
#include "featuremsgs/posefeature.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include <cmath> // for trig operations etc
#include <boost/algorithm/string.hpp>

Pucc robot;
typedef std::map <std::string, std::string> StrMap;
StrMap vehicleTypes;
enum Topic_T {POSE, VEL, NONE}; // Topic Type when querying master for topics.
enum {MAX_NAME_LENGTH = 100};
// TODO is there a way to typdef above in a single place so that both pucc.hpp and main.cpp are aware?

enum {LED_DURATION_FEAT = 0, LED_INTENSITY_FEAT = 1, LED_COLOR_FEAT = 2}; // The subscript of the features vector for the LED_FREQ


// General TODO : 'state', associated msg and pub now mostly redundant with 'pose'.

//receive state updates
void poseGenericCallback(const geometry_msgs::PoseStamped &msg){
/*** Check to ensure the subscribed topic is for a valid vehicle ***/
	std::string vehName = msg.header.frame_id;
	std::string namePrefix = vehName.substr(0,2); // first two letters of name -- define robot type

	StrMap::iterator vehType;
	if (vehicleTypes.count(namePrefix) != 0)
		vehType = vehicleTypes.find(namePrefix);
	else
		ROS_ERROR_STREAM("Vehicle name prefix-- "+namePrefix+" -- is not recognized.  Perhaps you haven't added it yet?");
/*** ***/
	double psi = 2.0*acos(msg.pose.orientation.w);
	double ssgn = msg.pose.orientation.z;
	if (ssgn < 0){
		psi *= -1.0;
	}
	//put psi in [0, 2pi]
	while (psi < 0)
		psi += 2.0*PI;
	while (psi > 2.0*PI)
		psi -= 2.0*PI;

	if(vehName.compare(robot.name) == 0) {// We are calling back the pose of THIS gpucc
//		ROS_DEBUG("Pose Callback on self");
		robot.setPose(msg.pose.position.x,msg.pose.position.y, psi);
	}

	else { //update obstacle corresponding to this vehicle
	// If this obstacle is being added for first time, give it unique id
	// Note that prioritization code used by the potential field relies
	// on obstacle IDS correctly matching their vehicle ID.
		if (robot.vehicleNumber.count(vehName) == 0) {
			int obstIDNum = getVehId(vehName);
			robot.vehicleNumber[vehName] = obstIDNum;
					robot.numObstacles++;
//			ROS_DEBUG("Number of Obstacles: %d", robot.numObstacles);
		}

		robot.obstacleMap[vehName]->name = vehName;
		robot.obstacleMap[vehName]->id  = robot.vehicleNumber[vehName]; // unique number for this vehicle
		robot.obstacleMap[vehName]->x   = msg.pose.position.x;
		robot.obstacleMap[vehName]->y   = msg.pose.position.y;
		robot.obstacleMap[vehName]->z   = msg.pose.position.z;
		robot.obstacleMap[vehName]->psi = psi;

		// NOTE here that velocity has not been updated - that is done
		// in a separate callback.

		robot.obstacleMap[vehName]->t   = (double)(msg.header.stamp.sec + msg.header.stamp.nsec/1.0e9);
		// TODO that this assumes that all other obstacles are GPUCCs;
		// Would be better to have a generic here.
		robot.obstacleMap[vehName]->radius = GPUCC_RADIUS;

//		ROS_DEBUG("Vehicle %s with numObstacles %d updating pose for obstacle %s", robot.name.c_str(), robot.numObstacles, vehName.c_str());
	}
}

void velGenericCallback(const geometry_msgs::TwistStamped &msg){
/*** Check to ensure the subscribed topic is for a valid vehicle ***/
	std::string vehName = msg.header.frame_id;
	std::string namePrefix = vehName.substr(0,2); // first two letters of name -- define robot type

	std::map<std::string, std::string>::iterator vehType;
	if (vehicleTypes.count(namePrefix) != 0)
		vehType = vehicleTypes.find(namePrefix);
	else
		ROS_ERROR_STREAM("Vehicle name prefix-- "+namePrefix+" -- is not recognized.  Perhaps you haven't added it yet?");
/*** ***/

	double tx, ty, tpsi, tvel;
	robot.getState(tx, ty, tpsi, tvel);
	double speed = distance2(0,0,msg.twist.linear.x,msg.twist.linear.y);
	double velHdg = atan2(msg.twist.linear.y, msg.twist.linear.x);
	double diff = velHdg-tpsi;

	while(diff > PI)
		diff -= 2.0*PI;
	while(diff < -PI)
		diff += 2.0*PI;

	if(fabs(diff) > PI/2.0)
	    speed *= -1.0;

	if(vehName.compare(robot.name) == 0) {// We are calling back the vel of THIS gpucc
		//ROS_INFO("dx: %f dy: %f dth: %f spd: %f", msg.twist.linear.x, msg.twist.linear.y, msg.twist.angular.z, speed);
		robot.setVel(msg.twist.linear.x,msg.twist.linear.y,msg.twist.angular.z, speed);
	}
	else {
		robot.obstacleMap[vehName]->vel = speed;
		robot.obstacleMap[vehName]->dx = msg.twist.linear.x;
		robot.obstacleMap[vehName]->dy = msg.twist.linear.y;
//		ROS_DEBUG("Vehicle %s with numObstacles %d updating vel for obstacle %s", robot.name.c_str(), robot.numObstacles, vehName.c_str());
	}
}

// subscribe a vehicle to this particular obst (essentially perfect state info of other agents)
void subscribeObstacle(ros::NodeHandle nhandle, std::string newObstName, bool isSimulated)
{
	if(newObstName.compare(robot.name) == 0) {
		ROS_INFO("Attempted to subscribe vehicle %s as its own obstacle!", robot.name.c_str());
		return;
	}

	if(robot.obstacleMap.count(newObstName) != 0) {
		ROS_INFO("Tried to resubscribe to an obstacle already subscribed!");
		return;
	}
//    ROS_INFO("New obst detected and added: %s", newObstName.c_str());
	Obstacle *newObst = new Obstacle();
	newObst->name = newObstName;

//subscribe to its pose topic
	ROS_INFO("Vehicle --%s-- subscribing to obstacle: --%s--",robot.name.c_str(),newObstName.c_str());
	newObst->posesub = new ros::Subscriber(nhandle.subscribe(newObstName+"/pose", 1, &poseGenericCallback)); //s/b 200 if we aren't using the callback
	newObst->velsub = new ros::Subscriber(nhandle.subscribe(newObstName+"/vel", 1, &velGenericCallback));

	robot.obstacleMap[newObstName] = newObst;

}

//receive waypoint updates
void wayptCallback(const pucc::waypoint::ConstPtr& msg){
//	robot.turnMode = 0;
	robot.followMode = -1;
//	robot.searchMode = false;
	robot.addWaypoint(msg->clear, msg->mutable_id, msg->x, msg->y, msg->psi, msg->speed, msg->feats); //msg->clear: 0 is append, 1 is clear and add new
}

void vpsidotCallback(const pucc::vrotcmd::ConstPtr& msg){
	robot.setVPsidotRef(msg->v, msg->psidot);
}

void toggleLEDCallback(const ros::TimerEvent& timerEvent)
{
	unsigned char newPlayLEDColor = 1; // TODO Placeholder, can modify colors later
	// The casts below are to remind you that the data types are a bit wonky
	if(robot.playLEDIntensity > 0) robot.playLEDIntensity = 0;
	else robot.playLEDIntensity = (unsigned char)(255);
	ROS_INFO("Set LED to be %d", robot.playLEDIntensity);
	// Only actually physically set the LED if not simulated
	if(!robot.isSimulated) robot.setLED(LED_PLAY, newPlayLEDColor, robot.playLEDIntensity);
	// double below to emphasize that the feature is stored as a double, though value itself cannot have decimal
	robot.features[LED_INTENSITY_FEAT] = (double)(robot.playLEDIntensity);
	robot.features[LED_COLOR_FEAT] = newPlayLEDColor;
}

///////// from vehiclebroadcaster.cpp

Topic_T checkVehTopic(std::string namePrefix, std::string &obstName, std::string topicName)//inline
{
//DEBUG    const char * cstrTopicName = topicName.c_str();
	std::vector<std::string> tmp;
	boost::split(tmp, topicName, boost::is_any_of("/"), boost::token_compress_on); // of the form: /uP01/pose
    // tmp[0-2] contains /""/<name>/"pose"
    obstName = tmp.at(1);
    if (vehicleTypes.count(namePrefix) != 0){ // This is a valid vehicle
        if(!tmp.at(2).compare("pose")){ // This is a pose topic
 //DEBUG         ROS_INFO("Topic %s IS a vehicle pose topic.",cstrTopicName);
            return POSE;
        } else if(!tmp.at(2).compare("vel")) {
//DEBUG        	ROS_INFO("Topic %s IS a vehicle vel topic.",cstrTopicName);
        	return VEL;
        } else {
//DEBUG            ROS_INFO("Topic %s is a vehicle topic, non-pose.",cstrTopicName);
        }
    }
    else {
//    	ROS_INFO("Topic %s with prefix %s does not correspond to a known vehicle type.",cstrTopicName,namePrefix.c_str());
    }
    return NONE;
}

////////////// from vehicleBroadcaster.cpp
void updateObstTopics(ros::NodeHandle nhandle, ros::master::V_TopicInfo &allTopics)
{
	std::set<std::string> vehNamesFromTopics;
    for(int i=0; i < allTopics.size(); i++){
        std::string topicName = allTopics[i].name; // /uP04/pose
        std::string namePrefix = topicName.substr(1,2); // takes form /uP etc
        std::string newObstName;
        Topic_T topic_t = checkVehTopic(namePrefix, newObstName, topicName);
		if(topic_t == POSE || topic_t == VEL) { // This topic is a vehicle topic
			vehNamesFromTopics.insert(newObstName);
	        if(newObstName.compare(robot.name) != 0) { // The topic-vehicle does not belong to this robot
				if(robot.obstacleMap.count(newObstName) == 0) { // The topic-vehicle is not yet subscribed
					ROS_INFO("Subscribing vehicle %s to obstacle %s",robot.name.c_str(), newObstName.c_str());
					subscribeObstacle(nhandle,newObstName,PUCC::checkNameSimulated(newObstName));
				}
			}
        }
    }
//    robot.removeOldObstacles(vehNamesFromTopics);
}

void initMaps()
{
// Initialize map of vehicle types
	vehicleTypes.insert(std::pair<std::string, std::string>("uP","/upucc"));
	vehicleTypes.insert(std::pair<std::string, std::string>("BQ","/quad"));
	vehicleTypes.insert(std::pair<std::string, std::string>("mQ","/miniquad"));
	vehicleTypes.insert(std::pair<std::string, std::string>("GP","/gpucc"));
}

// TODO - iterate through all topics a second time, could be time-consuming - better to remove the non-vehicle topics the first time (while checking for nodes to add), and check that list.
/** Check for Obstacles which correspond to topics no longer being published;
 *  Remove those Obstacles from the obstacleMap and destruct them.
 *  Note we place this here and note in main() since there is no interaction with subscribers,
 *  everything is internal to the pucc.
 */
void removeOldObstacles(std::set<std::string> vehNamesFromTopics)
{
// iterate through the subscribers and get the name of each of the vehicles to which we are already subscribing
	std::map <std::string, Obstacle *>::iterator it = robot.obstacleMap.begin();
	while(it != robot.obstacleMap.end()) {
		std::string subscribedObstName = it->first; // vehicle name
        bool wasRemoved = (std::find(vehNamesFromTopics.begin(),vehNamesFromTopics.end(), subscribedObstName) == vehNamesFromTopics.end()); // do we hit the end of the vehNamesFromTopics list without finding our obstacle
        // if that vehicle is no longer present in the topics being broadcast, we should delete its associated marker
        if(wasRemoved)
        {
        	Obstacle *obstToDelete = (it->second);
            robot.obstacleMap.erase(it++); // erase the obstacle, which calls its destructor automatically.
            if(obstToDelete != NULL) delete(obstToDelete);
            ROS_INFO("Obstacle --%s-- was removed from consideration by vehicle",subscribedObstName.c_str(), robot.name.c_str());
        }
        else ++it;
    }
}

void updateLEDDuration(ros::NodeHandle &nh)
{
	if(robot.features.size() > 0) {// Make sure the robot has been assigned features
		double LEDDur = robot.features[LED_DURATION_FEAT];
		if(robot.togglePeriod != LEDDur) {
			ROS_INFO("Setting new toggle period to be %f", LEDDur);
			robot.togglePeriod = LEDDur;
			ros::Duration toggleDur = ros::Duration(LEDDur);
			robot.ledToggleTimer = nh.createTimer(toggleDur, toggleLEDCallback);
		}
	}
}

// Main block
int main( int argc, char *argv[] )
{
	//argv[0] = rosrun
	//argv[1] = pucc
	//argv[2] = gpucc_controller
	//argv[3] = -s for simulation, -h for hardware
	//argv[4] = name
	//argv[5] = <collision avoidance type>  eg --potential or --orca

    //// May want to add an arg for 'team'

	//Debugging code to see what argc and argv are
	ROS_INFO("argc is %d", argc);
	for(int i = 0; i < argc; i++){
		ROS_INFO("argv %d is %s", i, argv[i]);
	}

	std::string puccname;
	std::string usbPortLoc;
    //argv[0] = /.../gpucc_controller
	//argv[1] = -s, -h
	//argv[2] = name
	//argv[3] = __name:=...  [only for launchfile]
	//argv[4] = __log:=...   [only for launchfile]
	if (argc < 3){
		ROS_FATAL("Not enough arguments to instantiate pucc.");
	} else{
		int argcnt = 0;
		bool validPucc = false;
		bool argsuccess = false;
		while(argcnt < argc){
			if (strncmp(argv[argcnt], "gpucc", 5) == 0){
				robot = Gpucc();
				validPucc = true;
			}
			else if (strncmp(argv[argcnt], "upucc", 5) == 0){
				robot = Upucc();
				validPucc = true;
			}
			if(validPucc) { // specified either 'gpucc' or 'upucc' above
				if (argcnt+2 < argc){ // At least enough arguments to specify name and isSimulated
					puccname = argv[argcnt+2];
					// This is a simulated pucc
					if(strncmp(argv[argcnt+1] == "-s", 2)) {
						if(!Pucc::checkNameSimulated(puccname)) {
							ROS_FATAL("You specified a simulated pucc, but did not append an 's' to its name, e.g. GP02s");
						}
						else robot.setSim(true);
					}
					else if(strncmp(argv[argcnt+1] == "-h", 2)) {
						if(Pucc::checkNameSimulated(puccname)) {
							ROS_FATAL("You specified a hardware pucc, but appended an 's' to its name, e.g. GP02s");
						}
						else robot.setSim(false);
					}
				}
				else ROS_FATAL("Not enough arguments following pucc type (gpucc or upucc) to create node");

				if(robot.isSim()) {argsuccess = true; break;} // No further arguments needed
				else { // hardware
					if(argcnt+3 < argc){
						usbPortLoc = argv[argcnt+2];
						argsuccess = true;
						break;
					}
					else ROS_FATAL("Specified hardware pucc, but failed to provide usb port location");
				}
			}
			argcnt++;
		} // Loop completed
		if(!argsuccess){
			ROS_FATAL("Did not find required arguments to gpucc_controller.");
		}
	}
    if (gpuccname.length() > MAX_NAME_LENGTH){
		ROS_FATAL("gpucc names cannot be greater than 100 characters.");
		return EXIT_FAILURE;
	}

    initMaps();
	robot.setName(puccname);
	robot.setSim(sim);
	robot.setID(Pucc::getVehID(puccname));

	//initialize ROS and subscribe/advertise topics
	std::string nodename = "gpucc_controller_";
	nodename.append(gpuccname);
	ros::init(argc, argv, nodename);
	ros::NodeHandle nhandle;
	ros::Subscriber posesub, velsub; // used for actual hardware
	ros::Publisher featpub, statepub; // TODO: statepub now redundant
    ros::Publisher poseSpooferPub, velSpooferPub; // used during simulation
	if (!robot.isSimulated()){
		posesub = nhandle.subscribe(gpuccname+"/pose", 1000, poseGenericCallback);
		velsub = nhandle.subscribe(gpuccname+"/vel", 1000, velGenericCallback);
	}

    else { // We need to advertise our own pose for visualization, spoofing vicon output
	    poseSpooferPub = nhandle.advertise<geometry_msgs::PoseStamped>(gpuccname+"/pose", 1000);
	    velSpooferPub = nhandle.advertise<geometry_msgs::TwistStamped>(gpuccname+"/vel", 1000);
    }

    //// TODO - statepub is now pretty redundant with the pose publisher
	statepub = nhandle.advertise<gpucc::state>(gpuccname+"/state", 1000);

	featpub = nhandle.advertise<featuremsgs::posefeature>(gpuccname+"/features", 1000);


	ros::Subscriber wptsub = nhandle.subscribe(gpuccname+"/waypts", 1000, wayptCallback);

	ros::Publisher carrotpub = nhandle.advertise<pucc::carrot>(gpuccname+"/carrots", 1000);

	ros::Subscriber vpsidotsub = nhandle.subscribe(gpuccname+"/vpsidot", 1000, vpsidotCallback);


	//## Welcome msg
	ROS_INFO("Starting %s gpucc_controller for %s...", (robot.isSimulated() ? "simulation" : "hardware") , gpuccname.c_str() );

	//setup the gpucc: if hardware, open serial. If simulation, give the gpucc an initial position.
	//## Open the serial connection for the gpucc if we aren't in simulation mode
	if (!robot.isSimulated()){
// The 'commandModule' below corresponds to camera-actuated mode, no longer used.
/*        if(commandModule){
	        serial.spInitialize("/dev/ttyUSB0",19200,true);
	        ROS_INFO("Initialized serial port at baudrate 19200");
          }else{ */
        bool success = robot.serial->spInitialize(usbPortLoc,57600,true);
	    if(success != 1) {
            ROS_INFO("Failed to initialize Serial Port at %s", usbPortLoc.c_str());
            return EXIT_FAILURE;
            }
            ROS_INFO("Initialized serial port at baudrate 57600");
            robot.start_OI();
	} else {
		//put the gpucc somewhere...
		double xfrac = (double)(rand()%100)/100.0, yfrac = (double)(rand()%100)/100.0;
		robot.setPose(XMIN + (XMAX-XMIN)*xfrac,YMIN + (YMAX-YMIN)*yfrac,0);
		robot.setVel(0,0,0,0);
	}


	//## Give an initial goal at current location
	Waypoint wp = Waypoint();
	ros::spinOnce(); // Get first waypoint (current robot position)
	ROS_INFO("Robotx: %2.2f, Roboty: %2.2f", robot.x,robot.y);
	wp.x = robot.x;
	wp.y = robot.y;
	wp.psi = robot.psi;
	wp.vel = SPEED_LIMIT; //// Prev. value for bluetooth 0.2; //can't go faster or the controller goes to hell - BT can't update fast enough.
	robot.goal.push_back(wp);


	//**************************************************
	//## MAIN LOOP
	//**************************************************
	ROS_INFO("Starting main control loop for %s...", gpuccname.c_str());
	double tx,ty,tpsi,tvel;
	ros::Time curTime, prevTime;
	prevTime = ros::Time::now();
	ros::Rate loop_timer(200);


	ros::master::V_TopicInfo allTopics;



	// TODO limit the number of times we query the master per loop


	while( ros::ok() ) {
		ros::master::getTopics(allTopics);
		updateObstTopics(nhandle,allTopics);

		ros::spinOnce(); //do callbacks
		//run the controller and send commands or simulate
		robot.calcCommands();
		if (!robot.isSimulated()){
			robot.sendCommands();
		} else {
			//simulate
			curTime = ros::Time::now();
			double dt = curTime.sec + curTime.nsec/1.0e9 - prevTime.sec - prevTime.nsec/1.0e9;
			prevTime = curTime;
			robot.simulateDynamics(dt);
		}
        if(robot.isSimulated()) {
            //publish the spoofed pose for this simulated bot.
        geometry_msgs::PoseStamped posemsgSpoof;
        posemsgSpoof.header.stamp = ros::Time::now();
        posemsgSpoof.header.frame_id = gpuccname;
        posemsgSpoof.pose.position.x = robot.x;
        posemsgSpoof.pose.position.y = robot.y;
        posemsgSpoof.pose.position.z = 0;
        posemsgSpoof.pose.orientation.x = 0;
        posemsgSpoof.pose.orientation.y = 0;
        posemsgSpoof.pose.orientation.z = sin(robot.psi/2.0); // Conversions to quaternion representation
        posemsgSpoof.pose.orientation.w = cos(robot.psi/2.0);

        poseSpooferPub.publish(posemsgSpoof);

        geometry_msgs::TwistStamped velmsgSpoof;
        velmsgSpoof.header.stamp = ros::Time::now();
        velmsgSpoof.header.frame_id = gpuccname;
        velmsgSpoof.twist.linear.x = robot.dx;
        velmsgSpoof.twist.linear.y = robot.dy;
        velmsgSpoof.twist.linear.z = 0;
        velmsgSpoof.twist.angular.x = 0;
        velmsgSpoof.twist.angular.y = 0;
        velmsgSpoof.twist.angular.z = sin(robot.dpsi/2.0); // Conversions to quaternion representation

        velSpooferPub.publish(velmsgSpoof);
        }

////////////// TODO - the 'state' message is now mostly redundant
		//publish state
		//broadcast messages
		gpucc::state statemsg;
		statemsg.header.stamp = ros::Time::now();
		statemsg.header.frame_id = gpuccname;
		statemsg.name = gpuccname;
		statemsg.mutable_id = robot.getMutableID();
		statemsg.idle = robot.idle;
		statemsg.x = robot.x;
		statemsg.y = robot.y;
		statemsg.psi = robot.psi;
		statemsg.dx = robot.dx;
		statemsg.dy = robot.dy;
		statemsg.dpsi = robot.dpsi;

		statepub.publish(statemsg);

		//publish the carrot message
		pucc::carrot carrotmsg;
		carrotmsg.header.stamp = ros::Time::now();
		carrotmsg.name = robot.getName();
		carrotmsg.x = robot.carrotx;
		carrotmsg.y = robot.carroty;
		carrotpub.publish(carrotmsg);

		//publish the features
		//this is used when cameras are operating in simulation mode
		//and need the gpuccs to tell them what features are being emitted
		featuremsgs::posefeature featmsg;
		featmsg.header.stamp = ros::Time::now();
		featmsg.mutable_id = robot.getMutableID();
		featmsg.obsname = gpuccname;
		featmsg.name = gpuccname;
		////BOB Warning - sign and unsigned comparison
		for (int i = 0; i < robot.features.size(); i++){
			featmsg.feats.push_back(robot.features[i]);
		}
		featmsg.pose.position.x = robot.x;
		featmsg.pose.position.y = robot.y;
		featmsg.pose.position.z = 0;
		//statemsg.feats = robot.features;
		featpub.publish(featmsg);

		// LED duration (blinking) can change at any time based on 'features' of robot,
		// set by features field of pose
		updateLEDDuration(nhandle);

		/*******DEBUG *******/
//		ROS_INFO("Target wp for robot %s is (%f, %f)",robot.name.c_str(),robot.goal.at(0).x,robot.goal.at(0).y);
		/*****************/


		// what's the status
		/*if( cnt%20 == 0 ) {

			for(int i=0;i<gpuccs.size();i++)
			{
				gpuccs[i].getState(tx,ty,tpsi,tvel);
				printf("%s: x: %+0.2f->%+0.2f, y: %+0.2f->%+0.2f, psi: %+0.2f->%0.2f, vel: %+0.2f->%0.2f batt: %0.2f \n",gpuccs[i].getName(),tx,gpuccs[i].goal.at(0).x,ty,gpuccs[i].goal.at(0).y,tpsi,gpuccs[i].goal.at(0).psi,tvel,gpuccs[i].goal.at(0).vel,gpuccs[i].battVoltage);
				printf("\n");
			}
			printf("\n");

		}*/



		// loop maintenance
		loop_timer.sleep();

	} // end while(rosok)

	//## Close up shop
	ROS_INFO("Shutting down gpucc_controller for %s", gpuccname.c_str());

	//## Return
	return(1);
}
