/*
 * Contains definitions common to many applications / vehicles in RAVEN.
 */

#ifndef RAVEN_DEFINES_HPP_
#define RAVEN_DEFINES_HPP_

#include <string>
#include <string.h>

// Legacy Vehicle States - for backwards compatability
enum{ 
  OLD_LANDED_READY=2,
    OLD_FLYING_TASKABLE=3};

enum Waypoint_Clear_T {WPTS_APPEND, WPTS_CLEAR};

// The subscript of the features message (vector) published by all puccs
enum {LED_DURATION_FEAT = 0, LED_INTENSITY_FEAT = 1, LED_COLOR_FEAT = 2, IDLE_FEAT = 3};

// These variables are not associated with one of the parameter launch files
// since they should be changed with care.
#define PI		(3.14159265358979323)
#define PI_2		(3.14159265358979323/2.0)
#define EPSILON		0.000000000001
#define LARGE_NUM	10000000000000.0
#define SER_BUF_SZ      1024
#define ALPHA           .5
#define GPUCC_RADIUS    0.20 // m
#define UPUCC_RADIUS	0.10 // m
#define UPUCC_WHEELBASE 0.08 // m
#define GPUCC_WHEELBASE 0.26 // m

// NOTE These are now unused variables, or at least "L1" controller functionality untested
#define USE_L1          0      // use the L1 controller
#define L1DIST          0.2

// Angle between the line connecting the previous waypoint to the current and
// the line between the gpucc and the current; beyond this angle (usually 90 deg)
// the waypoint is considered achieved; this way puccs do not backtrack along
// their path when the true goal still lies ahead.
#define CHECKPT_ANGLE	(90.0/180.0 * PI)
#define MAX_WP_DIST		0.10 // m

// TODO are these needed? What are they?
/*
#define X0  0.019
#define Y0 -1.286
#define X1 -0.067
#define Y1  5.541

#define I0X -1.5
#define I0Y 5.4

#define I1X -1.5
#define I1Y -1.2
*/


#endif /*RAVEN_DEFINES_HPP_*/
