/*
 * Contains classes common to many experiments and vehicles in RAVEN.
 * TODO: merge this with Mark C's common code.
 */

#ifndef RAVEN_CLASSES_HPP_
#define RAVEN_CLASSES_HPP_

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <signal.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>
#include <vector>
#include <iostream>
#include <string>
#include <string.h>
#include <math.h>
//#include <bluetooth/bluetooth.h>
//#include <bluetooth/hci.h>
//#include <bluetooth/hci_lib.h>
//#include <bluetooth/rfcomm.h>

#include "ros/ros.h"

#include <linux/input.h>
#include <linux/joystick.h>

#include <RAVEN_defines.hpp>

#include <valarray> // For the Bezier

// TODO Might need to include one of the following boost things here for boost::array
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/algorithm/string.hpp> // For adding strings to the StrMap


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
typedef struct
{
        double psi;
        double vel;

        short vL;
        short vR;

		short cam;
}
Controls;

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
typedef struct
{
    double x;
    double y;
    double psi;
    double speed;
	double id;
	bool obstructed;
	std::vector<double> feats;
	int mutable_id;
}
Waypoint;

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class Obstacle
{
public:
    // these are used in carrotGen
    std::string name;
    int    id;
	double x;
	double y;
	double z;

	double dx;
	double dy;
//	double dz; // not implemented

	double radius;
	double t; // not used in carrotgen

	// these are used in estimator
	double ttt;
	double psi;
	double vel;
	int    type;
	int    turn;
	ros::Subscriber *posesub, *velsub;

	inline Obstacle()
	{
		name = "";
		posesub = velsub = NULL;
		x = y = z = dx = dy = radius = t = ttt = psi = vel = 0;
		id = type = turn = 0;
	}
	inline ~Obstacle()
	{
		if(posesub != NULL) posesub->shutdown();
		if(velsub != NULL) velsub->shutdown();
	}
};


class RedAgent : public Obstacle {
public:
    int team;
	bool idle; // feature vector used in later code uses double for all values

////	bool tagged; ////REMOVED; NO 'tag.h'
	ros::Subscriber featsub;
	ros::Publisher wptpub;
	inline RedAgent() : Obstacle()
	{
		team = 0;
		idle = true;
	}

	inline ~RedAgent()
	{ }

};



struct Message
{
    int    src;
    int    dest;
    int    cmdID;
    double data[12];

    // Constructors:
    Message()
    {
        src   = 0;
        dest  = 0;
        cmdID = 0;
        for( int ii=0; ii<12; ii++)
        {
            data[ii] = 0.0;
        }
    }
};


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class BTPort
{

		const char * mac;
        char buffer[2048];
        int s;

public:

        int   BTInit(std::string macaddr);
        int   BTSend(char* pkt, int len);
        uint8_t  BTReceiveByte();
        void  BTClose();

};

class UDPPort
{

        bool broadcast;
        
        int  portTx;
        int  portRx;
        
        int  socketTx;
        int  socketRx;
        
        struct sockaddr_in addressTx;
        struct sockaddr_in addressRx;

        char buffer[2048];

public:

        int   udpInit(bool bcast, std::string ipaddr, int port);
        int   udpInit(bool bcast, std::string ipaddr, int listen_port, int bcast_port);
        int   udpSend(char* pkt);
        int   udpSend(Message* pkt);
        char* udpReceive();
        void  udpClose();

};


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class SerialPort
{

        int  serial;
        unsigned char buffer[SER_BUF_SZ];

public:

        SerialPort();

        bool  initialized;
        int   spInitialize(std::string usbport,int baudrate, bool block);
        int   spSend(char* pkt);
        int   spSend(char* pkt, int sz);
        int   spSend(uint8_t* pkt, uint8_t sz);
        int   spSend(unsigned char* pkt, int sz);
        unsigned char* spReceive();
        unsigned char* spReceive(int* nbytes);
        int   spReceive(unsigned char* buf, int nbytes);
        char  spReceiveSingle();
        int   spSetBaud(int rate);
        void  spFlush();        
        void  spClose();

};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class  Integrator
{
private:

public:
        //Main data members
        double value;
        double AW_thresh;
        double AW_value;

        //Constructors:
        Integrator();
        Integrator(double aw_thresh);

        void increment (double inc, double dt);
        void reset ();
};



/* The Bezier class is useful in generating smooth paths given some list of waypoints.
 * See http://devmag.org.za/2011/06/23/bzier-path-algorithms/
 */

/* TODO finish the BezierPath class */
/*
class BezierPath
{
public:
	const int SEGMENTS_PER_CURVE = 10;
	// const double MINIMUM_SQR_DISTANCE = 0.01;
    // private const float DIVISION_THRESHOLD = -0.99f; Used when simplifying (sparsifying) a curve, corresponds to 172 deg = 8 deg from straight.

	std::list<Vector_T> controlPoints;
	int curveCount;

	BezierPath();


	std::valarray<double> calculateBezierPoint(double t, Vector_T startPt, Vector_T ctrlPt1, Vector_T ctrPt2, Vector_T endPt);
};
*/


//// ObstacleList no longer used by carrotgen - using std::map instead
/*
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class ObstacleList
{

//        pthread_mutex_t mutex;

public:
        std::vector<Obstacle *> list;

        ObstacleList();
        void add(Obstacle *obj);
        void getState(char* name, double &x, double &y, double &dx, double &dy, double &t);
        void clear();
        int  size();
};
*/

#endif /*RAVEN_CLASSES_HPP_*/
