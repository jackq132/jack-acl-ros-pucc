/*
 * Contains classes common to many experiments and vehicles in RAVEN.
 * TODO: merge this with Mark C's common code.
 */


#ifndef RAVEN_UTILS_HPP_
#define RAVEN_UTILS_HPP_

#include <iostream>
#include <math.h>
#include <string>
#include <string.h>
#include <stdio.h>
#include <stdlib.h> // atoi needed by getVehIDFromName
#include <vector>

#include "RAVEN_defines.hpp"

//## Functions

double param(std::string n); // should be static

double wrap(double ang);

double unwrap(double ang);

double saturate(double val, double vmin, double vmax);

short  saturate(short val, short vmin, short vmax);

unsigned char saturate(unsigned char val, unsigned char vmin, unsigned char vmax);

double distance2(double x1, double y1, double x2, double y2);

int    min(double* in, int sz);

double quaternionTo2DAngle(double z, double w);

bool checkNameSimulated(std::string vehName);

bool isInsidePolygon(int nvert, double *vertx, double *verty, double testx, double testy);

int getVehIDFromName(std::string vehName);


#endif /*RAVEN_UTILS_HPP_*/
